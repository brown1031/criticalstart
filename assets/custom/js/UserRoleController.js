angular.module('CSMainApp', ['ngSanitize'])
.controller('UserRoleController', function ( $scope, $http, $timeout ) {
	
	$scope.baseUrl = "";
	
	$scope.userRoles = [];
	
	$scope.init = function ( str_baseUrl ) {
		
		$scope.baseUrl = str_baseUrl;
		
		$scope.loadUserRoles();
		
	}
	
	$scope.loadUserRoles = function () {
		
		$http.post( $scope.baseUrl + "settings/getuserroles", {})
			.success(function(data,s,h,c){
				
			for ( var i = 0; i < data.length; i ++ ) {
				
				var userrole = {
						id: data[i].id,
						roleName: data[i].roleName,
						roleDescription: data[i].roleDescription,
						bedit: false
				};
				
				$scope.userRoles.push ( userrole );
				
			}
			
		});
		
	}
	
	$scope.onDeleteUserRole = function ( $index ) {
		
		bootbox.confirm('Are you sure?', function(result) {
			
			if ( result ) {
				
				$http.post( $scope.baseUrl + "settings/deleteUserRole/" + $scope.userRoles[$index].id, {})
					.success( function ( data, s,h,c) {
						
						if ( data.result == 'success') {
							
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["success"]("User role deleted successfully!", "Success");
							
							$scope.userRoles.splice( $index, 1);
						} else {
							
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["error"]("Sorry, failed to delete user role!", "Failure");
							
						}
						
					} );
			}
		});
		
	}
	
	$scope.addNewUserRole = function () {
		
		$("#formNewUserRole").find("#submitButton").html('Waiting...').addClass('disabled');
		
		$("#formNewUserRole").ajaxSubmit({
			success: function ( data, s,h,c) {
				$("#formNewUserRole").find("#submitButton").html('Save').removeClass('disabled');
				
				var result= JSON.parse( data );
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				if ( result.result == 'success') {
					
					toastr["success"]("User role added successfully!", "Success");
					
					var alertType = {
							id: result.userRole.id,
							roleName: result.userRole.roleName,
							roleDescription: result.userRole.roleDescription,
							bedit: false
					};
					
					$scope.userRoles.push( alertType );
					
					$scope.$apply();
					
					$("#idDialog").modal( 'hide' );

					$("#formNewUserRole").find('input[type="text"]').val('');
					$("#formNewUserRole").find('textarea').val('');
					
				} else {
					
					toastr["error"]("Sorry, failed to add new user role type!", "Failure");
				}

			}
		});
	}
	
	$scope.onUpdateUserRole = function ( $index ) {
		
		var objUpdateForm = $(document).find("#editUserRoleForm_" + $index).eq(0);
		
		objUpdateForm.ajaxSubmit({
			
			success: function ( data, s,h,c ) {
				
				var result= JSON.parse( data );
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				if ( result.result == 'success') {
					
					toastr["success"]("User role updated successfully!", "Success");

				} else {
					
					toastr["error"]("Sorry, failed to update user role!", "Failure");
				}
			}
		});
	}
});