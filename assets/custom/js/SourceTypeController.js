angular.module('CSMainApp', ['ngSanitize'])
.controller('SourceTypeController', function ( $scope, $http ) {

	$scope.baseUrl = "";
	
	$scope.sourceTypes = [];
	
	$scope.init = function ( str_baseUrl ) {
		
		$scope.baseUrl = str_baseUrl;
		
		$scope.loadSourceTypes();
		
	}
	
	$scope.loadSourceTypes = function () {
		
		$http.post( $scope.baseUrl + "settings/getsourcetypes", {})
			.success(function(data,s,h,c){
				
			for ( var i = 0; i < data.length; i ++ ) {
				
				var sourcetype = {
						id: data[i].id,
						typeName: data[i].typeName,
						typeDescription: data[i].typeDescription,
						createdAt: data[i].createdAt,
						modifiedAt: data[i].modifiedAt,
						modifiedBy: data[i].odUserFirstName + " " + data[i].odUserLastName,
						bedit: false
				};
				
				$scope.sourceTypes.push ( sourcetype );
				
			}
			
		});
		
	}
	
	$scope.onDeleteSourceType = function ( $index ) {
		
		bootbox.confirm('Are you sure?', function(result) {
			
			if ( result ) {
				
				$http.post( $scope.baseUrl + "settings/deleteSourceType/" + $scope.sourceTypes[$index].id, {})
					.success( function ( data, s,h,c) {
						
						if ( data.result == 'success') {
							
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["success"]("Source type deleted successfully!", "Success");
							
							$scope.sourceTypes.splice( $index, 1);
						} else {
							
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["error"]("Sorry, failed to delete source type!", "Failure");
							
						}
						
					} );
			}
		});
		
	}
	
	$scope.addNewSourceType = function () {
		
		$("#formNewSourceType").find("#submitButton").html('Waiting...').addClass('disabled');
		
		$("#formNewSourceType").ajaxSubmit({
			success: function ( data, s,h,c) {
				
				var result= JSON.parse( data );
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				if ( result.result == 'success') {
					
					toastr["success"]("Source type added successfully!", "Success");
					
					var sourcetype = {
							id: result.sourceType.id,
							typeName: result.sourceType.typeName,
							typeDescription: result.sourceType.typeDescription,
							createdAt: result.sourceType.createdAt,
							modifiedAt: result.sourceType.modifiedAt,
							modifiedBy: result.sourceType.odUserFirstName + " " + result.sourceType.odUserLastName,
							bedit: false
					};
					
					$scope.sourceTypes.push( sourcetype );
					
					$scope.$apply();
					
					$("#idDialog").modal( 'hide' );
					
					$("#formNewSourceType").find("input[type='text']").val("");
					$("#formNewSourceType").find("textarea").val("");
					
				} else {

					toastr["error"]("Sorry, failed to add new source type!", "Failure");
				}
				
				$("#formNewSourceType").find("#submitButton").html('Save').removeClass('disabled');
				
				
			}
		});
	}
	
	$scope.onUpdateSourceType = function ( $index ) {
		
		var objUpdateForm = $(document).find("#editSourceTypeForm_" + $index).eq(0);
		
		objUpdateForm.ajaxSubmit({
			
			success: function ( data, s,h,c ) {
				
				var result= JSON.parse( data );
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				if ( result.result == 'success') {
					
					toastr["success"]("Source type updated successfully!", "Success");

				} else {
					
					toastr["error"]("Sorry, failed to update source type!", "Failure");
				}
			}
		});
	}
});