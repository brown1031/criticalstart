angular.module('CSMainApp', ['ngSanitize'])
.controller('ScriptsController', function ( $scope, $http ) {
	
	$scope.baseUrl = "";
	
	$scope.scripts = [];
	
	$scope.init = function ( strBaseUrl ) {
		
		$scope.baseUrl = strBaseUrl;
		
		$scope.loadScripts();
		
		toastr.options = {
				"closeButton" : true,
				"positionClass" : "toast-top-right",
				"onclick" : null,
				"showDuration" : "1000",
				"hideDuration" : "1000",
				"showEasing": "swing",
				"hideEasing" : "linear",
				"showMethod" : "fadeIn",
				"hideMethod" : "fadeOut"
				};
		
	}
	
	$scope.loadScripts = function () {
		
		$http.post( $scope.baseUrl + "settings/getscripts", {})
			.success( function ( data, s,h,c ) {
				
				for ( var i = 0; i < data.length; i ++ ) {
					
					var ascript = {
						id: data[i].id,
						fileName: data[i].scriptFileName,
						filePath: data[i].scriptFilePath,
						description: data[i].scriptDescription,
						runTimes: data[i].scriptRunTimes,
						scriptArgs: data[i].scriptArgs,
						scriptMaxRuntime: data[i].scriptMaxRuntime,
						scriptOrgs: data[i].scriptOrgs,
						scriptReturnVals: data[i].scriptReturnVals,
						scriptAudit: data[i].scriptAudit=='Y' ? true:false,
						bedit: false
					};
					
					$scope.scripts.push ( ascript );
					
				}
			});
	}
	
	$scope.addNewScript = function () {
		
		$("#formNewScript").find("#submitButton").html('Waiting...').addClass('disabled');
		
		$("#formNewScript").ajaxSubmit({
			success: function( data,s,h,c) {
				var result = JSON.parse( data );
				
				$("#formNewScript").find("#submitButton").html('Save').removeClass('disabled');
				
				if ( result.result == 'success' ) {
					
					toastr["success"]("New Script added successfully!", "Success");
					
					var ascript = {						
						id: result.pscript.id,
						fileName: result.pscript.scriptFileName,
						filePath: result.pscript.scriptFilePath,
						description: result.pscript.scriptDescription,
						runTimes: result.pscript.scriptRunTimes,
						scriptArgs: result.pscript.scriptArgs,
						scriptMaxRuntime: result.pscript.scriptMaxRuntime,
						scriptOrgs: result.pscript.scriptOrgs,
						scriptReturnVals: result.pscript.scriptReturnVals,
						scriptAudit: result.pscript.scriptAudit=='Y' ? true:false,
						bedit: false							
					};
					
					$scope.scripts.push( ascript );
					
					$scope.$apply();
					
					$("#idDialog").modal('hide');
					
					$("#formNewScript").find("input[type='text']").val("");
					$("#formNewScript").find("textarea").val("");
					
				} else {
					
					toastr["error"]("Sorry, failed to add new script!", "Failure");
					
				}
			}
		});
		
	}
	
	$scope.onDeleteScript = function ( $index ) {
		
		bootbox.confirm('Are you sure?', function( result ){
			if (result ) {
				$http.post( $scope.baseUrl + "settings/deleteScript/" + $scope.scripts[$index].id, {})
					.success(function(data,s,h,c) {
						
						if ( data.result == 'success' ) {
							
							toastr["success"]("Script deleted successfully!", "Success");
							
							$scope.scripts.splice( $index, 1);
							
						} else {
							
							toastr["error"]("Sorry, failed to delete script!", "Failure");
							
						}
					});
			}
		});
	}
	
	$scope.onUpdateScript = function ( $index ) {
		
		var objUpdateForm = $(document).find("#editScriptsForm_" + $index).eq(0);
		
		objUpdateForm.ajaxSubmit({
			success: function ( data, s,h,c) {
				var result = JSON.parse( data );
				
				if ( result.result == 'success' ) {
					toastr["success"]("Script updated successfully!", "Success");
				} else {
					toastr["error"]("Sorry,failed to update script!", "Success");
				}
			}
		});
		
	}
	
	$scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
		
		$('.make-switch').bootstrapSwitch();
		
	});
})
.directive('onFinishRender', function($timeout) {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			if ( scope.$last == true ) {
				$timeout( function() {
					scope.$emit('ngRepeatFinished');
				});
			}
		}
	};
});