angular.module('CSMainApp', ['ngSanitize'])
.controller('AlertTypeController', function ( $scope, $http, $timeout ) {
	
	$scope.baseUrl = "";
	
	$scope.alertDataTypes = [];
	
	$scope.init = function ( str_baseUrl ) {
		
		$scope.baseUrl = str_baseUrl;
		
		$scope.loadAlertDataTypes();
		
	}
	
	$scope.loadAlertDataTypes = function () {
		
		$http.post( $scope.baseUrl + "settings/getalertdatatypes", {})
			.success(function(data,s,h,c){
				
			for ( var i = 0; i < data.length; i ++ ) {
				
				var alerttype = {
						id: data[i].id,
						type: data[i].type,
						description: data[i].description,
						createdAt: data[i].createdAt,
						modifiedAt: data[i].modifiedAt,
						modifiedBy: data[i].odUserFirstName + " " + data[i].odUserLastName,
						bedit: false
				};
				
				$scope.alertDataTypes.push ( alerttype );
				
			}
			
		});
		
	}
	
	$scope.onDeleteAlertType = function ( $index ) {
		
		bootbox.confirm('Are you sure?', function(result) {
			
			if ( result ) {
				
				$http.post( $scope.baseUrl + "settings/deleteAlertType/" + $scope.alertDataTypes[$index].id, {})
					.success( function ( data, s,h,c) {
						
						if ( data.result == 'success') {
							
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["success"]("Alert data type deleted successfully!", "Success");
							
							$scope.alertDataTypes.splice( $index, 1);
						} else {
							
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["error"]("Sorry, failed to delete alert data type!", "Failure");
							
						}
						
					} );
			}
		});
		
	}
	
	$scope.addNewAlertDataType = function () {
		
		$("#formNewAlertDataType").find("#submitButton").html('Waiting...').addClass('disabled');
		
		$("#formNewAlertDataType").ajaxSubmit({
			success: function ( data, s,h,c) {
				$("#formNewAlertDataType").find("#submitButton").html('Save').removeClass('disabled');
				
				var result= JSON.parse( data );
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				if ( result.result == 'success') {
					
					toastr["success"]("Alert data type added successfully!", "Success");
					
					var alertType = {
							id: result.alertType.id,
							type: result.alertType.type,
							description: result.alertType.description,
							createdAt: result.alertType.createdAt,
							modifiedAt: result.alertType.modifiedAt,
							modifiedBy: result.alertType.odUserFirstName + " " + result.alertType.odUserLastName,
							bedit: false
					};
					
					$scope.alertDataTypes.push( alertType );
					
					$scope.$apply();
					
					$("#idDialog").modal( 'hide' );
					
					$("#formNewAlertDataType").find("input[type='text']").val("");
					$("#formNewAlertDataType").find("textarea").val("");
					
				} else {
					
					toastr["error"]("Sorry, failed to add new alert data type!", "Failure");
				}
				
				
			}
		});
	}
	
	$scope.onUpdateAlertDataType = function ( $index ) {
		
		var objUpdateForm = $(document).find("#editAlertTypeForm_" + $index).eq(0);
		
		objUpdateForm.ajaxSubmit({
			success: function ( data, s,h,c ) {
				
				var result= JSON.parse( data );
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				if ( result.result == 'success') {
					
					toastr["success"]("Alert data type updated successfully!", "Success");

				} else {
					
					toastr["error"]("Sorry, failed to update alert data type!", "Failure");
				}
			}
		});
	}
});