angular.module('CSMainApp', ['ngSanitize'])
    .controller('MainController', function ( $scope, $http, $timout ) {

        $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
            // $('.make-switch').bootstrapSwitch();
            // $('.tooltips').tooltip();
        });

    }).directive('onFinishRender', function($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if ( scope.$last == true ) {
                    $timeout( function() {
                        scope.$emit('ngRepeatFinished');
                    });
                }
            }
        };
    });