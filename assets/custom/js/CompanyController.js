angular.module('CSMainApp', ['ngSanitize'])
.controller('CompanyController', function ( $scope, $http, $timeout ) {
	
	$scope.baseUrl = "";
	
	$scope.companies = [];

    $scope.userRole = '3';
	
	$scope.addCompanyInfo = {
		
	};
	
	$scope.init = function (strBaseUrl, strUserRole) {
		
		$scope.baseUrl = strBaseUrl;

        $scope.userRole = strUserRole;
		
		$scope.getCompanyList();
		
		$("#idNewCompany").modal({'show': false});
		var error1 = $('.alert-danger', $("#formNewCompany"));
		$("#formNewCompany").validate({
	        errorElement: 'span', //default input error message container
	        errorClass: 'help-block help-block-error', // default input error message class
	        focusInvalid: false, // do not focus the last invalid input
	        ignore: "",  // validate all fields including form hidden input
	        rules: {
	            orgId: {
	                minlength: 2,
	                required: true
	            },
	            userName: {
	                required: true,
	                email: false
	            },
	            password: {
	                required: true,
	                email: false
	            }
	        },

	        invalidHandler: function (event, validator) { //display error alert on form submit              
	            error1.show();
	        },

	        errorPlacement: function (error, element) { // render error placement for each input type
	            
	        },

	        highlight: function (element) { // hightlight error inputs
	            $(element)
	                .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
	        },

	        unhighlight: function (element) { // revert the change done by hightlight
	            
	        },

	        success: function (label, element) {
	            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
	        },

	        submitHandler: function (form) {
	            error1.hide();
	            $scope.addNewCompany();
	        }
	    });
	}
	
	$scope.getCompanyList = function() {
		$http.post( $scope.baseUrl + "companies/getList", {})
			.success(function(data,status,headers,config) {
				for ( var i = 0; i < data.length; i ++) {
					var companyInfo = {
						id: data[i].id,
						orgId: data[i].orgId,
						originId: data[i].originId,
						orgName: data[i].orgName,
						creatorId: data[i].creatorId,
						mspOrgId: data[i].mspOrgId,
						orgTypeId: data[i].orgTypeId,
						odCreatedAt: data[i].odCreatedAt,
						odModifiedAt: data[i].odModifiedAt,
						hasDelegatedAdmin: data[i].hasDelegatedAdmin,
						enabledFireEye: data[i].enabledFireEye=="Y"?true:false,
						networkCount: data[i].networkCount,
						odToken: data[i].odToken,
						apiKey: data[i].apiKey,
						userId: data[i].userId,
						passwd: data[i].passwd,
						enabledWhiteList: data[i].enabledWhiteList=='Y'?true:false,
						whiteListVal: data[i].whiteListVal,
						sharedThreatIntel: data[i].sharedThreatIntel=='Y'?true:false,
						require2FA: data[i].require2FA=="Y"?true:false,
						connectWiseId: data[i].connectWiseId,
						customApiKey: data[i].customApiKey=='Y'?true:false,
						daysAlertActive: data[i].daysAlertActive,
						bedit: false,
						bshowapikey: false,
						bshowtoken: false
					};
					
					$scope.companies.push( companyInfo );
				}
                $("#idWaiting").modal('hide');
			});
	}
	
	$scope.onDeleteCompany = function ( ind ) {
		bootbox.confirm("Are you sure!", function(result){
			if ( result ) {
				$http.post( $scope.baseUrl + "companies/delete/" + $scope.companies[ind].id, {})
					.success(function(data,status,headers,config){
						toastr.options = {
								"closeButton" : true,
								"positionClass" : "toast-top-right",
								"onclick" : null,
								"showDuration" : "1000",
								"hideDuration" : "1000",
								"showEasing": "swing",
								"hideEasing" : "linear",
								"showMethod" : "fadeIn",
								"hideMethod" : "fadeOut"
								};
						toastr["success"]("Company information removed successfully!", "Success");
						
						$scope.companies.splice(ind,1);
						//$timeout(function(){$('.make-switch').bootstrapSwitch();});
					});
			}
		});
	}
    $scope.refreshCompanies = function() {
        $("#idWaiting").modal('show');
        $http.post($scope.baseUrl + 'companies/refresh_all', {})
            .success(function (data,s,h,c){
                toastr.options = {
                    "closeButton" : true,
                    "positionClass" : "toast-top-right",
                    "onclick" : null,
                    "showDuration" : "1000",
                    "hideDuration" : "1000",
                    "showEasing": "swing",
                    "hideEasing" : "linear",
                    "showMethod" : "fadeIn",
                    "hideMethod" : "fadeOut"
                };
                toastr["success"]("Company information reloaded successfully!", "Success");
                $scope.companies = [];
                $scope.getCompanyList();
            });

    }
	
	$scope.onUpdateCompany = function ( ind, companyId ) {
		var strFromId = "editForm_" + ind;
		var objForm = $(document).find("#" + strFromId);
		objForm.ajaxSubmit({
			success: function ( data, statusText, xhr, $form ) {
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				toastr["success"]("Company information updated successfully!", "Success");
			},
			error: function(err) {
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				toastr["danger"]("Failed to update company information!", "Failure");
			}
		});
	}
	
	$scope.onAddNewCompany = function() {
		$("#idNewCompany").modal('show');
	}
	
	$scope.addNewCompany = function() {
		$("#formNewCompany").find("button[type='submit']").html('Waiting...').addClass('disabled');
		$("#formNewCompany").ajaxSubmit({
			timeout: 10000,
			success: function( data, statustext, xhr, $form) {
				$("#formNewCompany").find("button[type='submit']").html('Save').removeClass('disabled');
				var result = JSON.parse( data );
				if (result.result == 'success') {
					$("#idNewCompany").modal('hide');
					toastr.options = {
							"closeButton" : true,
							"positionClass" : "toast-top-right",
							"onclick" : null,
							"showDuration" : "1000",
							"hideDuration" : "1000",
							"showEasing": "swing",
							"hideEasing" : "linear",
							"showMethod" : "fadeIn",
							"hideMethod" : "fadeOut"
							};
					toastr["success"]("Company information added successfully!", "Success");
					
					var companyInfo = {
							id: result.companyInfo.id,
							orgId: result.companyInfo.orgId,
							originId: result.companyInfo.originId,
							orgName: result.companyInfo.orgName,
							creatorId: result.companyInfo.creatorId,
							mspOrgId: result.companyInfo.mspOrgId,
							orgTypeId: result.companyInfo.orgTypeId,
							odCreatedAt: result.companyInfo.odCreatedAt,
							odModifiedAt: result.companyInfo.odModifiedAt,
							hasDelegatedAdmin: result.companyInfo.hasDelegatedAdmin,
							enabledFireEye: result.companyInfo.enabledFireEye=="Y"?true:false,
							networkCount: result.companyInfo.networkCount,
							odToken: result.companyInfo.odToken,
							apiKey: result.companyInfo.apiKey,
							userId: result.companyInfo.userId,
							passwd: result.companyInfo.passwd,
							enabledWhiteList: result.companyInfo.enabledWhiteList=='Y'?true:false,
							whiteListVal: result.companyInfo.whiteListVal,
							sharedThreatIntel: result.companyInfo.sharedThreatIntel=='Y'?true:false,
							require2FA: result.companyInfo.require2FA=="Y"?true:false,
							connectWiseId: result.companyInfo.connectWiseId,
							customApiKey: result.companyInfo.customApiKey=='Y'?true:false,
							daysAlertActive: result.companyInfo.daysAlertActive,
							bedit: false,
							bshowapikey: false,
							bshowtoken: false
						};
					
					$scope.companies.push( companyInfo );
					
					$scope.$apply();
					
				} else {
					toastr.options = {
							"closeButton" : true,
							"positionClass" : "toast-top-right",
							"onclick" : null,
							"showDuration" : "1000",
							"hideDuration" : "1000",
							"showEasing": "swing",
							"hideEasing" : "linear",
							"showMethod" : "fadeIn",
							"hideMethod" : "fadeOut"
							};
					toastr["error"]( result.msg, "Failed" );
				}
				
			}
			
		});
		return false;
	}
	
	$scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
		$('.make-switch').bootstrapSwitch();
		$('.tooltips').tooltip();
	});
})
.directive('onFinishRender', function($timeout) {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			if ( scope.$last == true ) {
				$timeout( function() {
					scope.$emit('ngRepeatFinished');
				});
			}
		}
	};
});