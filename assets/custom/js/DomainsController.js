angular.module('CSMainApp', ['ngSanitize'])
.controller('DomainsController', function ( $scope, $http, $timeout ) {
	$scope.companyId = "";
	
	$scope.domainsList = [];
	
	$scope.baseUrl = "";

    $scope.userRole = '3';
	
	$scope.accessOptions = [{
		label: 'Access', val: 'allow'
	},
	{
		label: 'Block', val: 'block'
	}];
	
	$scope.sourceTypes = [];
	
	// Subdomain
	$scope.domainListId = "";
	
	$scope.subDomains = [];
	
	
	$scope.init = function ( strBaseUrl, strCompanyId, strUserRole) {
		
		$scope.companyId = strCompanyId;
		
		$scope.baseUrl = strBaseUrl;

        $scope.userRole = strUserRole;
		
		$scope.loadSourceTypes();
		
		//$scope.loadDomainsList();
		
	}

    $scope.onRefreshDomainList = function () {
        $("#idWaiting").modal('show');
        $http.post($scope.baseUrl + 'domains/refresh/' + $scope.companyId, {})
            .success(function(data,s,h,c) {
                $scope.domainsList = [];
                $scope.loadDomainsList();
                toastr.options = {
                    "closeButton" : true,
                    "positionClass" : "toast-top-right",
                    "onclick" : null,
                    "showDuration" : "1000",
                    "hideDuration" : "1000",
                    "showEasing": "swing",
                    "hideEasing" : "linear",
                    "showMethod" : "fadeIn",
                    "hideMethod" : "fadeOut"
                };
                toastr["success"]("Domain lists reloaded successfully!", "Success");
            });

    }
	
	$scope.loadSourceTypes = function () {
		
		$http.post( $scope.baseUrl + 'settings/getsourcetypelist', {})
			.success(function(data,sts,header,$config) {
				
				$scope.sourceTypes = data;
				
				$scope.loadDomainsList();
				
			});
		
	}
	
	$scope.getSelectedAccessOption = function ( strValue ) {
		
		for ( var i = 0; i < $scope.accessOptions.length; i ++ ) {
			
			if ( $scope.accessOptions[i].val == strValue ) {
				return $scope.accessOptions[i];
			}
			
		}
		
		return $scope.accessOptions[0];
		
	}
	
	$scope.getSelectedSourceType = function ( strId ) {
		
		for ( var i = 0; i < $scope.sourceTypes.length; i ++ ) {
		
			if ( $scope.sourceTypes[i].id == strId ) {
				
				return $scope.sourceTypes[i];
				
			}
			
		}
		
		return $scope.sourceTypes[i];
		
	}
	
	$scope.loadDomainsList = function () {
		$http.post( $scope.baseUrl + "domains/getListJson/" + $scope.companyId, {})
			.success(function(data,status,headers,config) {
				for ( var i = 0; i < data.length; i ++ ) {
					var domainList = {
							id: data[i].id,
							odAccess: $scope.getSelectedAccessOption( data[i].odAccess ),
							domainLimit: data[i].domainLimit,
							domainListSync: data[i].domainListSync=="Y" ? true:false,
							odCreatedAt: data[i].odCreatedAt,
							odId: data[i].odId,
							odModifiedAt: data[i].odModifiedAt,
							odName: data[i].odName,
							sTypeName: data[i].sTypeName,
							sDesc: data[i].sDesc,
							bedit: false,
							odisGlobal: data[i].odisGlobal,
							sourceType: $scope.getSelectedSourceType(data[i].sourceTypeId)
					};
					
					$scope.domainsList.push( domainList );
				}

                $("#idWaiting").modal('hide');
			});
	}
	
	$scope.onAddNewDomainList = function () {
		
		$("#idNewDomainlist").modal('show');
		
	}
	
	$scope.AddNewDomainList = function () {
		
		$("#formNewDomainlist").find("#submitButton").html("Waiting...").addClass("disabled");
		
		
		$("#formNewDomainlist").ajaxSubmit({
			
			success: function( data, status, headers, $form) {
				
				$("#formNewDomainlist").find("#submitButton").html("Save");
				
				var result = JSON.parse( data );
				
				if ( result.result == 'success' ) {
					$("#idNewDomainlist").modal('hide');
					toastr.options = {
							"closeButton" : true,
							"positionClass" : "toast-top-right",
							"onclick" : null,
							"showDuration" : "1000",
							"hideDuration" : "1000",
							"showEasing": "swing",
							"hideEasing" : "linear",
							"showMethod" : "fadeIn",
							"hideMethod" : "fadeOut"
							};
					toastr["success"]("New Domainlist added successfully!", "Success");
					
					var domainList = {
							id: result.domainList.id,
							odAccess: $scope.getSelectedAccessOption ( result.domainList.odAccess ),
							domainLimit: result.domainList.domainLimit,
							domainListSync: result.domainList.domainListSync=="Y" ? true:false,
							odCreatedAt: result.domainList.odCreatedAt,
							odId: result.domainList.odId,
							odModifiedAt: result.domainList.odModifiedAt,
							odName: result.domainList.odName,
							sTypeName: result.domainList.sTypeName,
							sDesc: result.domainList.sDesc,
							bedit: false,
							odisGlobal: result.domainList.odisGlobal
					};
					
					$scope.domainsList.push ( domainList );
					
					$scope.$apply();
					
				} else {
					toastr.options = {
							"closeButton" : true,
							"positionClass" : "toast-top-right",
							"onclick" : null,
							"showDuration" : "1000",
							"hideDuration" : "1000",
							"showEasing": "swing",
							"hideEasing" : "linear",
							"showMethod" : "fadeIn",
							"hideMethod" : "fadeOut"
							};
					toastr["error"](result.msg, "Failure");
	 			}
			},
			error: function ( result ) {
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				toastr["error"]("Sorry, failed to add new domainlist", "Failure");
			}
		});
	}
	
	$scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
	
		$('.make-switch').bootstrapSwitch();
		
	});
	
	$scope.onRemoveDomainList = function ( $index ) {
		
		bootbox.confirm("Are you sure?", function(result) {
			
			if ( result ) {
				
				$http.post( $scope.baseUrl + "domains/removeDomainList/" + $scope.domainsList[$index].id, {})
					.success(function (data, statusText, headers, $form) {
						// var result = JSON.parse( data );
						if ( data.result == 'success' ) {
							
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["success"]("Domainlist deleted successfully!", "Success");
							
							$scope.domainsList.splice( $index, 1);
							
						} else {
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["error"]("Sorry, failed to delete domainlist", "Failure");
						}
					});
				
			} else {
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				toastr["error"]("Sorry, failed to delete domainlist", "Failure");
			}
		});
		
	}
	
	$scope.onUpdateDomainList = function ( $index ) {
		
		var objEditForm = $(document).find("#editDomainForm_" + $index).eq(0);
		
		objEditForm.ajaxSubmit({
			success: function ( data, statusText, headers, $config ) {
				
				console.log ( data );
				
			}
		});
		
	}
	
	// subdomain page
	$scope.subdomain_init = function ( strBaseUrl, strCompanyId, strDomainId ) {
		
		$scope.baseUrl = strBaseUrl;
		
		$scope.companyId = strCompanyId;
		
		$scope.domainListId = strDomainId;
		
		$scope.loadSubDomains();
	}
	
	$scope.loadSubDomains = function() {
		$http.post( $scope.baseUrl + "domains/subdomainsJson/" + $scope.domainListId, {})
			.success( function( data, statustext, headers, $config ) {
				for ( var i = 0; i < data.length; i ++ ) {
					var domainInfo = {
							id: data[i].id,
							odId: data[i].odId,
							name: data[i].name
					};
					
					$scope.subDomains.push ( domainInfo );
				}
			});
	}
	
	$scope.onRemoveSubDomain = function ( $index ) {
		
		bootbox.confirm('Are you sure?', function( result ) {
			if ( result ) {
				$.ajax({
					url: $scope.baseUrl + "domains/removeDomain", 
					type: "post",
					data: {companyId: $scope.companyId, domainListId: $scope.domainListId, domainId: $scope.subDomains[$index].id, domainName: $scope.subDomains[$index].name},
					success: function(data) {
						var result = JSON.parse( data );
						
						if ( result.result == 'success' ) {
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["success"]("Deleted successfully!", "Success");
							
							$scope.subDomains.splice( $index, 1 );
							
							$scope.$apply();
						} else {
							
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["error"]("Sorry, failed to delete!", "Failure");
							
						}
					}
				});
				
			}
		});
		
	}
	
	$scope.onAddNewSubDomain = function () {
		$("#idNewSubDomain").modal("show");
	}
	
	$scope.AddNewSubDomain = function () {
		
		$("#formNewSubDomain").find("#submitButton").html("Waiting...").addClass('disabled');
		
		$("#formNewSubDomain").ajaxSubmit({
		
			success: function( data,s,h,c) {
				
				var result = JSON.parse( data );
				
				if ( result.result == 'success' ) {
					
					$("#idNewSubDomain").modal("hide");
					
					var domainInfo = {
							id: result.domain.id,
							odId: result.domain.odId,
							name: result.domain.name
					};
					
					$scope.subDomains.push ( domainInfo );
					
					$scope.$apply();

					toastr.options = {
							"closeButton" : true,
							"positionClass" : "toast-top-right",
							"onclick" : null,
							"showDuration" : "1000",
							"hideDuration" : "1000",
							"showEasing": "swing",
							"hideEasing" : "linear",
							"showMethod" : "fadeIn",
							"hideMethod" : "fadeOut"
							};
					toastr["success"]("New domain added successfully!", "Success");
					
				} else {
					toastr.options = {
							"closeButton" : true,
							"positionClass" : "toast-top-right",
							"onclick" : null,
							"showDuration" : "1000",
							"hideDuration" : "1000",
							"showEasing": "swing",
							"hideEasing" : "linear",
							"showMethod" : "fadeIn",
							"hideMethod" : "fadeOut"
							};
					toastr["error"]("Sorry, failed to add new domain", "Failure");
				}		
				$("#formNewSubDomain").find("#submitButton").removeClass('disabled').html("Save");
			}
		});
	}
})
.directive('onFinishRender', function($timeout) {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			if ( scope.$last == true ) {
				$timeout( function() {
					scope.$emit('ngRepeatFinished');
				});
			}
		}
	};
});