/**
 * Created by MasterDev on 4/7/15.
 */
angular.module('CSMainApp', ['ngSanitize'])
    .controller('AlertsController', function ( $scope, $http, $q ) {

        // Variables
        $scope.baseUrl          = '';
        $scope.alerts           = [];
        $scope.alertDataTypes   = [];
        $scope.iTypes           = [];
        $scope.sourceTypes      = [];

        $scope.alertInfo        = {
            id: null,
            info: null,
            notes: null,
            active: null,
            shared: null,
            alerttype: null,
            sourcetype: null,
            itype: null
        };

        // Functions
        $scope.init = function ( strBaseUrl ) {
            toastr.options = {
                "closeButton" : true,
                "positionClass" : "toast-top-right",
                "onclick" : null,
                "showDuration" : "1000",
                "hideDuration" : "1000",
                "showEasing": "swing",
                "hideEasing" : "linear",
                "showMethod" : "fadeIn",
                "hideMethod" : "fadeOut"
            };

            $scope.baseUrl = strBaseUrl;
            // Load all alert data types
            var ajaxLoadAlertDataTypes = $http.post( $scope.baseUrl + "settings/getalertdatatypes", {})
                .success(function (data, s, h, c) {
                    $scope.alertDataTypes = data;
                });
			// Load all iTypes
            var ajaxLoadiTypes = $http.post( $scope.baseUrl + "settings/getitypes", {})
                .success( function ( data, s, h ,c ) {
                    $scope.iTypes = data;
                });
			// Load all source types
            var ajaxLoadSourceTypes = $http.post( $scope.baseUrl + "settings/getsourcetypelist", {} )
                .success( function ( data, s, h ,c ) {
                    $scope.sourceTypes = data;
                });
			// if all ( alert data types, iTypes and source types ) are loaded, load all alerts
            $q.all([ajaxLoadAlertDataTypes, ajaxLoadiTypes, ajaxLoadSourceTypes]).then(function( result ) {
                // Load all alerts
                $http.post( $scope.baseUrl + "alerts/getalerts")
                    .success( function (data, s, h, c) {
                        $scope.alerts = [];
                        for ( var i = 0; i < data.length; i ++ ) {
                            var tmpAlert = {
                                id: data[i].id,
                                info: data[i].info,
                                createdAt: data[i].createdAt,
                                modifiedAt: data[i].modifiedAt,
                                notes: data[i].notes,
                                sourceName: data[i].sourceName,
                                active: data[i].active == 'Y'?true:false,
                                shared: data[i].shared =='Y'?true:false,
                                userId: data[i].cs_users_id,
                                userName: data[i].firstName + ' ' + data[i].lastName,
                                companyId: data[i].cs_companies_id,
                                comOrgId: data[i].odOrgComId,
                                alertType: $scope.getAlertType( data[i].cs_alerttype_id ),
                                sourceType: $scope.getSourceType( data[i].cs_sourcetype_id),
                                itypeType: $scope.getiTypeType( data[i].cs_itype_id),
                                comEmail: data[i].comEmail,
                                bedit: false
                            };

                            $scope.alerts.push( tmpAlert );
                        }
                });
            });
        };

        $scope.getAlertType = function ( alertTypeId ) {
            for ( var i = 0; i < $scope.alertDataTypes.length; i ++ ) {
                if ( alertTypeId == $scope.alertDataTypes[i].id ) {
                    return $scope.alertDataTypes[i];
                }
            }
            return null;
        };

        $scope.getSourceType = function ( sourceTypeId ) {
            for ( var i = 0; i < $scope.sourceTypes.length; i ++ ) {
                if ( sourceTypeId == $scope.sourceTypes[i].id ) {
                    return $scope.sourceTypes[i];
                }
            }
            return null;
        };

        $scope.getiTypeType = function ( itypeId ) {
            for ( var i = 0; i < $scope.iTypes.length; i ++ ) {
                if ( itypeId == $scope.iTypes[i].id ) {
                    return $scope.iTypes[i];
                }
            }

            return null;
        };

        // Upate alert
        $scope.onUpdateAlert = function ( $index ) {
            var obj_form = $( '#editAlertsForm_' + $index );
            var error1 = $('.alert-danger', obj_form);
            var success1 = $('.alert-success', obj_form);

            var ret = obj_form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    info: {
                        required: true
                    },
                    notes: {
                        required: true
                    },
                    sourceName: {
                        required: true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    error1.show();
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    // success1.show();
                    error1.hide();

                    obj_form.ajaxSubmit({
                        success: function( data, s, h, c) {

                            var result = JSON.parse( data );
                            if ( result.result == 'success' ) {
                                toastr["success"]("Alert information updated successfully!", "Success");
                            } else {
                                toastr["error"](result.msg, "Failure");
                            }
                        }
                    });
                    return false;
                }
            });

            obj_form.submit();
        };

        // Remove alert
        $scope.onRemoveAlert = function ( $index ) {
			bootbox.confirm("Are you sure!", function(result){
				if ( result ) {
					$http({
						method: 'POST',
						url: $scope.baseUrl + "alerts/delete/" + $scope.alerts[$index].id
					}).success( function ( data, s, h , c) {
						toastr["success"]("Alert information removed successfully!", "Success");
						$scope.alerts.splice( $index, 1 );
					});
				}
			});
        };
		
		// Add New alert function
		$scope.onAddNewAlert = function () {
			//formAddAlert
			var obj_form = $( '#formAddAlert' );
            var error1 = $('.alert-danger', obj_form);
            var success1 = $('.alert-success', obj_form);

            var ret = obj_form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    info: {
                        required: true
                    },
                    notes: {
                        required: true
                    },
                    sourceName: {
                        required: true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    error1.show();
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    // success1.show();
                    error1.hide();

                    obj_form.ajaxSubmit({
                        success: function( data, s, h, c) {

                            var result = JSON.parse( data );
                            if ( result.result == 'success' ) {
                                toastr["success"]("Alert information added successfully!", "Success");
								var row = {
									id: result.alertInfo.id,
									info: result.alertInfo.info,
									createdAt: result.alertInfo.createdAt,
									modifiedAt: result.alertInfo.modifiedAt,
									notes: result.alertInfo.notes,
									sourceName: result.alertInfo.sourceName,
									active: result.alertInfo.active == 'Y'?true:false,
									shared: result.alertInfo.shared =='Y'?true:false,
									userId: result.alertInfo.cs_users_id,
									userName: result.alertInfo.firstName + ' ' + result.alertInfo.lastName,
									companyId: result.alertInfo.cs_companies_id,
									comOrgId: result.alertInfo.odOrgComId,
									alertType: $scope.getAlertType( result.alertInfo.cs_alerttype_id ),
									sourceType: $scope.getSourceType( result.alertInfo.cs_sourcetype_id),
									itypeType: $scope.getiTypeType( result.alertInfo.cs_itype_id),
									bedit: false
								};
								
								$scope.alerts.push( row );
								
								$("#idAddDialog").modal('hide');
								
								$scope.$apply();
                            } else {
                                toastr["error"](result.msg, "Failure");
                            }
                        }
                    });
                    return false;
                }
            });

            obj_form.submit();
		};
		
		// Upload new alert
		$scope.onUploadAlert = function () {
			var obj_form = $("#formUpload");
			
			obj_form.ajaxSubmit({
				success: function ( data, s, h, c ) {
					console.log ( data );
					toastr["success"]("Alert information added successfully!", "Success");
				}
			});
		};
		
		// Set real value to the switch control for active
        $scope.isActive = function ( bActive ) {
            return bActive == true ? 'Y' : 'N';
        };
		// Set real value to the switch control for shared
        $scope.isShared = function ( bShared ) {
            return bShared == true ? 'Y' : 'N';
        };
		// ngRepeat finish event
        $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
            // $('.make-switch').bootstrapSwitch();
            // $('.tooltips').tooltip();
        });

    })
	// Directives for AlertsController
	.directive('onFinishRender', function($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if ( scope.$last == true ) {
                    $timeout( function() {
                        scope.$emit('ngRepeatFinished');
                    });
                }
            }
        };
    }).directive('bootstrapSwitch', [
        function() {
            return {
                restrict: 'A',
                require: '?ngModel',
                link: function(scope, element, attrs, ngModel) {
                    element.bootstrapSwitch();

                    element.on('switchChange.bootstrapSwitch', function(event, state) {
                        if (ngModel) {
                            scope.$apply(function() {
                                ngModel.$setViewValue(state);
                            });
                        }
                    });

                    scope.$watch(attrs.ngModel, function(newValue, oldValue) {
                        if (newValue) {
                            element.bootstrapSwitch('state', true, true);
                        } else {
                            element.bootstrapSwitch('state', false, true);
                        }
                    });
                }
            };
        }
    ]);