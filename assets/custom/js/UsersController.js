/**
 * Created by MasterDev on 3/23/15.
 */
angular.module('CSMainApp', ['ngSanitize'])
    .controller('UsersController', function ( $scope, $http, $timeout) {

        $scope.baseUrl = '';

        $scope.userslist = [];

        $scope.userRole = '4';

        $scope.userRoles = [
            {id: 2, roleName: 'Read Only Admin'},
            {id: 3, roleName: 'User Role'},
            {id: 4, roleName: 'Read Only User Role'}
        ];

        $scope.companies = [];

        toastr.options = {
            "closeButton" : true,
            "positionClass" : "toast-top-right",
            "onclick" : null,
            "showDuration" : "1000",
            "hideDuration" : "1000",
            "showEasing": "swing",
            "hideEasing" : "linear",
            "showMethod" : "fadeIn",
            "hideMethod" : "fadeOut"
        };

        $scope.init = function ( strBaseUrl, strUserRole ) {

            $scope.baseUrl = strBaseUrl;

            $scope.userRole = strUserRole;

            $scope.loadCompanies();

        };

        $scope.loadCompanies = function () {

            $http.post($scope.baseUrl + 'companies/getList', {})
                .success(function(data, s, h, c) {

                    for ( var i = 0; i < data.length; i ++) {
                        var company = {
                            'id': data[i].id,
                            'name': data[i].orgName
                        };

                        $scope.companies.push( company );
                    }

                    $scope.loadUsers();
                });
        };

        $scope.loadUsers = function () {

            $http.post( $scope.baseUrl + "users/getlist", {})
                .success( function( data, s, h, c) {

                    for ( var i = 0; i < data.length; i ++ ) {
                        var userInfo = {
                            id: data[i].id,
                            odUserFirstName: data[i].odUserFirstName,
                            odUserLastName: data[i].odUserLastName,
                            odUserEmail: data[i].odUserEmail,
                            odUser2FA: data[i].odUser2FA=='N'?false:true,
                            user2FA: data[i].user2FA=='N'?false:true,
                            primaryContact: data[i].primaryContact=='N'?false:true,
                            odUserStatus: data[i].odUserStatus,
                            odUserRole: data[i].cs_role_id,
                            roleName: data[i].roleName,
                            companyName: data[i].orgName,
                            companyInfo: $scope.getCompanyInfo( data[i].cs_companies_id),
                            roleInfo: $scope.getRoleInfo( data[i].cs_role_id),
                            orgId: data[i].odComOrgId,
                            companyId: data[i].cs_companies_id,
                            createdAt: data[i].createdAt,
                            modifiedAt: data[i].modifiedAt,
                            lastLogin: data[i].lastLogin,
                            isOdUser: data[i].isOdUser,
                            bedit: false
                        };
                        $scope.userslist.push( userInfo );

                    }

                });
        };

        $scope.getCompanyInfo = function( companyId ) {

            for ( var i = 0; i < $scope.companies.length; i ++ ) {

                if ( $scope.companies[i].id == companyId ) {
                    return $scope.companies[i];
                }

            }

            return $scope.companies[0];
        };

        $scope.getRoleInfo = function ( roleId ) {

            for ( var i = 0; i < $scope.userRoles.length; i ++) {

                if ( $scope.userRoles[i].id == roleId ) {

                    return $scope.userRoles[i];
                }

            }

            return $scope.userRoles[0];

        };

        $scope.onRemoveUser = function ( ind ) {

            var userId = $scope.userslist[ind].id;

            $http.post( $scope.baseUrl + 'users/delete/' + userId, {})
                .success(function( data, s,h,c) {

                    if ( data.result == 'success') {

                        toastr.success('User account deleted successfully!', 'Success');

                        $scope.userslist.splice( ind, 1 );

                    } else {

                        toastr.error('Failed to delete user account!', 'Failure');
                    }


                });

        };
        
        $scope.onAddNewUser = function () {
        	
        	var objForm = $('#formNewUser');
        	
        	objForm.find('#submitButton').html('Waitting..').addClass('disabled');
        	
        	objForm.ajaxSubmit({
        		success: function ( data, s, h, c) {
        			result = JSON.parse( data );
        			if ( result.result == 'success') {
        				
        				toastr.success('User added successfully!', 'Success');
        				objForm.find('#submitButton').html('Save').removeClass('disabled');
        				objForm.find('input[type="text"]').val('');
        				$("#idDialog").modal('hide');
        			} else {
        				objForm.find('#submitButton').html('Save').removeClass('disabled');
        				toastr.error( data.msg, 'Failure' );
        			}
        			
        		}
        	});
        	
        };
        
        $scope.onUpdateUser = function ( $index ) {
        	
        	var objForm = $("#editUsersForm_" + $index );
        	
        	objForm.ajaxSubmit({
        		success: function ( data, s, h, c ) {
        			
        			var result = JSON.parse( data );
        			
        			if ( result.result == 'success' ) {
        				
        				toastr.success('Updated successfully!', 'Success');
        				
        			} else {
        				
        				toastr.error( result.msg, 'Failure');
        				
        			}
        			
        		}
        	});
        	
        };

        $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
            $('.make-switch').bootstrapSwitch();
        });
    }).directive('onFinishRender', function($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if ( scope.$last == true ) {
                    $timeout( function() {
                        scope.$emit('ngRepeatFinished');
                    });
                }
            }
        };
    });