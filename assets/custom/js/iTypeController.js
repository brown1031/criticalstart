angular.module('CSMainApp', ['ngSanitize'])
.controller('iTypeController', function ( $scope, $http ) {

	$scope.baseUrl = "";
	
	$scope.iTypes = [];
	
	$scope.init = function ( str_baseUrl ) {
		
		$scope.baseUrl = str_baseUrl;
		
		$scope.loadiTypes();
		
	}
	
	$scope.loadiTypes = function () {
		
		$http.post( $scope.baseUrl + "settings/getitypes", {})
			.success(function(data,s,h,c){
				
			for ( var i = 0; i < data.length; i ++ ) {
				
				var itype = {
						id: data[i].id,
						itypeType: data[i].itypeType,
						description: data[i].description,
						createdAt: data[i].createdAt,
						modifiedAt: data[i].modifiedAt,
						modifiedBy: data[i].odUserFirstName + " " + data[i].odUserLastName,
						bedit: false
				};
				
				$scope.iTypes.push ( itype );
				
			}
			
		});
		
	}
	
	$scope.onDeleteiType = function ( $index ) {
		
		bootbox.confirm('Are you sure?', function(result) {
			
			if ( result ) {
				
				$http.post( $scope.baseUrl + "settings/deleteItype/" + $scope.iTypes[$index].id, {})
					.success( function ( data, s,h,c) {
						
						if ( data.result == 'success') {
							
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["success"]("iType deleted successfully!", "Success");
							
							$scope.iTypes.splice( $index, 1);
						} else {
							
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["error"]("Sorry, failed to delete iType!", "Failure");
							
						}
						
					} );
			}
		});
		
	}
	
	$scope.addNewiType = function () {
		
		$("#formNewiType").find("#submitButton").html('Waiting...').addClass('disabled');
		
		$("#formNewiType").ajaxSubmit({
			success: function ( data, s,h,c) {
				$("#formNewiType").find("#submitButton").html('Save').removeClass('disabled');
				
				var result= JSON.parse( data );
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				if ( result.result == 'success') {
					
					toastr["success"]("iType added successfully!", "Success");
					
					var iType = {
							id: result.iType.id,
							itypeType: result.iType.itypeType,
							description: result.iType.description,
							createdAt: result.iType.createdAt,
							modifiedAt: result.iType.modifiedAt,
							modifiedBy: result.iType.odUserFirstName + " " + result.iType.odUserLastName,
							bedit: false
					};
					
					$scope.iTypes.push( iType );
					
					$scope.$apply();
					
					$("#idDialog").modal( 'hide' );
					
					$("#formNewiType").find("input[type='text']").val("");
					$("#formNewiType").find("textarea").val("");
					
				} else {
					
					toastr["error"]("Sorry, failed to add new iType!", "Failure");
					
				}
				
			}
		});
	}
	
	$scope.onUpdateiType = function ( $index ) {
		
		var objUpdateForm = $(document).find("#editiTypeForm_" + $index).eq(0);
		
		objUpdateForm.ajaxSubmit({
			
			success: function ( data, s,h,c ) {
				
				var result= JSON.parse( data );
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				if ( result.result == 'success') {
					
					toastr["success"]("iType updated successfully!", "Success");

				} else {
					
					toastr["error"]("Sorry, failed to update iType!", "Failure");
				}
			}
		});
	}
});