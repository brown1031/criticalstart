angular.module('CSMainApp', ['ngSanitize'])
.controller('WhitelistController', function ( $scope, $http, $timeout ) {
	
	$scope.baseUrl = "";
	
	$scope.whitelists = [];
	
	$scope.init = function ( str_baseUrl ) {
		
		$scope.baseUrl = str_baseUrl;
		
		$scope.loadWhitelists();
		
	}
	
	$scope.loadWhitelists = function () {
		
		$http.post( $scope.baseUrl + "settings/getwhitelists", {})
			.success(function(data,s,h,c){
				
			for ( var i = 0; i < data.length; i ++ ) {
				
				var whitelist = {
						id: data[i].id,
						data: data[i].data,
						note: data[i].notes,
						createdAt: data[i].createdAt,
						modifiedAt: data[i].modifiedAt,
						bedit: false
				};
				
				$scope.whitelists.push ( whitelist );
				
			}
			
		});
		
	}
	
	$scope.onDeleteWhitelist = function ( $index ) {
		
		bootbox.confirm('Are you sure?', function(result) {
			
			if ( result ) {
				
				$http.post( $scope.baseUrl + "settings/deleteWhitelist/" + $scope.whitelists[$index].id, {})
					.success( function ( data, s,h,c) {
						
						if ( data.result == 'success') {
							
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["success"]("Whitelist deleted successfully!", "Success");
							
							$scope.whitelists.splice( $index, 1);
						} else {
							
							toastr.options = {
									"closeButton" : true,
									"positionClass" : "toast-top-right",
									"onclick" : null,
									"showDuration" : "1000",
									"hideDuration" : "1000",
									"showEasing": "swing",
									"hideEasing" : "linear",
									"showMethod" : "fadeIn",
									"hideMethod" : "fadeOut"
									};
							toastr["error"]("Sorry, failed to delete whitelist!", "Failure");
							
						}
						
					} );
			}
		});
		
	}
	
	$scope.addNewWhitelist = function () {
		
		$("#formNewWhitelist").find("#submitButton").html('Waiting...').addClass('disabled');
		
		$("#formNewWhitelist").ajaxSubmit({
			success: function ( data, s,h,c) {
				$("#formNewWhitelist").find("#submitButton").html('Save').removeClass('disabled');
				var result= JSON.parse( data );
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				if ( result.result == 'success') {
					
					toastr["success"]("Whitelist added successfully!", "Success");
					
					var whitelist = {
							id: result.whitelist.id,
							data: result.whitelist.data,
							note: result.whitelist.notes,
							createdAt: result.whitelist.createdAt,
							modifiedAt: result.whitelist.modifiedAt,
							bedit: false
					};
					
					$scope.whitelists.push( whitelist );
					
					$scope.$apply();
					
					$("#idDialog").modal( 'hide' );
					
					$("#formNewWhitelist").find('input[type="text"]').val("");
					$("#formNewWhitelist").find('textarea').val("");
					
				} else {
					
					toastr["error"](result.msg, "Failure");
				}
				
				
			}
		});
	}
	
	$scope.onUpdateWhitelist = function ( $index ) {
		
		var objUpdateForm = $(document).find("#editWhitelistForm_" + $index).eq(0);
		
		objUpdateForm.ajaxSubmit({
			
			success: function ( data, s,h,c ) {
				
				var result= JSON.parse( data );
				toastr.options = {
						"closeButton" : true,
						"positionClass" : "toast-top-right",
						"onclick" : null,
						"showDuration" : "1000",
						"hideDuration" : "1000",
						"showEasing": "swing",
						"hideEasing" : "linear",
						"showMethod" : "fadeIn",
						"hideMethod" : "fadeOut"
						};
				if ( result.result == 'success') {
					
					toastr["success"]("Whitelist updated successfully!", "Success");

				} else {
					
					toastr["error"]("Sorry, failed to update whitelist!", "Failure");
				}
			}
		});
	}
});