<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuration extends CI_Controller {

	public function index()
	{
		$data['main_content'] = 'configuration/configuration';
		$data['page'] = 'configuration';
		$this->load->view('includes/template', $data);
	}
}