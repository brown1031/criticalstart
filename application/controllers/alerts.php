<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alerts extends CI_Controller {
	
	public function __construct() {
		
		parent::__construct();
		
		$this->load->model('alerts_model');
		
	}
	
	public function index()
	{

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true ) {

            redirect('users/login');

        }

		$data['main_content'] = 'alerts/alerts';
		
		$data['page'] = 'alerts';

        $data['jsPath'] = 'alerts/alertsJs';
		
		$data['alerts'] = $this->alerts_model->getlist( 'N' );
		
		$this->load->view('includes/template', $data);
		
	}
	
	public function alertexps() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true ) {

            redirect('users/login');

        }
		
		$data['main_content'] = 'alerts/alert_exceptions';
		
		$data['page'] = 'alertexps';
		
		$this->load->view('includes/template', $data);
		
	}
	
	public function getalerts() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true ) {

            die ( json_encode( array('result'=>'failed', 'msg'=>'Invalid user account') ) );
        }
		
		$result = $this->alerts_model->getlist('N');
		
		die( json_encode ( $result ) );
		
	}
	
	public function add() {
		
		if ( $this->session->userdata('userRole' ) != USER_SUPERADMIN ) {
			
			die( json_encode( array('result'=>'failed', 'msg' => 'Failed to add new alert!') ) );
			
		}
		
		$result = $this->alerts_model->add();
		
		die( json_encode( $result ) );
		
	}

    public function update() {

        $result = $this->alerts_model->update();

        die( json_encode( $result ) );
    }
	
	public function delete( $alertId ) {
		
		if ( $this->session->userdata('userRole') != USER_SUPERADMIN ) {
			
			die( json_encode( array( 'result' => 'success', 'msg'=> 'Failed to delete!') ) );
			
		}
		
		$result = $this->alerts_model->delete( $alertId );
		
		die( json_encode( $result ) );
		
	}
	
	public function upload( $token ) {
		
		printLog('error', $token);
		
		if ( !$token ) { 
			die( 'failed' );
		}
		
		$this->load->model('companies_model');
		
		$companyInfo = $this->companies_model->getCompanyByToken( $token );
		
		if ( !$companyInfo ) {
			die('wrong token id');
		}
		
		$addresses = $this->parseJSON();
		if ( $addresses == null ) 
			die('Invalid data');
		$this->load->model('alerts_model');
		
		foreach ( $addresses as $str_ip ) {			
			$this->alerts_model->saveFireEyeAddress( $str_ip, $companyInfo->id );		
		}
		die('success');
	}
	
	public function parseJSON() {
		$inputs = "";
		$fp = fopen('php://input','r');
		while (!feof($fp))
			$inputs .= fgets($fp);
		fclose($fp);
		
		printLog('error', 'PHP input - '. $inputs);
		$jsonstring = (preg_replace( "/\r|\n/", "", $inputs ));
		$jsonData = json_decode($jsonstring);
		if (!$jsonData) return null;
		$addresses = array();
		
		foreach( $jsonData->alert->explanation as $k => $v ) {
			if ( $k == 'cnc-services' ) {
				foreach ( $v as $kk => $vv ) {
					if ( $kk == 'cnc-service') {			
						$addresses[] = $vv->address;
					}
				}
			}
		}
		
		return $addresses;
	}
}
