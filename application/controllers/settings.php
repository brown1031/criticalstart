<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {
	
	public function __construct() {
		
		parent::__construct();
		
		$this->load->model('settings_model');
		
	}
	
	public function siteconfig()
	{
        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            redirect( 'users/login' );

        }

		$data['main_content'] = 'settings/settings';
		$data['page'] = 'settings_siteconfig';
		// $list = $this->companies_model->getList();
		// $data['companies'] = $list;
		$data['jsPath'] = 'settings/js/siteconfigJs';
		
		$siteconfig = $this->settings_model->getconfig();
		
		$data['siteconfig'] = $siteconfig;
		
		$this->load->view('includes/template', $data);
	}

	public function getsourcetypelist() {
		
		$this->load->model('sourcetypes_model');
		
		$result = $this->sourcetypes_model->getList();
		
		die( json_encode( $result ) );
		
	}
	
	public function update() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {
            die( json_encoe(array('result'=>'failed', 'msg'=>'Permission denied')) );
        }
		
		$result = $this->settings_model->updateconfig();
		
		die( json_encode($result) );
		
	}
	
	// Alert Data Types Start
	public function alertdatatypes() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            redirect('users/login');

        }

		$data['main_content'] = 'settings/alertdatatypes';
		$data['page'] = 'settings_alertdatatypes';
		// $list = $this->companies_model->getList();
		// $data['companies'] = $list;
		$data['jsPath'] = 'settings/js/alerttypeJs';
		$this->load->view('includes/template', $data);
	}
	
	public function getalertdatatypes() {

		$this->load->model('alerttypes_model');
		
		$result = $this->alerttypes_model->getList();
		
		die ( json_encode ( $result ) );
		
	}
	
	public function addNewAlertDataType() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            return json_encoe(array('result'=>'failed', 'msg'=>'Permission denied'));

        }
		$this->load->model('alerttypes_model');
		
		$result = $this->alerttypes_model->add();
		
		die ( json_encode( $result ) );
		
	}
	
	public function updateAlertDataTypes() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die( json_encode(array('result'=>'failed', 'msg'=>'Permission denied')) );

        }
		
		$this->load->model('alerttypes_model');
		
		$result = $this->alerttypes_model->update();
		
		die ( json_encode( $result ) );
		
	}
	
	public function deleteAlertType($alertTypeId) {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die( json_encode(array('result'=>'failed', 'msg'=>'Permission denied')) );

        }
		
		$this->load->model('alerttypes_model');
		
		$result = $this->alerttypes_model->delete( $alertTypeId );
		
		die( json_encode( $result ) );
		
	}
	// Alert Data Types End
	
	// User Role Start
	public function userroles() {
        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            redirect('users/login');

        }
		$data['main_content'] = 'settings/userroles';
		$data['page'] = 'settings_userroles';
		// $list = $this->companies_model->getList();
		// $data['companies'] = $list;
		$data['jsPath'] = 'settings/js/userRoleJs';
		$this->load->view('includes/template', $data);
	}
	
	public function getuserroles() {

		$this->load->model('roles_model');
		
		$result = $this->roles_model->getlist();
		
		die ( json_encode( $result ) );
		
	}
	
	public function updateUserRole() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die( json_encode(array('result'=>'failed', 'msg'=>'Permission denied')));

        }

		$this->load->model('roles_model');
		
		$result = $this->roles_model->update();
		
		die ( json_encode( $result ) );
		
	}
	
	public function deleteUserRole( $roleId ) {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die( json_encode(array('result'=>'failed', 'msg'=>'Permission denied')) );

        }

		$this->load->model('roles_model');
		
		$result = $this->roles_model->delete( $roleId );
		
		die ( json_encode( $result ) );
		
	}
	
	public function addNewUserRole() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die( json_encode(array('result'=>'failed', 'msg'=>'Permission denied')) );

        }
		$this->load->model('roles_model');
		
		$result = $this->roles_model->add();
		
		die ( json_encode( $result ) );
		
	}
	// User Role End
	
	// Source Type Start
	public function sourcetypes() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            redirect('users/login');

        }

		$data['main_content'] = 'settings/sourcetypes';

		$data['page'] = 'settings_sourcetypes';
		// $list = $this->companies_model->getList();
		// $data['companies'] = $list;
		$data['jsPath'] = 'settings/js/sourceTypeJs';

		$this->load->view('includes/template', $data);
	}
	
	public function getsourcetypes() {

		$this->load->model('sourcetypes_model');
		
		$result = $this->sourcetypes_model->getlist();
		
		die ( json_encode( $result ) );
	}

	public function addNewSourceType() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die(json_encode(array('result'=>'failed', 'msg'=>'Permission denied')));

        }
	
		$this->load->model('sourcetypes_model');
	
		$result = $this->sourcetypes_model->add();
	
		die ( json_encode( $result ) );
	
	}
	
	public function updateSourceType() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die( json_encode(array('result'=>'failed', 'msg'=>'Permission denied')));

        }
	
		$this->load->model('sourcetypes_model');
	
		$result = $this->sourcetypes_model->update();
	
		die ( json_encode( $result ) );
	
	}
	
	public function deleteSourceType($sourceTypeId) {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die( json_encode( array('result'=>'failed', 'msg'=>'Permission denied')));

        }
	
		$this->load->model('sourcetypes_model');
	
		$result = $this->sourcetypes_model->delete( $sourceTypeId );
	
		die( json_encode( $result ) );
	
	}
	// Source Type End
	
	// iType start

	public function itypes() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            redirect('users/login');

        }
		
		$data['main_content'] = 'settings/itypes';
		
		$data['page'] = 'settings_itypes';
		
		$data['jsPath'] = 'settings/js/iTypesJs';
		
		$this->load->view('includes/template', $data);
		
	}

	public function getitypes() {
	
		$this->load->model('itypes_model');
	
		$result = $this->itypes_model->getlist();
	
		die ( json_encode( $result ) );
	}
	
	public function addNewItype() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die( json_encode(array('result'=>'failed', 'msg'=>'Permission denied')));

        }
	
		$this->load->model('itypes_model');
	
		$result = $this->itypes_model->add();
	
		die ( json_encode( $result ) );
	
	}
	
	public function updateItype() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die( json_encode(array('result'=>'failed', 'msg'=>'Permission denied')));

        }
	
		$this->load->model('itypes_model');
	
		$result = $this->itypes_model->update();
	
		die ( json_encode( $result ) );
	
	}
	
	public function deleteItype( $iTypeId ) {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die( json_encode ( array ( 'result' => 'failed', 'msg' => 'Permission denied')));

        }
	
		$this->load->model('itypes_model');
	
		$result = $this->itypes_model->delete( $iTypeId );
	
		die( json_encode( $result ) );
	
	}
	// iType end
	
	// Domainwhitelist start

	public function domainwhitelist() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            redirect( 'users/login' );

        }
		$data['main_content'] = 'settings/domainwhitelist';
		$data['page'] = 'settings_domainwhitelist';
		// $list = $this->companies_model->getList();
		// $data['companies'] = $list;
		$data['jsPath'] = 'settings/js/whitelistJs';
		$this->load->view('includes/template', $data);
	}

	public function getwhitelists() {
	
		$this->load->model('whitelist_model');
	
		$result = $this->whitelist_model->getlist();
	
		die ( json_encode( $result ) );
	}
	
	public function addNewWhiteList() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die(json_encode(array('result'=>'failed', 'msg'=>'Permission denied')));

        }
	
		$this->load->model('whitelist_model');
	
		$result = $this->whitelist_model->add();
	
		die ( json_encode( $result ) );
	
	}
	
	public function updateWhitelist() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die(json_encode(array('result'=>'failed', 'msg'=>'Permission denied')));

        }
	
		$this->load->model('whitelist_model');
	
		$result = $this->whitelist_model->update();
	
		die ( json_encode( $result ) );
	
	}
	
	public function deleteWhitelist( $whitelistId ) {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die(json_encode(array('result'=>'failed', 'msg'=>'Permission denied')));

        }
	
		$this->load->model('whitelist_model');
	
		$result = $this->whitelist_model->delete( $whitelistId );
	
		die( json_encode( $result ) );
	
	}
	// Domainwhitelist end
	
	// Scripts start
	public function scripts() {
        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            redirect('users/login');

        }
		$data['main_content'] = 'settings/scripts';
		$data['page'] = 'settings_scripts';
		// $list = $this->companies_model->getList();
		// $data['companies'] = $list;
		$data['jsPath'] = 'settings/js/scriptsJs';
		$this->load->view('includes/template', $data);
	}
	
	public function getscripts() {
		
		$this->load->model('scripts_model');
		
		$result = $this->scripts_model->getlist();
		
		die( json_encode( $result ) );
		
	}
	
	public function addNewScript() {
        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die(json_encode(array('result'=>'failed', 'msg'=>'Permission denied')));

        }
		
		$this->load->model('scripts_model');
		
		$result = $this->scripts_model->add();
		
		die( json_encode( $result ) );
		
	}
	
	public function deleteScript( $scriptId ) {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die(json_encode(array('result'=>'failed', 'msg'=>'Permission denied')));

        }
		
		$this->load->model('scripts_model');
		
		$result = $this->scripts_model->delete( $scriptId );
		
		die( json_encode( $result ) );
		
	}
	
	public function updateScript() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true || $this->session->userdata('userRole') != USER_SUPERADMIN ) {

            die(json_encode(array('result'=>'failed', 'msg'=>'Permission denied')));

        }
		
		$this->load->model('scripts_model');
		
		$result = $this->scripts_model->update();
		
		die( json_encode( $result ) );
	}
	// Scripts end
}