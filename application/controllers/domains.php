<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Domains extends CI_Controller {
	
	public function __construct() {
		
		parent::__construct();
		
		$this->load->model('domains_model');
		
	}
	
	public function index()
	{

        $this->load->model('commons_model');

        if ( !$this->commons_model->isLoggedIn() ) {

            redirect('users/login');

        }

        $this->load->model('companies_model');

        $companyInfo = $this->companies_model->get( $this->session->userdata('companyId') );
		
		$data['main_content'] = 'domains/domains';
		
		$data['page'] = 'domains';
		
		$data['jsPath'] = 'domains/domainsJs';

        $data['companyInfo'] = $companyInfo;
		
		$this->load->view('includes/template', $data);
		
	}

    public function refresh( $companyId ) {

        $this->domains_model->retrieveAllDomainsFromOD( $companyId );

        die( json_encode(array('result'=>'success', 'msg'=>'')) );
    }
	
	public function domainlist() {
		
		$data['main_content'] = 'domains/domains';
		
		$data['page'] = 'company';
		
		$data['jsPath'] = 'domains/domainsJs';
		
		$this->load->view('includes/template', $data);
		
	}
	
	public function getList( $companyId ) {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true ) {

            redirect('users/login');

        }
		
		$domains = $this->domains_model->getList( $companyId );
		
		$data['main_content'] = 'domains/domains';
		
		$data['page'] = 'company';
		
		$this->load->model('companies_model');
		
		$data['companyInfo'] = $this->companies_model->get( $companyId );
		
		$data['jsPath'] = 'domains/domainsJs';
		
		$data['domains'] = $domains;
		
		$this->load->view('includes/template', $data);
		
	}
	
	public function getListJson ( $companyId ) {
		
		$domains = $this->domains_model->getList( $companyId );
		
		die( json_encode( $domains ) );
		
	}
	
	public function subdomains( $companyId, $domainId ) {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') ==false ) {

            redirect('users/login');

        }
		
		$subdomains = $this->domains_model->getSubDomains( $domainId );
		
		$data['main_content'] = 'domains/subdomains';
		
		$data['page'] = 'company';
		
		$data['jsPath'] = 'domains/domainsJs';
		
		$data['subdomains'] = $subdomains;
		
		$data['domainInfo'] = $this->domains_model->get( $domainId );
		
		$this->load->model('companies_model');
		
		$data['companyInfo'] = $this->companies_model->get( $companyId );
		
		$this->load->view('includes/template', $data);
		
	}
	
	public function subdomainsJson( $domainListId ) {
		
		$subdomains = $this->domains_model->getSubDomains( $domainListId );
		
		die( json_encode( $subdomains ) );
		
	}
	
	public function addNewDomainList( $companyId ) {
		
		die ( json_encode( $this->domains_model->addNewDomainList( $companyId ) ));
		
	}
	
	public function removeDomainList ( $domainId ) {
		
		$result = $this->domains_model->removeDomainList( $domainId );
		
		die ( json_encode ( $result ) );
		
	}
	
	public function addNewDomain( $companyId, $domainListId ) {
		
		die ( json_encode( $this->domains_model->addNewDomain( $companyId, $domainListId) ) );
		
	}
	
	public function removeDomain( ) {
		
		$result = $this->domains_model->removeDomain();
		
		die( json_encode( $result ) );
		
	}
	
	// Update Domainlist
	public function update(  ) {
		
		$result = $this->domains_model->update(  );
		
		die ( json_encode( $result ) );
		
	}
}