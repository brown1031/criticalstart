<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {
	
	public function __construct() {
		
		parent::__construct();
		
		$this->load->model("users_model");
		
	}
	
	public function index() {

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') != true ) {

            redirect( 'users/login' );

        }

		$data['main_content'] = 'users/list';
		
		$data['page'] = 'users_list';

        $data['jsPath'] = 'users/usersJs';
		
		$this->load->view('includes/template', $data);
	}

    public function getlist() {

        $this->load->model('commons_model');

        if ( !$this->commons_model->isLoggedIn() ) {

            die(json_encode(''));

        }

        $result = $this->users_model->getlist();

        die(json_encode( $result ));

    }

    public function delete( $userId ) {

        if ( $this->session->userdata('userRole') == USER_SUPERADMIN ) {

            $result = $this->users_model->delete( $userId );

            die( json_encode( $result ) );

        } else {

            die( json_encode(array('result'=>'failed', 'msg'=>'Permission denied')) );

        }

    }
    
    public function add() {
    	
    	$this->form_validation->set_rules('firstName', 'First Name', 'trim|required');
    	
    	$this->form_validation->set_rules('lastName', 'Last Name', 'trim|required');
    	
    	$this->form_validation->set_rules('userEmail', 'Email Address', 'trim|required|valid_email|is_unique[cs_users.odUserEmail');
    	
    	if ( $this->form_validation->run() ) {
    		
    		$result = $this->users_model->add();
    		
    		die( json_encode( $result ) );
    		
    	} else {
    		
    		die( json_encode(array('result'=>'failed', 'msg'=>validation_errors())));
    		
    	}
    }
	
	public function login() {

        $this->load->model('commons_model');

        //if ( $this->commons_model->isLoggedIn() ) {

        //    redirect('companies');

        //}

	    $data['main_content'] = 'users/login';

	    $data['page'] = 'login';

        $cookie = get_cookie('cs_rememberme');

        $this->users_model->deletecookie( $cookie );

        delete_cookie( 'cs_rememberme' );

        $this->session->set_userdata('remember_st', 'false');

        $data['jsPath'] = 'users/usersJs';

        if ( $this->session->userdata('userId') ) {

            $this->session->set_userdata(array('userId'=> null, 'isloggedin'=>0));

        }

	    // $data['jsPath'] = 'users/usersJs';

	    $this->load->view('includes/template', $data);		
	}
	
	public function forgotPassword() {

	    $data['main_content'] = 'users/forgotpassword';

	    $data['page'] = 'login';


	    $this->load->view('includes/template', $data);

	}

    public function forgotpwd() {

        $this->form_validation->set_rules('userEmail', 'Email Address', 'trim|required|valid_email');

        if ( $this->form_validation->run() ) {

            $this->users_model->sendEmailForgotPassword( );

            $data['main_content'] = 'users/forgotpassword';

            $data['page'] = 'login';

            $data['errors'] = "<div class='alert alert-success'>Please check your Inbox</div>";

            $data['userEmail'] = $this->input->post('userEmail');

            // $data['jsPath'] = 'users/usersJs';

            $this->load->view('includes/template', $data);

        } else {

            $data['main_content'] = 'users/forgotpassword';

            $data['page'] = 'login';

            $data['errors'] = "<div class='alert alert-danger'>" . validation_errors() . "</div>";

            $data['userEmail'] = $this->input->post('userEmail');
            // $data['jsPath'] = 'users/usersJs';

            $this->load->view('includes/template', $data);

        }

    }
	
	public function getGroups() {

		$this->load->model('roles_model');

	}
	
	public function getCompanyUserList( $companyId ) {
		
		$users = $this->users_model->getCompanyUsers( $companyId );
		
		$data['main_content'] = 'users/companyusers';
		
		$data['page'] = 'users';
		
		$data['jsPath'] = 'users/usersJs';
		
		$data['users'] = $users;
		
		$this->load->model('companies_model');
		
		$data['companyInfo'] = $this->companies_model->get( $companyId );
		
		$this->load->view('includes/template', $data);
		
	}
	
	public function update() {
		
		if ( $this->session->userdata('userRole') == USER_ROADMIN ) {
			
			die( json_encode( array('result'=>'failed', 'msg'=>'Invalid permission')) );
			
		} else {
			
			$result = $this->users_model->update();
			
			die( json_encode( $result ));
			
		}
		
	}

    public function checkAuth() {

        $this->form_validation->set_rules('userEmail', 'User Email', 'trim|required|valid_email');//|is_unique[cs_users.odUserEmail]

        $this->form_validation->set_rules('userPass', 'Password', 'required');

        // $data['npage'] = $this->input->post('npage');

        if ( $this->form_validation->run() ) {

            if ( $this->users_model->checkAuth() ) {

                $cookie_str = substr(str_shuffle(md5(time())), 0, 50);

                $cookie = array(
                    'name' => 'rememberme',
                    'value' => $cookie_str,
                    'expire' => '2592000',
                    'domain' => '',
                    'path' => '/',
                    'prefix' => '',
                    'secure' => FALSE
                );

                if ( $this->session->userdata('remember_st') == 'true' && $this->session->userdata('userRole') != USER_SUPERADMIN) {

                    set_cookie( $cookie );

                    $this->users_model->saveCookie( $cookie_str );

                }

                if ( $this->session->userdata('userRole') == USER_SUPERADMIN || $this->session->userdata('userRole') == USER_ROADMIN)
                    redirect('companies');
                else
                    redirect('domains');

            } else {

                $data['main_content'] = 'users/login';

                $data['page'] = 'login';

                $data['errors'] = "<div class='alert alert-danger'>Invalid credentials</div>";

                $this->load->view('includes/template', $data);

            }

        } else {

            $data['main_content'] = 'users/login';

            $data['page'] = 'login';

            $data['errors'] = "<div class='alert alert-danger'>" . validation_errors() . "</div>";

            $this->load->view('includes/template', $data);

        }

    }

    public function logout() {

        $this->load->model('commons_model');

        if ( $this->commons_model->isLoggedIn() ) {

            if ( $this->commons_model->isSuperAdmin() ) {
                $params = array(
                    'auditDetails' => 'Super admin logged out',
                    'auditAction' => 'Super admin Logout',
                    'auditDate' => date('Y-m-d H:i:s'),
                );
            } else {
                $params = array(
                    'auditDetails' => 'Logged out',
                    'auditAction' => 'User Logout',
                    'auditDate' => date('Y-m-d H:i:s'),
                    'auditUserId' => $this->session->userdata('userId'),
                    'cs_companies_id' => $this->session->userdata('companyId')
                );
            }


            $this->commons_model->printAudit( $params );

            $this->session->sess_destroy();
        }

        $this->login();

    }

    public function register() {

        $this->load->model('commons_model');

        if ( !$this->commons_model->isLoggedIn() || !$this->common_model->isSuperAdmin() ) {

            die( json_encode(array('result', 'msg'=>'You have no permission.')) );

        }

        $this->form_validation->set_rules('firstName', 'First Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('lastName', 'Last Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('userEmail', 'User Email', 'trim|required|valid_email|is_unique[cs_users.odUserEmail]');

        if ( $this->form_validation->run() ) {

            $result = $this->users_model->createUser();

        } else {

            $result = array('result'=>'failed', 'msg'=>validation_errors());

        }

        die( json_encode( $result ) );

    }

    public function doActive() {

        $data['main_content'] = 'users/activateuser';

        $data['page'] = 'login';

        $this->load->view('includes/template', $data);

    }

    public function activate() {

        $this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]|min_length[4]');

        $this->form_validation->set_rules('passconf', 'Confirm Password', 'required');

        if ( $this->form_validation->run() ) {

            $result = $this->users_model->activate();

            redirect('users/login');

        } else {

            $data['main_content'] = 'users/activateuser';

            $data['page'] = 'login';

            $data['errors'] = "<div class='alert alert-danger'>" . validation_errors() . "</div>";

            $this->load->view('includes/template', $data);

        }

    }

    public function rememberme_sw() {

        if ( $this->session->userdata('remember_st') == 'true' ) {

            $this->session->set_userdata('remember_st', 'false');

            die('false');

        } else {

            $this->session->set_userdata('remember_st', 'true');

            die('true');

        }

    }

    public function myprofile () {

        $data['main_content'] = 'users/myprofile';

        $data['page'] = 'users';

        $data['jsPath'] = 'users/usersJs';

        $this->load->view('includes/template', $data);
    }
	
}