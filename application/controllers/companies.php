<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companies extends CI_Controller {
	
	public function __construct() {
		
		parent::__construct();
		
		$this->load->model('companies_model');
		
	}
	public function index()
	{
		$this->load->model('commons_model');
		
		if ( !$this->commons_model->isLoggedIn() ) {

			redirect('users/login');
			
			return;
			
		}
        if ( $this->session->userdata('userRole') == USER_ROUSER || $this->session->userdata('userRole') == USER_DEFAULTUSER ) {

            redirect('users/login');

        }
		$data['main_content'] = 'company/companies';
		
		$data['page'] = 'company';
		
		$list = $this->companies_model->getList();
		
		$data['companies'] = $list;
		
		$data['jsPath'] = 'company/companyJs';
		
		$this->load->view('includes/template', $data);
	}
	
	public function getList() {
		
		$result = $this->companies_model->getList();
		
		die( json_encode( $result ) );
		
	}
	
	public function get() {
		
		$result = $this->companies_model->get();
		
		die ( json_encode( $result ) );
		
	}
	
	public function addNew() {
		
		die ( json_encode( $this->companies_model->addNew() ) );
		
	}
	
	public function delete( $companyId ) {
		
		$result = $this->companies_model->delete( $companyId );
		
		die ( json_encode('success') );
		
	}
	
	public function update() {
		
		$result = $this->companies_model->update();
		
		die ( json_encode( $result ) );
		
	}

    public function refresh_all() {

        $result = $this->companies_model->refresh_all();

        die( json_encode($result) );

    }
}