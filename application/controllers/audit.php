<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Audit extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->model('audit_model');

	}
	public function index()
	{

        if ( !$this->session->userdata('userId') || $this->session->userdata('isloggedin') == false ) {

            redirect('users/login');

        }

		$data['main_content'] = 'audits/list';
		
		$data['page'] = 'audits';
		
		$data['audits'] = $this->audit_model->getlist();
		
		$data['jsPath'] = 'audits/auditsJs';
		
		$this->load->view('includes/template', $data);
		
	}
	
	public function getList() {
	}
	
	public function addNew() {
	}
}