<?php
class Alerts_Model extends CI_Model {

	// id,info,createdAt,modifiedAt,notes,sourceName,active,shared,cs_users_id,cs_companies_id,cs_sourcetype_id,cs_alerttype_id,cs_itype_id,deletedYN
	public $table_name = 'cs_alerts';

	function __construct() {
		
		parent::__construct();

        printLog('info', 'Alerts Model Construct');
		
	}

    public function add() {

        $inputs = $this->input->post();
		
		$inputs['createdAt'] = gmdate("Y-m-d H:i:s");
		
		$inputs['modifiedAt'] = gmdate("Y-m-d H:i:s");

        $this->db->insert($this->table_name, $inputs );

        $alertId = $this->db->insert_id();

        $added = $this->get( $alertId, 'N' );

        return array('result'=>'success', 'msg'=>'', 'alertInfo'=> $added);
    }

    public function get( $alertId, $exp ) {

        // if ( $exp ) {
        $this->db->where('ca.deletedYN', $exp);
        
		$this->db->where('ca.id', $alertId);
        $this->db->select('ca.*,cu.odUserFirstName firstName,cu.odUserLastName lastName,cu.odUserId,cat.type alertType,cit.itypeType itypeType,cc.odOrgId odOrgComId, cc.userId comEmail,cs.typeName sourceType');

        $this->db->from('cs_alerts ca');

        $this->db->join('cs_alerttype cat','cat.id=ca.cs_alerttype_id','left');

        $this->db->join('cs_sourcetype cs', 'cs.id=ca.cs_sourcetype_id', 'left');

        $this->db->join('cs_users cu', 'cu.id=ca.cs_users_id', 'left');

        $this->db->join('cs_itype cit', 'cit.id=ca.cs_itype_id', 'left');

        $this->db->join('cs_companies cc', 'cc.id=ca.cs_companies_id', 'left');

        $result = $this->db->get()->result();

        return isset($result) ? $result[0] : array();

    }
	
	public function getlist( $exp ) {
		
		$this->db->where('ca.deletedYN', $exp);
		
		$this->db->select('ca.*,cu.odUserFirstName firstName,cu.odUserLastName lastName,cu.odUserId,cat.type alertType,cit.itypeType itypeType,cc.odOrgId odOrgComId, cc.userId comEmail,cs.typeName sourceType');
		
		$this->db->from('cs_alerts ca');
		
		$this->db->join('cs_alerttype cat','cat.id=ca.cs_alerttype_id','left');
		
		$this->db->join('cs_sourcetype cs', 'cs.id=ca.cs_sourcetype_id', 'left');
		
		$this->db->join('cs_users cu', 'cu.id=ca.cs_users_id', 'left');
		
		$this->db->join('cs_itype cit', 'cit.id=ca.cs_itype_id', 'left');
		
		$this->db->join('cs_companies cc', 'cc.id=ca.cs_companies_id', 'left');
		
		$result = $this->db->get()->result();
		
		return isset($result) ? $result : array();
	}
	
	public function deleteByCompany ( $companyId ) {
		
		$this->db->update('cs_alerts', array('deletedYN'=>"Y"), array('cs_companies_id'=>$companyId));
		
	}
	
	public function delete( $alertId ) {		
		$this->db->where( 'id', $alertId);
		
		$this->db->update( $this->table_name, array('deletedYN'=>'Y') );
		
		return array('result' => 'success', 'msg' => '' );
	}

    public function update() {
        $inputs = $this->input->post();

        $alertId = $inputs['id'];

        $params = array(
            'info' => $inputs['info'],
            'notes' => $inputs['notes'],
            'modifiedAt' => gmdate("Y-m-d H:i:s"),
            'sourceName' => $inputs['sourceName']
        );

        $this->db->where('id', $alertId);

        $this->db->update( $this->table_name, $params );

        $updated = $this->get( $alertId, 'N' );

        return array('result'=> 'success', 'msg'=>'Failed to update!');
    }
    
    public function saveFireEyeAddress( $str_ip, $companyId ) {
        	
    	$param = array(
    			'sourceName' => $str_ip,
    			'cs_companies_id' => $companyId,
    			'active' => 'Y',
    			'cs_sourcetype_id' => '3',
    			'cs_alerttype_id' => '3',
    			'cs_itype_id' => '14',
    			'deletedYN' => 'N',
    			'createdAt' => gmdate("Y-m-d H:i:s"),
    			'modifiedAt' => gmdate("Y-m-d H:i:s"),
    			'notes' => 'cnc-service',
    			'info' => 'From FireEye'
    	);
    	
    	$this->db->insert( $this->table_name, $param );
    }
}