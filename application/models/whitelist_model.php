<?php
class Whitelist_model extends CI_Model {
	
	function __construct() {
		
		parent::__construct();
		
	}
	
	public function getList() {
		
		$this->db->select("*")->from('cs_whitelist');
		
		$result = $this->db->get()->result();
		
		return $result;
		
	}
	
	public function get( $whitelistId ) {

		$this->db->select("*")->from('cs_whitelist')->where('id', $whitelistId);
		
		$result = $this->db->get()->result();
		
		return isset($result) ? $result[0] : null;
		
	}
	
	public function checkDuplication( $strName ) {
		
		$this->db->select("*")->from("cs_whitelist")->where('data', $strName);
		
		return $this->db->count_all_results() > 0;
	}
	
	public function add() {
		
		$inputs = $this->input->post();
		
		if ( isset( $inputs ) ) {
			
			$params = array(
					'data' => $inputs['data'],
					'notes' => $inputs['note'],
					'createdAt'=>date("Y-m-d H:i:s"),
					'modifiedAt'=>date("Y-m-d H:i:s")
			);
			
			if ( $this->checkDuplication($inputs['data']) == true ) 
				return array('result'=>'failed', 'msg'=>'Duplicated data');
				
			$this->db->insert('cs_whitelist', $params);
				
			$whitelistId = $this->db->insert_id();
				
			$whitelist = $this->get( $whitelistId );
				
			return array('result'=>'success', 'msg'=>'', 'whitelist' => $whitelist);
			
		}
		
		return array('result'=>'failed', 'msg'=>'Bad Request');
		
	}
	
	public function delete( $whitelistId ) {
		
		$this->db->where('id', $whitelistId);
		
		$this->db->delete('cs_whitelist');
		
		return array('result'=>'success', 'msg'=>'');
		
	}
	
	public function update() {
		
		$inputs = $this->input->post();

		if ( isset( $inputs ) ) {
				
			$params = array(
					'data'=> $inputs['data'],
					'notes'=>$inputs['note'],
					'modifiedAt'=>date("Y-m-d H:i:s")
			);
				
			$this->db->where('id', $inputs['whitelistId']);
				
			$this->db->update('cs_whitelist', $params);
				
			return array('result'=>'success', 'msg'=>'');
				
		}
		
		return array('result'=>'failed', 'msg'=>'Bad Request');
		
	}
}