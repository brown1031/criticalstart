<?php
class Audit_model extends CI_Model {
	
	function __construct() {
		
		parent::__construct();
		
	}
	
	public function getlist() {

        if ( $this->session->userdata('userRole') == USER_SUPERADMIN || $this->session->userdata('userRole') == USER_ROADMIN ) {
            $this->db->select('ca.*,cs.odOrgId orgComId')->from('cs_audits ca')->join('cs_companies cs', 'cs.id=ca.cs_companies_id', 'left');
        } else {
            $this->db->select('ca.*,cs.odOrgId orgComId')->from('cs_audits ca')->join('cs_companies cs', 'cs.id=ca.cs_companies_id', 'left')->where('cs.id', $this->session->userdata('companyId'));
        }
		
		$result = $this->db->get()->result();
		
		return $result;
		
	}
	
	public function deleteByCompany( $companyId ) {
		// if ( $this->session->userdata('userRole') == USER_SUPERADMIN)
		$this->db->delete('cs_audits', array('cs_companies_id' => $companyId));
		
	}

    public function add( $params ) {

        $this->db->trans_start();

        $this->db->insert( 'cs_audits', $params );

        $this->db->trans_complete();

        if ( $this->db->trans_status() !== false ) {

            return true;

        }

        return false;

    }
}