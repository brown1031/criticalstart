<?php
class Sourcetypes_model extends CI_Model {
	
	function __construct() {
		
		parent::__construct();
		
	}
	
	public function getList() {
		
		$this->db->select("cs.*, cu.odUserFirstName, cu.odUserLastName")->from('cs_sourcetype cs');
		
		$this->db->join('cs_users cu','cu.id=cs.modifiedId','left');
		
		$result = $this->db->get()->result();
		
		return $result;
		
	}
	
	public function get( $sourceTypeId ) {
		

		$this->db->where("cs.id", $sourceTypeId);
		
		$this->db->select("cs.*, cu.odUserFirstName, cu.odUserLastName")->from('cs_sourcetype cs');
		
		$this->db->join('cs_users cu','cu.id=cs.modifiedId','left');
				
		$result = $this->db->get()->result();
		
		return isset($result) ? $result[0] : null;
		
	}
	
	public function checkDuplication( $strName ) {
		
		$this->db->select("*")->from("cs_sourcetype")->where('typeName', $strName);
		
		return $this->db->count_all_results() > 0;
	}
	
	public function add() {
		
		$inputs = $this->input->post();
		
		if ( isset( $inputs ) ) {
			
			if ( $this->checkDuplication($inputs['typeName']) )
				return array('result'=>'failed', 'msg'=>'Duplicated type name');
			
			$params = array(
					'typeName' => $inputs['typeName'],
					'typeDescription' => $inputs['typeDescription'],
					'createdAt'=>date("Y-m-d H:i:s"),
					'modifiedAt'=>date("Y-m-d H:i:s"),
					//'modifiedId' =>'39'
			);
				
			$this->db->insert('cs_sourcetype', $params);
				
			$sourceTypeId = $this->db->insert_id();
				
			$sourceType = $this->get( $sourceTypeId );
				
			return array('result'=>'success', 'msg'=>'', 'sourceType' => $sourceType);
			
		}
		
		return array('result'=>'failed', 'msg'=>'Bad Request');
		
	}
	
	public function delete( $sourceTypeId ) {
		
		$this->db->where('id', $sourceTypeId);
		
		$this->db->delete('cs_sourcetype');
		
		return array('result'=>'success', 'msg'=>'');
		
	}
	
	public function update() {
		
		$inputs = $this->input->post();

		if ( isset( $inputs ) ) {
				
			$params = array(
					'typeName'=> $inputs['typeName'],
					'typeDescription'=>$inputs['typeDescription'],
					'modifiedAt'=>date("Y-m-d H:i:s"),
					//'modifiedId'=>'39'
			);
				
			$this->db->where('id', $inputs['sourceTypeId']);
				
			$this->db->update('cs_sourcetype', $params);
				
			return array('result'=>'success', 'msg'=>'');
				
		}
		
		return array('result'=>'failed', 'msg'=>'Bad Request');
		
	}
}