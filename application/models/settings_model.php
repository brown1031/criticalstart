<?php
class Settings_model extends CI_Model {
	
	function __construct() {
		
		parent::__construct();
		
	}
	
	function printAudit( $params ) {
		
	}
	
	public function getApiKey() {
		
		$str_sql = "SELECT apiKey FROM cs_siteconfig LIMIT 1";
		
		$result = $this->db->query( $str_sql )->result();
		
		return isset($result) ? $result[0]->apiKey : '';
		
	}
	
	public function getconfig() {
		
		$this->db->select("*")->from("cs_siteconfig");
		
		$result = $this->db->get()->result();
		
		return isset( $result ) ? $result[0] : null;
		
	}
	
	public function updateconfig() {
		
		$inputs = $this->input->post();
		
		if ( isset( $inputs ) ) {
			
			$params = array(
				$inputs['name'] => $inputs['value']
			);
			
			$this->db->update( 'cs_siteconfig', $params );
			
			return array('result' => 'success', 'msg' => '' );
		}
		
		return array('result'=>'failed', 'msg'=>'');
	}
}