<?php
class Companies_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	public function getList() {
		$strSql = "SELECT 
				id,
				odOrgId orgId,
				odOriginId originId,
				odOrganizationname orgName,
				odCreatorUserId creatorId,
				odMspOrgId mspOrgId,
				odOrgTypeId orgTypeId,
				odCreatedAt,
				odModifiedAt,
				odHasDelegatedAdmin hasDelegatedAdmin,
				enabledFireEye,
				odNetworkCount networkCount,
				odToken,
				apiKey,
				userId,
				password passwd,
				enabledWhiteList,
				whiteListVal,
				sharedThreatIntel,
				daysAlertActive,
				require2FA,
				connectWiseId,
				customApiKey
				 FROM cs_companies";

		$list = $this->db->query( $strSql )->result();
		
		return $list;
		
	}
	
	public function get( $id ) {
		
		$strSql = 	"SELECT 
						id,
						odOrgId orgId,
						odOriginId originId,
						odOrganizationname orgName,
						odCreatorUserId creatorId,
						odMspOrgId mspOrgId,
						odOrgTypeId orgTypeId,
						odCreatedAt,
						odModifiedAt,
						odHasDelegatedAdmin hasDelegatedAdmin,
						enabledFireEye,
						odNetworkCount networkCount,
						odToken,
						apiKey,
						userId,
						password passwd,
						enabledWhiteList,
						whiteListVal,
						sharedThreatIntel,
						daysAlertActive,
						require2FA,
						connectWiseId,
						customApiKey
				 	FROM cs_companies
					WHERE id=?";
		
		$result = $this->db->query( $strSql, array($id) )->result();
		
		return isset($result) ? $result[0] : null;
		
	}
	
	public function getCompanyByToken( $token ) {
		
		$strSql = 	"SELECT
						id,
						odOrgId orgId,
						odOriginId originId,
						odOrganizationname orgName,
						odCreatorUserId creatorId,
						odMspOrgId mspOrgId,
						odOrgTypeId orgTypeId,
						odCreatedAt,
						odModifiedAt,
						odHasDelegatedAdmin hasDelegatedAdmin,
						enabledFireEye,
						odNetworkCount networkCount,
						odToken,
						apiKey,
						userId,
						password passwd,
						enabledWhiteList,
						whiteListVal,
						sharedThreatIntel,
						daysAlertActive,
						require2FA,
						connectWiseId,
						customApiKey
				 	FROM cs_companies
					WHERE odToken=?";
		
		$result = $this->db->query( $strSql, array( $token ) )->result();
		
		return isset( $result ) && count($result ) > 0 ? $result[0] : null;
		
	}
	
	public function checkDuplication( $strName ) {
		
		$this->db->select("*")->from("cs_companies")->where('userId', $strName);
		
		return $this->db->count_all_results() > 0;
	}
	
	public function addNew() {
		$inparams = $this->input->post();
		$str_orgId = $inparams['orgId'];
		$str_name = $inparams['userName'];
		$str_password = $inparams['password'];
		$this->load->model('settings_model');
		
		if ( $this->checkDuplication($str_name) == true)
			return array('result'=>'failed', 'msg'=>'Duplicated company user');
		
		// Get Api Key from site configuration
		$str_apiKey = $this->settings_model->getApiKey();
		
		// Get Token from OpenDNS using API
		$result = getOpenDNSToken(array('apiKey'=>$str_apiKey, 'name'=>$str_name, 'password'=>$str_password));

        if ( $result->status->code == 200 ) { // success to get token
			$str_token = $result->data->token;
			
			// Retrieve Company Information 
			$companyInfo = apiGetCompanyInfo($str_orgId, $str_token, $str_apiKey);
			
			if ( $companyInfo->status->code == 200 ) {
				$params = array(
					'orgId' => $str_orgId,
					'originId' => $companyInfo->data->originId,
					'orgName' => $companyInfo->data->organizationName,
					'creatorUserId'=>$companyInfo->data->creatorUserId,
					'mspOrgId'=>$companyInfo->data->mspOrganizationId,
					'orgTypeId'=>$companyInfo->data->organizationTypeId,
					'createdAt' => $companyInfo->data->createdAt,
					'modifiedAt' => $companyInfo->data->modifiedAt,
					'networkCount' => $companyInfo->data->networkCount,
					'hasDelegatedAdmin' => $companyInfo->data->hasDelegatedAdmin,
					'odToken' => $str_token,
					'odApiKey' => $str_apiKey,
					'userName' => $str_name,
					'password' => $str_password
				);
				$str_sql = "INSERT INTO cs_companies(
						odOrgId, 
						odOriginId, 
						odOrganizationName, 
						odCreatorUserid, 
						odMspOrgId, 
						odOrgTypeId, 
						odCreatedAt,
						odModifiedAt, 
						odNetworkCount,
						odHasDelegatedAdmin,
						odToken,
						apiKey,
						userId,
						password) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				$this->db->query( $str_sql, $params );
				
				$companyId = $this->db->insert_id();
				// Retrieve user information from OpenDNS
				$this->load->model('users_model');
				$this->users_model->retrieveAllUsersFromOD( $companyId );
				// Retrieve domain information from OpenDNS
				$this->load->model('domains_model');
				$this->domains_model->retrieveAllDomainsFromOD( $companyId );
				
				return array('result' => 'success', 'companyInfo' => $this->get( $companyId ) );
			} else {
				// printLog('error', 'Error get company Information : code - ' . $companyInfo->status->code . ', text - ' .$companyInfo->status->text);
				return array('result'=>'failed', 'msg'=> $companyInfo->status->text);
			}
		} else {
			printLog('error', 'Error get token : code - ' . $result->status->code . ', text  - ' . $result->status->text);
			return array('result'=>'failed', 'msg'=>$result->status->text);
		}
	}

    public function refresh_all( ) {

        $companies = $this->getList();

        foreach( $companies as $k => $v ) {

            // Retrieve user information from OpenDNS
            $this->load->model('users_model');
            $this->users_model->retrieveAllUsersFromOD( $v->id );
            // Retrieve domain information from OpenDNS
            $this->load->model('domains_model');
            $this->domains_model->retrieveAllDomainsFromOD( $v->id );

        }

        return array('result'=>'success', 'msg'=>'');

    }
	
	public function update() {
		
		$inputs = $this->input->post();
		
		$companyId = isset($inputs['companyId']) ? $inputs['companyId'] : '';
		
		if ( $companyId == '' ) return array('result'=>'failed', 'msg'=>'Bad request');
		
		$str_sql = "UPDATE cs_companies SET ";
		
		if (isset($inputs['chkFireEye'])) {
			$str_sql .= 'enabledFireEye="Y",';
		} else {
			$str_sql .= 'enabledFireEye="N",';
		}
		
		if (isset($inputs['chkEnabledWhiteList'])) {
			$str_sql .= "enabledWhiteList='Y',";
		} else {
			$str_sql .= "enabledWhiteList='N',";
		}
		
		if (isset($inputs['chkRequire2FA'])) {
			$str_sql .= "require2FA='Y',";
		} else {
			$str_sql .= "require2FA='N',";
		}
		
		if (isset($inputs['chkShareTreatIntel'])) {
			$str_sql .= "sharedThreatIntel='Y',";
		} else {
			$str_sql .= "sharedThreatIntel='N',";
		}
		
		if (isset($inputs['chkCustomApiKey'])) {
			$str_sql .= "customApiKey='Y'";
		} else {
			$str_sql .= "customApiKey='N'";
		}
		
	if (isset($inputs['txtUserName'])) {
			$str_sql .= ",userId='".$inputs['txtUserName']."'";
		} 
		
		if (isset($inputs['txtDaysAlertActive'])) {
			$str_sql .= ",daysAlertActive='".$inputs['txtDaysAlertActive']."'";
		} 
		
		if (isset($inputs['txtConnectWiseId'])) {
			$str_sql .= ",connectWiseId='".$inputs['txtConnectWiseId']."'";
		} 
		
		if (isset($inputs['txtWhiteListVal'])) {
			$str_sql .= ",whiteListVal='".$inputs['txtWhiteListVal']."'";
		} 
		
		if (isset($inputs['txtOdToken'])) {
			$str_sql .= ",odToken='".$inputs['txtOdToken']."'";
		}
		
		if (isset($inputs['txtApiKey'])) {
			$str_sql .= ",apiKey='".$inputs['txtApiKey']."'";
		}
		
		$str_sql .= " WHERE id=?";

		$this->db->query( $str_sql, array($companyId) );
		
		return array('result'=>'success', 'msg'=>'');
		
	}
	
	public function delete( $companyId ) {
		// delete domains
		$this->load->model('domains_model');
		$this->domains_model->deleteByCompany($companyId);
		// delete users
		$this->load->model('users_model');
		$this->users_model->deleteByCompany( $companyId );
		// delete alerts
		$this->load->model('alerts_model');
		$this->alerts_model->deleteByCompany( $companyId );
		
		$this->db->delete('cs_companies', array('id' => $companyId) );
		
		return array('result' => 'success', 'msg' => '');
	}
}