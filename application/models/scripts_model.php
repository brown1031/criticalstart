<?php
class Scripts_model extends CI_Model {
	
	function __construct() {
		
		parent::__construct();
		
	}
	
	public function getlist() {
		
		$this->db->select("*")->from('cs_scripts');
		
		$result = $this->db->get()->result();
		
		return $result;
		
	}
	
	public function get( $scriptId ) {
		
		$this->db->select("*")->from("cs_scripts")->where('id', $scriptId);
				
		$result = $this->db->get()->result();
		
		return isset($result) ? $result[0] : null;
		
	}
	
	public function checkDuplication( $str_name ) {
		
		$this->db->select("*")->from("cs_scripts")->where("scriptFileName", $str_name);
		
		return $this->db->count_all_results() > 0;
		
	}
	
	public function add() {
		
		$inputs = $this->input->post();
		
		if ( isset( $inputs ) ) {
			
			if ( $this->checkDuplication($inputs['fileName']))
				return array('result'=>'failed', 'msg'=>'Duplicated file name');
			
			$params = array(
				'scriptFileName' => $inputs['fileName'],
				'scriptFilePath' => $inputs['filePath'],
				'scriptDescription'=>$inputs['description'],
				'scriptRunTimes'=>$inputs['runTimes'],
				'scriptArgs' =>$inputs['scriptArgs'],
				'scriptMaxRuntime' => $inputs['scriptMaxRuntime'],
				'scriptOrgs' => $inputs['scriptOrgs'],
				'scriptReturnVals' => $inputs['scriptReturnVals'],
				'scriptAudit'=>isset($inputs['scriptAudit']) ? 'Y':'N'
			);
				
			$this->db->insert('cs_scripts', $params);
				
			$scriptId = $this->db->insert_id();
				
			$script = $this->get( $scriptId );
				
			return array('result'=>'success', 'msg'=>'', 'pscript' => $script);
			
		}
		
		return array('result'=>'failed', 'msg'=>'Bad Request');
		
	}
	
	public function delete( $scriptId ) {
		
		$this->db->where('id', $scriptId);
		
		$this->db->delete('cs_scripts');
		
		return array('result'=>'success', 'msg'=>'');
		
	}
	
	public function update() {
		
		$inputs = $this->input->post();

		if ( isset( $inputs ) ) {
				
			$params = array(
				'scriptFileName' => $inputs['fileName'],
				'scriptFilePath' => $inputs['filePath'],
				'scriptDescription'=>$inputs['description'],
				'scriptRunTimes'=>$inputs['runTimes'],
				'scriptArgs' =>$inputs['scriptArgs'],
				'scriptMaxRuntime' => $inputs['scriptMaxRuntime'],
				'scriptOrgs' => $inputs['scriptOrgs'],
				'scriptReturnVals' => $inputs['scriptReturnVals'],
				'scriptAudit'=>isset($inputs['scriptAudit']) ? 'Y':'N'
			);
				
			$this->db->where('id', $inputs['scriptId']);
				
			$this->db->update('cs_scripts', $params);
				
			return array('result'=>'success', 'msg'=>'');
				
		}
		
		return array('result'=>'failed', 'msg'=>'Bad Request');
		
	}
}