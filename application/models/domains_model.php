<?php
class Domains_model extends CI_Model {
	
	function __construct() {
		
		parent::__construct();
		
	}
	
	public function get( $domainId ) {
		
		$this->db->select("td.*, tst.typeName sTypeName, tst.typeDescription sDesc")->from('cs_domains td')->join('cs_sourcetype tst','tst.id=td.sourceTypeId','left')->where("td.id", $domainId);
		
		$result = $this->db->get()->result();
		
		return isset($result) ? $result[0] : null;
		
	}
	
	public function getSubdomain( $domainId ) {
		
		$this->db->select("*")->from('cs_subdomains')->where('id', $domainId);
		
		$result = $this->db->get()->result();
		
		return isset( $result ) ? $result[0] : null;
		
	}
	
	public function getList( $companyId ) {
		
		$this->db->select("td.*, tst.typeName sTypeName, tst.typeDescription sDesc")->from('cs_domains td')->join('cs_sourcetype tst','tst.id=td.sourceTypeId','left')->where('td.cs_companies_id', $companyId);
		
		return $this->db->get()->result();
		
	}
	
	public function retrieveAllDomainsFromOD( $companyId ) {
		$this->load->model('companies_model');

        $companyInfo = $this->companies_model->get( $companyId );
		
		if ( isset( $companyInfo ) ) {
			$str_token = $companyInfo->odToken;
			$str_apiKey = $companyInfo->apiKey;
			$str_orgId = $companyInfo->orgId;
			$result = retrieveAllDomainsFromOD( $str_orgId, $str_token, $str_apiKey, 0, 100);
			
			if ( $result->status->code == 200 ) {
				foreach ( $result->data as $k => $v ) {

					$params = array(
						'odId' => $v->id,
							'odAccess' => $v->access,
							'odisGlobal' => $v->isGlobal==0 ? 'N':'Y',
							'odName' => $v->name,
							'odCreatedAt' => $v->createdAt,
							'odModifiedAt' => $v->modifiedAt,
							'odIsMspDefault' => $v->isMspDefault == 0 ? 'N':'Y',
							'domainLimit' => 50,
							'cs_companies_id' => $companyId
					);

                    $this->db->where('odId', $v->id);
                    $this->db->select('*')->from('cs_domains')->limit(1);
                    $q = $this->db->get();
                    $domainListId = '';
                    if ( $q->num_rows() > 0 ) {
                        $row = $q->row();
                        $this->db->where('odId', $v->id);
                        $this->db->update('cs_domains', $params);
                        $domainListId = $row->id;
                    } else {
                        $this->db->insert( 'cs_domains', $params );
                        $domainListId = $this->db->insert_id();
                    }

					// Retrieve All Subdomains from OpenDNS
					$odDomainListId = $v->id;
					$subdomains = retrieveAllSubdomainsFromOD($str_orgId, $str_token, $str_apiKey, 0, 500, $odDomainListId);
					if ( $subdomains->status->code == 200 ) {
						foreach ( $subdomains->data as $kk => $vv ) {
							// if ( $this->checkSubDomainExist( $vv->domainId ) == false ) {
                            $params1 = array(
                                'odId' => $vv->domainId,
                                'name' => $vv->domain,
                                'createdAt' => $vv->createdAt,
                                'domainListId' => $domainListId
                            );

                            $this->db->where('odId', $vv->domainId);
                            $this->db->select('*')->from('cs_subdomains')->limit(1);
                            $q = $this->db->get();
                            if ( $q->num_rows() > 0 ) {
                                $this->db->where('odId', $vv->domainId);
                                $this->db->update('cs_subdomains', $params1);
                            } else {
                                $this->db->insert('cs_subdomains', $params1);
                            }
							// }
						}
					}
				}
			}
		}
	}
	
	public function checkDomainExist( $odId ) {
		
		$this->db->select('*')->from('cs_domains')->where('odId', $odId);
		
		$result = $this->db->get();
		
		return $result->num_rows() > 0 ? true : false;
		
	}
	
	public function checkSubDomainExist( $odDomainId ) {
		$this->db->select('*')->from('cs_subdomains')->where( 'odId', $odDomainId );
		$result = $this->db->get();
		return $result->num_rows() > 0 ? true : false;
	}
	
	public function getSubDomains( $domainId ) {
		
		$this->db->select("*")->from('cs_subdomains')->where('domainListId', $domainId);
		
		return $this->db->get()->result();
		
	}
	
	public function getDomainListInfo( $domainListId ) {
		
		$this->db->select("*")->from("cs_domains")->where('id', $domainListId);
		
		return $this->db->get()->result();
		
	}
	
	public function addNewDomainList( $companyId ) {
		
		$this->load->model('companies_model');
		
		$companyInfo = $this->companies_model->get( $companyId );
		
		$access = $this->input->post('access');
		
		$name = $this->input->post('listName');
		
		$str_token = $companyInfo->odToken;
		
		$str_apiKey = $companyInfo->apiKey;
		
		$str_orgId = $companyInfo->orgId;
		
		$result = apiAddDomainList( $str_orgId, $str_token, $str_apiKey, array('name'=>$name, 'access'=>$access) );
		
		if ( $result->status->code == 200 ) {
			
			$params = array(
				'odId' => $result->data->id,
				'odAccess' => $result->data->access,
				'odisGlobal' => $result->data->isGlobal==0 ? 'N':'Y',
				'odName' => $result->data->name,
				'odCreatedAt' => $result->data->createdAt,
				'odModifiedAt' => $result->data->modifiedAt,
				'odIsMspDefault' => $result->data->isMspDefault == 0 ? 'N':'Y',
				'domainLimit' => 50,
				'cs_companies_id' => $companyId
			);
			
			$this->db->insert('cs_domains', $params);
			
			$domainListId = $this->db->insert_id();
			
			$domainList = $this->get( $domainListId );
			
			return array('result'=>'success', 'msg'=>'', 'domainList' => $domainList );
			
		} else {
			return array('result'=>'failed', 'msg'=>$result->status->text);
		}
	}
	
	public function addNewDomain( $companyId, $domainListId ) {
		
		$domainListInfo = $this->get($domainListId);
		
		if ( isset($domainListInfo) ) {
			
			$this->load->model('companies_model');
			
			$companyInfo = $this->companies_model->get( $companyId );
			
			$name = $this->input->post('domainName');
			
			$str_token = $companyInfo->odToken;
			
			$str_apiKey = $companyInfo->apiKey;
			
			$str_orgId = $companyInfo->orgId;
			
			$result = apiAddNewDomain( $str_orgId, $str_token, $str_apiKey, $domainListInfo, $name );
			
			if ( $result->status->code == 200 ) {
				
				// Insert New Line
				$params = array(
					'name'=> $name,
					'domainListId' => $domainListId
				);
				
				$this->db->insert( 'cs_subdomains', $params );
				
				$domainId = $this->db->insert_id();
				
				$subdomains = retrieveAllSubdomainsFromOD($str_orgId, $str_token, $str_apiKey, 0, 500, $domainListInfo->odId);
				
				if ( $subdomains->status->code == 200 ) {
					
					foreach ( $subdomains->data as $kk => $vv ) {
						
						//printLog('error', $vv->domain );
						// printLog('error', $vv->domain . ($vv->domain==$name?'true':'false'));
						if ( strcmp($vv->domain, $name) == 0 ) {
							
							$params1 = array(	
									
								'odId' => $vv->domainId,
									
								'createdAt' => $vv->createdAt
									
							);
							
							$this->db->where('domainListId', $domainListId);
							
							$this->db->where('name', $name);
								
							$this->db->update('cs_subdomains', $params1);
							
						}
					}
					
					$domain = $this->getSubdomain( $domainId );
					
					return array( 'result'=>'success', 'msg' => '', 'domain' => $domain );
				}
				
				return array("result"=>'failed', 'msg' => 'Bad Request');
				
			}
			
		} 
		
		return array('result'=>'failed', 'msg'=>'Failed to add new domain');
	}
	
	public function deleteByCompany( $companyId ) {
		
		$str_sql = "DELETE FROM cs_domains WHERE cs_companies_id=?";
		
		$this->db->query( $str_sql, array( $companyId ) );
		
	}
	
	public function removeDomainList ( $domainId ) {
		
		$domainListInfo = $this->get( $domainId );
		
		if ( $domainListInfo ) {
			
			$this->load->model( 'companies_model' );
			$companyInfo = $this->companies_model->get( $domainListInfo->cs_companies_id );
			
			$result = apiDeleteDomainList( $companyInfo->orgId, $companyInfo->odToken, $companyInfo->apiKey, $domainListInfo );
			
			if ( $result->status->code == 200 ) {
				
				$this->db->where('id', $domainId );
				
				$this->db->delete( array('cs_domains') );
				
				return array( 'result' => 'success', 'msg' => '');
				
			}
			
			return array( 'result' => 'failed', 'msg' => 'Failed to delete domain list');
			
		}
		
		return array( 'result' => 'failed', 'msg' => 'Failed to delete domain list');
	}
	
	public function removeDomain( ) {
		
		$inputs = $this->input->post();
		
		if ( isset($inputs) ) {
			$companyId = $inputs['companyId'];
			
			$domainListId = $inputs['domainListId'];
			$domainListInfo = $this->get( $domainListId );
			
			$domainId = $inputs['domainId'];
			
			$domainName = $inputs['domainName'];
			
			$this->load->model( 'companies_model' );
			$companyInfo = $this->companies_model->get( $domainListInfo->cs_companies_id );
				
			$result = apiRemoveDomain( $companyInfo->orgId, $companyInfo->odToken, $companyInfo->apiKey, $domainListInfo, $domainName );
			
			if ( $result->status->code == 200 ) {
				
				$this->db->where('id', $domainId);
				
				$this->db->delete('cs_subdomains');
				
				return array('result'=>'success', 'msg'=>'');
				
			} else {
				
				return array('result'=>'failed', 'msg'=>'Bad Request');
				
			}
		}
		
		return array('result'=>'failed', 'msg'=>'Bad Request');
	}
	
	
	public function update(  ) {
		
		$inputs = $this->input->post();
		
		if ( $inputs ) {
			
			$domainListId = $inputs['domainListId'];
			
			$domainListInfo = $this->get( $domainListId );

			$params = array(
				'odName' => $inputs['domainListName'],
				'odAccess' => $inputs['access'],
				'sourceTypeId' => $inputs['sourceTypeId'],
				'domainLimit' => $inputs['domainLimit'],
				'domainListSync' => isset($inputs['domainListSync'])?'Y':'N',
				'odisGlobal' => $domainListInfo->odisGlobal,
				'odCreatedAt' =>$domainListInfo->odCreatedAt,
				'odModifiedAt' => $domainListInfo->odModifiedAt,
				'odIsMspDefault' => $domainListInfo->odIsMspDefault,
				'odId' => $domainListInfo->odId
			);
			
						
			$this->load->model( 'companies_model' );
			$companyInfo = $this->companies_model->get( $domainListInfo->cs_companies_id );
				
			$result = apiUpdateDomainList( $companyInfo->orgId, $companyInfo->odToken, $companyInfo->apiKey, $params );
			
			if ( $result->status->code == 200 ) {

				$this->db->where('id', $domainListId);
				$this->db->update('cs_domains', $params);
				
				return array( 'result' => 'success', 'msg' => '' );
				
			} else {
				
				return array('result' => 'failed', 'msg' => 'Wrong api for OpenDNS.com' );
				
			}
			
		}
		
		return array('result' => 'failed', 'msg' => 'Bad Request');
	}
}