<?php
class Users_model extends CI_Model {
	
	public $tableName = 'cs_users';
	
	function __construct() {
		parent::__construct();
	}
	
	public function getlist() {

        if ( $this->session->userdata('userRole') == USER_SUPERADMIN || $this->session->userdata('userRole') == USER_ROADMIN ) {

            $this->db->join('cs_companies cc', 'cc.id=cu.cs_companies_id', 'left');

            $this->db->join( 'cs_role cr', 'cr.id=cu.cs_role_id', 'left');

            $this->db->from('cs_users cu');

            $this->db->select( 'cu.*, cr.roleName roleName, cc.odOrgId odComOrgId, cc.odOrganizationName orgName');

        } else {

            $this->db->join('cs_companies cc', 'cc.id=cu.cs_companies_id', 'left');

            $this->db->join( 'cs_role cr', 'cr.id=cu.cs_role_id', 'left');

            $this->db->from('cs_users cu');

            $this->db->select( 'cu.*, cr.roleName roleName, cc.odOrgId odComOrgId, cc.odOrganizationName orgName')->where(array('cs_companies_id'=>$this->session->userdata('companyId')));
        }

		$result = $this->db->get()->result();

        return $result;
	}
	
	public function get( $userId ) {
		
		$this->db->join('cs_companies cc', 'cc.id=cu.cs_companies_id', 'left');
		
		$this->db->join( 'cs_role cr', 'cr.id=cu.cs_role_id', 'left');
		
		$this->db->from('cs_users cu');
		
		$this->db->select( 'cu.*, cr.roleName roleName, cc.odOrgId odComOrgId, cc.odOrganizationName orgName')->where(array('cu.id'=>$userId));
		
		$result = $this->db->get()->result();
		
		return isset( $result ) ? $result[0] : null;
	}
	
	public function add() {
		
		$inputs = $this->input->post();
		$primaryContact = 'N';
		if ( isset( $inputs['primaryContact'] )) {
			
			$this->db->where('primaryContact', 'Y');
			
			$this->db->select('*')->from('cs_users');
			
			$q = $this->db->get();
			
			if ( $q->num_rows() > 0 ) {
				
				return array('result'=>'failed', 'msg'=>'Primary contact already exist in this company.');
				
			}
			
			$primaryContact = 'Y';
			
		}
		
		$params = array(
			'odUserFirstName' => $inputs['firstName'],
			'odUserLastName' => $inputs['lastName'],
			'odUserEmail' => $inputs['userEmail'],
			'createdAt' => date('Y-m-d H:i:s'),
			'isOdUser' => 'N',
			'primaryContact' => $primaryContact,
			'cs_role_id'=>$inputs['roleId'],
			'cs_companies_id'=>$inputs['companyId']
		);
		
		$this->db->trans_start();
		
		$this->db->insert('cs_users', $params);
		
		$userId = $this->db->insert_id();
		
		$this->db->trans_complete();
		
		return array('result'=>'success', 'msg'=>'', 'userInfo' => $this->get($userId));
	}
	
	public function update() {
		
		$userId = null;
		
		$inputs = $this->input->post();
		
		$password = isset($inputs['password']) ? $inputs['password'] : '';
		
		$params = array();
		
		
		if ( $this->session->userdata('userRole') == USER_SUPERADMIN ) {
			
			$userId = $inputs['userId'];
			
			$userInfo = $this->get( $userId );
			
			if ( $password == '' ) {
				if ( $userInfo->isOdUser == 'N' ) {
					$params = array(
							'odUserFirstName' => $inputs['firstName'],
							'odUserLastName' => $inputs['lastName'],
							'odUserEmail' => $inputs['userEmail'],
							'modifiedAt' => date('Y-m-d H:i:s'),
							'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
							'user2FA' => $userInfo->odUser2FA == 'N' ? isset($inputs['user2FA'])?'Y':'N' : 'N'
					);
				} else {
					$params = array(
							'modifiedAt' => date('Y-m-d H:i:s'),
							'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
							'user2FA' => $userInfo->odUser2FA == 'N' ? (isset($inputs['user2FA']) ? 'Y' : 'N') : 'N',
					);
				}
			} else {
				
				if ( $userInfo->isOdUser == 'N' ) {
					$params = array(
							'odUserFirstName' => $inputs['firstName'],
							'odUserLastName' => $inputs['lastName'],
							'odUserEmail' => $inputs['userEmail'],
							'modifiedAt' => date('Y-m-d H:i:s'),
							'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
							'user2FA' => $userInfo->odUser2FA == 'N' ? (isset($inputs['user2FA']) ? 'Y' : 'N') : 'N',
							'password' => hash('sha256', $password . $userInfo->hashKey)
					);
				} else {
					$params = array(
							'modifiedAt' => date('Y-m-d H:i:s'),
							'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
							'user2FA' => $userInfo->odUser2FA == 'N' ? (isset($inputs['user2FA']) ? 'Y' : 'N') : 'N',
							'password' => hash('sha256', $password.$userInfo->hashKey)
					);
				}				
			}
					
		} else if ( $this->session->userdata('userRole') == USER_DEFAULTUSER ) {
			
			$userId = $inputs['userId'];
			$userInfo = $this->get( $userId );
			if ( $this->session->userdata('userId') != $userId ) {
				if ( $userInfo->isOdUser == 'N' ) {
					$params = array(
							'odUserFirstName' => $inputs['firstName'],
							'odUserLastName' => $inputs['lastName'],
							'odUserEmail' => $inputs['userEmail'],
							'modifiedAt' => date('Y-m-d H:i:s'),
							'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
							'user2FA' => $userInfo->odUser2FA == 'N' ? isset($inputs['user2FA'])?'Y':'N' : 'N'
					);
				} else {
					$params = array(
							'modifiedAt' => date('Y-m-d H:i:s'),
							'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
							'user2FA' => $userInfo->odUser2FA == 'N' ? (isset($inputs['user2FA']) ? 'Y' : 'N') : 'N',
					);
				}		
			} else {
				if ( $password == '' ) {
					if ( $userInfo->isOdUser == 'N' ) {
						$params = array(
								'odUserFirstName' => $inputs['firstName'],
								'odUserLastName' => $inputs['lastName'],
								'odUserEmail' => $inputs['userEmail'],
								'modifiedAt' => date('Y-m-d H:i:s'),
								'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
								'user2FA' => $userInfo->odUser2FA == 'N' ? isset($inputs['user2FA'])?'Y':'N' : 'N'
						);
					} else {
						$params = array(
								'modifiedAt' => date('Y-m-d H:i:s'),
								'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
								'user2FA' => $userInfo->odUser2FA == 'N' ? (isset($inputs['user2FA']) ? 'Y' : 'N') : 'N',
						);
					}
				} else {
				
					if ( $userInfo->isOdUser == 'N' ) {
						$params = array(
								'odUserFirstName' => $inputs['firstName'],
								'odUserLastName' => $inputs['lastName'],
								'odUserEmail' => $inputs['userEmail'],
								'modifiedAt' => date('Y-m-d H:i:s'),
								'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
								'user2FA' => $userInfo->odUser2FA == 'N' ? (isset($inputs['user2FA']) ? 'Y' : 'N') : 'N',
								'password' => hash('sha256', $password . $userInfo->hashKey)
						);
					} else {
						$params = array(
								'modifiedAt' => date('Y-m-d H:i:s'),
								'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
								'user2FA' => $userInfo->odUser2FA == 'N' ? (isset($inputs['user2FA']) ? 'Y' : 'N') : 'N',
								'password' => hash('sha256', $password.$userInfo->hashKey)
						);
					}
				}
			}
			
		} else if ( $this->session->userdata('userRole') == USER_ROUSER ) {
			
			$userId = $inputs['userId'];
			$userInfo = $this->get( $userId );
			if ( $this->session->userdata('userId') != $userId ) {
				if ( $userInfo->isOdUser == 'N' ) {
					$params = array(
							'odUserFirstName' => $inputs['firstName'],
							'odUserLastName' => $inputs['lastName'],
							'odUserEmail' => $inputs['userEmail'],
							'modifiedAt' => date('Y-m-d H:i:s'),
							'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
							'user2FA' => $userInfo->odUser2FA == 'N' ? isset($inputs['user2FA'])?'Y':'N' : 'N'
					);
				} else {
					$params = array(
							'modifiedAt' => date('Y-m-d H:i:s'),
							'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
							'user2FA' => $userInfo->odUser2FA == 'N' ? (isset($inputs['user2FA']) ? 'Y' : 'N') : 'N',
					);
				}
			} else {
				if ( $password == '' ) {
					if ( $userInfo->isOdUser == 'N' ) {
						$params = array(
								'odUserFirstName' => $inputs['firstName'],
								'odUserLastName' => $inputs['lastName'],
								'odUserEmail' => $inputs['userEmail'],
								'modifiedAt' => date('Y-m-d H:i:s'),
								'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
								'user2FA' => $userInfo->odUser2FA == 'N' ? isset($inputs['user2FA'])?'Y':'N' : 'N'
						);
					} else {
						$params = array(
								'modifiedAt' => date('Y-m-d H:i:s'),
								'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
								'user2FA' => $userInfo->odUser2FA == 'N' ? (isset($inputs['user2FA']) ? 'Y' : 'N') : 'N',
						);
					}
				} else {
			
					if ( $userInfo->isOdUser == 'N' ) {
						$params = array(
								'odUserFirstName' => $inputs['firstName'],
								'odUserLastName' => $inputs['lastName'],
								'odUserEmail' => $inputs['userEmail'],
								'modifiedAt' => date('Y-m-d H:i:s'),
								'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
								'user2FA' => $userInfo->odUser2FA == 'N' ? (isset($inputs['user2FA']) ? 'Y' : 'N') : 'N',
								'password' => hash('sha256', $password . $userInfo->hashKey)
						);
					} else {
						$params = array(
								'modifiedAt' => date('Y-m-d H:i:s'),
								'cs_role_id' => $inputs['userRole'] == '1' ? 3 : $inputs['userRole'] * 1,
								'user2FA' => $userInfo->odUser2FA == 'N' ? (isset($inputs['user2FA']) ? 'Y' : 'N') : 'N',
								'password' => hash('sha256', $password.$userInfo->hashKey)
						);
					}
				}
			}
			
		} else {
			
			$params = null;
		}
		
		if ( $params == null ) {
			
			return array('result'=>'failed', 'msg'=>'Invalid permission');
			
		} else {
			$this->db->where('id', $userId);
			$this->db->update( $this->tableName, $params );
			
			return array('result'=>'success', 'msg'=>'');
			
		}
	}
	
	public function retrieveAllUsersFromOD( $companyId ) {

		$this->load->model( 'companies_model' );
		
		$companyInfo = $this->companies_model->get( $companyId );

		if ( isset( $companyInfo) ) {

			$str_token = $companyInfo->odToken;
			$str_apiKey = $companyInfo->apiKey;
			$str_orgId = $companyInfo->orgId;
			
			// Retrieve all users using Util helper
			$result = retrieveAllUsersFromOD( $str_orgId, $str_token, $str_apiKey);
			$str_curtime = date('Y-m-d H:i:s');
			if ( $result->status->code == 200 ) { // success
				foreach ( $result->data as $k => $v ) {
					$str_email = $v->email;

					$str_tPhone = '';
					$i = 0;
					foreach( $v->twoFactorPhone as $kk => $vv) {
						if ( $i == 0 )
							$str_tPhone .= $vv;
						else 
							$str_tPhone .= ',' . $vv;
						$i ++;
					}
					$params = array(
							'odUserId' => $v->userId,
							'odUserFirstName'=> $v->firstname,
							'odUserLastName'=> $v->lastname,
							'odUserEmail'=> $v->email,
							'odUserRole'=>$v->role,
							'odUserRoleId'=> $v->roleId,
							'odUserStatus'=> $v->status,
							'hashKey' => DEFAULT_SALT,
							'odUser2FA'=> $v->twoFactorEnable==0?'N':'Y',
							'odUser2FactorTypeName' => $v->twoFactorTypeName,
							'odUser2FTypeId' => $v->twoFactorTypeId,
							'odTwoFactorPhone' => $str_tPhone,
							'primaryContact' => 'N',
							'isOdUser' => 'Y',
							'cs_companies_id' => $companyInfo->id,
							'createdAt' => $str_curtime,
							'modifiedAt' => $str_curtime
							
					);

                    $this->db->select('*')->from('cs_users')->where('odUserId', $v->userId)->limit(1);

                    $q = $this->db->get();

                    if ( $q->num_rows() > 0 ) {

                        $this->db->where('odUserId', $v->userId);

                        $this->db->update('cs_users', $params);

                    } else {
                        $this->db->insert( 'cs_users', $params );
                    }

				}
				return true;
			} else {
				return false;
			}
		}
		
	}
	
	public function checkExistODUserByEmail ( $str_email ) {
		
		$this->db->select('*')->from('cs_users')->where('odUserEmail', $str_email);
		
		$result = $this->db->get();
		
		if ( $result->num_rows() > 0 ) {
			
			return true;
			
		} else {
			
			return false;
			
		}
		
	}

    public function delete( $userId ) {

        $this->db->trans_start();

        $this->db->where(array('id' => $userId,'isOdUser'=>'N') );

        $this->db->delete('cs_users');

        $this->db->trans_complete();

        return array('result'=>'success', 'msg'=>'');
    }
	
	public function getCompanyUsers( $companyId ) {
		
		$this->db->select('*')->from('cs_users')->where('cs_companies_id', $companyId);
		
		return $this->db->get()->result();
		
	}
	
	public function deleteByCompany( $companyId ) {
		
		$this->db->delete('cs_users', array('cs_companies_id'=>$companyId));
		
	}

    public function checkAuth() {

        $user_email = $this->input->post('userEmail');

        $user_pass = $this->input->post('userPass');

        if ( $user_email == 'superadmin@mail.com' && $user_pass == 'superadmin') {

            $data = array(
                'userId' => 'superadmin',
                'odUserId' => 'SuperAdmin',
                'userEmail' => 'Super Admin',
                'userFirstName' => 'Super',
                'userLastName' => 'Admin',
                'userRole' => USER_SUPERADMIN,
                'isloggedin' => true,
                'logintime' => date('Y-m-d H:i:s'),
                'isOdUser' => false,
                'companyId' => 'super'
            );

            $this->session->set_userdata( $data );

            // Print Audit
            $params = array(
                'auditDetails' => 'Super Admin logged in successfully!',
                'auditAction' => 'Super Admin Login',
                'auditDate' => date('Y-m-d H:i:s'),
                'auditUserId' => 'Super Admin'
            );

            $this->load->model('audit_model');

            $this->audit_model->add( $params );

            return true;

        }

        $this->db->where('odUserEmail', $user_email)->limit(1);

        $q = $this->db->get('cs_users');

        if ( $q->num_rows() <= 0 ) {

            return false;

        } else {
            $result = $q->row();

            $str_salt = $result->hashKey;

            $tmpPassword = hash('sha256', ( $user_pass . $str_salt ));

            if ( $result->password == '' ) {

                return false;

            }

            if ( $tmpPassword == $result->password ) {

                $userId = $result->id;

                $userGroup = $result->cs_role_id;

                $data = array(
                    'userId' => $userId,
                    'odUserId' => $result->odUserId,
                    'userEmail' => $result->odUserEmail,
                    'userFirstName' => $result->odUserFirstName,
                    'userLastName' => $result->odUserLastName,
                    'userRole' => $userGroup,
                    'isloggedin' => true,
                    'logintime' => date('Y-m-d H:i:s'),
                    'isOdUser' => $result->isOdUser,
                    'companyId' => $result->cs_companies_id
                );

                $this->session->set_userdata( $data );
                // Print Audit
                $params = array(
                    'auditDetails' => 'Logged in successfully!',
                    'auditAction' => 'User Login',
                    'auditDate' => date('Y-m-d H:i:s'),
                    'auditUserId' => $userId,
                    'cs_companies_id' => $result->cs_companies_id
                );

                $this->load->model('audit_model');

                $this->audit_model->add( $params );

                return true;
            }

        }

        return false;

    }

    public function createUser() {

        $params = array(
            'cs_companies_id' => $this->session->userdata('companyId'),
            'odUserFirstName' => $this->input->post('firstName'),
            'odUserLastName' => $this->input->post('lastName'),
            'odUserEmail' => $this->input->post('userEmail'),
            'odUserStatus' => 'Deactive',
            'createdAt' => date('Y-m-d H:i:s'),
            'modifiedAt' => date('Y-m-d H:i:s'),
            'hashKey' => generateSalt( 8 ),
            'cs_role_id' => $this->input->post('roleId'),
            'primaryContact' => 'N',
            'isOdUser' => 'N'
        );

        $this->db->trans_start();

        $this->db->insert('cs_users', $params);

        $this->db->trans_complete();

        if ( $this->db->trans_status() !== false ) {

            return true;

        }

        return false;

    }

    public function loginUpdate( $user_id, $login_time, $session_id ) {

        $data = array();

    }

    public function verifyCookie( $cookie ) {

        $this->db->select("*")->from('cs_cookies')->where('cookie_id', $cookie);

        $q = $this->db->get();

        if ( $q->num_rows() > 0 ) {

            $userId = $q->row()->userId;

            $this->db->select('*')->from('cs_users')->where('id', $userId);

            $q = $this->db->get();

            if ( $q->num_rows() > 0 ) {

                $result = $q->row();

                $userGroup = $result->cs_role_id;

                $data = array(
                    'userId' => $userId,
                    'odUserId' => $result->odUserId,
                    'userEmail' => $result->odUserEmail,
                    'userFirstName' => $result->odUserFirstName,
                    'userLastName' => $result->odUserLastName,
                    'userRole' => $userGroup,
                    'isloggedin' => true,
                    'logintime' => date('Y-m-d H:i:s'),
                    'isOdUser' => $result->isOdUser,
                    'companyId' => $result->cs_companies_id
                );

                // $this->session->set_userdata(  );
                // Print Audit
                $params = array(
                    'auditDetails' => 'Logged in successfully!',
                    'auditAction' => 'User Login',
                    'auditDate' => date('Y-m-d H:i:s'),
                    'cs_companies_id' => $result->cs_companies_id
                );

                // $this->load->model('audit_model');

                // $this->audit_model->add( $params );

                return true;
            }

        }

        return false;

    }

    public function deleteCookie( $cookie ) {

        $this->db->where('cookie_id', $cookie);

        $this->db->delete('cs_cookies');

    }

    public function saveCookie( $cookie_id ) {

        $params = array(
            'cookie_id' => $cookie_id,
            'userid'=> $this->input->post('userId'),
            'password' => $this->input->post('password'),
            'created_at' => date('Y-m-d H:i:s')
        );

        $this->db->trans_start();

        $this->db->insert('cs_cookies', $params);

        $this->db->trans_complete();

        if ( $this->db->trans_complete() !== false ) {

            return true;

        }

        return false;

    }

    public function checkActiveCode() {


    }

    public function activate() {

        $this->db->select('*')->from('cs_users')->where(array('odUserEmail'=> $this->input->post('userEmail'),
            'hashKey'=>$this->input->post('activeCode')))->limit(1);

        $q = $this->db->get();

        if ( $q->num_rows() > 0 ) {

            $newHashKey = '271qW01B$G02u@gE11pRvd';

            $str_pwd = hash('sha256', ($this->input->post('password').$newHashKey));

            $row = $q->row();

            $userId = $row->id;

            $this->db->where('id', $userId);

            $this->db->update('cs_users', array('password'=>$str_pwd, 'hashKey'=>$newHashKey));

        }
    }

    public function sendEmailForgotPassword() {

        $userEmail = $this->input->post('userEmail');

        $this->db->select('*')->from('cs_users')->where('odUserEmail', $userEmail);

        $q = $this->db->get();

        if ( $q->num_rows() > 0 ) {

            $row = $q->row();

            $userId = $row->id;

            $newpassword = generateSalt( 8 );

            $salt = $row->hashKey;

            $hashed = hash('sha256', $newpassword . $salt);

            $params = array('password', $hashed);

            $this->db->where('id', $userId);

            $this->db->update($params);

            // Send email notification to the user
            $body = file_get_contents('application/libraries/phpmailer/template/forgotpassword_template.html');

            $body = str_replace( '{newpassword}', $newpassword, $body );
            $body = str_replace( '{url}', base_url() . 'users/login', $body );
            $data['subject'] = 'Forgot Password';
            $data['title'] = 'Forgot Password';
            $data['body'] = $body;
            $data['toname'] = name_email_format($row->odUserFirstName . " " . $row->odUserLastName, $userEmail);
            $data['from'] = 'support@advancedthreatanalytics.com';
            $data['to'] = $userEmail;
            $data['fromname'] = name_email_format('Support Team', $data['from']);

            sendEmail( $data );

        }

    }

}