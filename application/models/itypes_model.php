<?php
class Itypes_model extends CI_Model {
	
	function __construct() {
		
		parent::__construct();
		
	}
	
	public function getList() {
		
		$this->db->select("ci.*, cu.odUserFirstName, cu.odUserLastName")->from('cs_itype ci');
		
		$this->db->join('cs_users cu','cu.id=ci.modifiedId','left');
		
		$result = $this->db->get()->result();
		
		return $result;
		
	}
	
	public function get( $itypeId ) {
		

		$this->db->where("ci.id", $itypeId);
		
		$this->db->select("ci.*, cu.odUserFirstName, cu.odUserLastName")->from('cs_itype ci');
		
		$this->db->join('cs_users cu','cu.id=ci.modifiedId','left');
				
		$result = $this->db->get()->result();
		
		return isset($result) ? $result[0] : null;
		
	}
	
	public function checkDuplication ( $str_itype ) {
		
		$this->db->select("*")->from("cs_itype")->where('itypeType', $str_itype);
		
		return $this->db->count_all_results() > 0;
		
	}
	
	public function add() {
		
		$inputs = $this->input->post();
		
		if ( isset( $inputs ) ) {
			
			if ( $this->checkDuplication($inputs['itypeType']))
				return array('result'=>'failed', 'msg'=>'Duplicated iType');
			
			$params = array(
					'itypeType' => $inputs['itypeType'],
					'description' => $inputs['description'],
					'createdAt'=>date("Y-m-d H:i:s"),
					'modifiedAt'=>date("Y-m-d H:i:s"),
					// 'modifiedId' =>'39'
			);
				
			$this->db->insert('cs_itype', $params);
				
			$itypeId = $this->db->insert_id();
				
			$itype = $this->get( $itypeId );
				
			return array('result'=>'success', 'msg'=>'', 'iType' => $itype);
			
		}
		
		return array('result'=>'failed', 'msg'=>'Bad Request');
		
	}
	
	public function delete( $itypeId ) {
		
		$this->db->where('id', $itypeId);
		
		$this->db->delete('cs_itype');
		
		return array('result'=>'success', 'msg'=>'');
		
	}
	
	public function update() {
		
		$inputs = $this->input->post();

		if ( isset( $inputs ) ) {
			
			//if ( $this->checkDuplication($inputs['itypeType']))
			//	return array('result'=>'failed', 'msg'=>'Duplicated iType');
				
			$params = array(
					'itypeType'=> $inputs['itypeType'],
					'description'=>$inputs['description'],
					'modifiedAt'=>date("Y-m-d H:i:s"),
					// 'modifiedId'=>'39'
			);
				
			$this->db->where('id', $inputs['itypeId']);
				
			if (!$this->db->update('cs_itype', $params))
				return array('result'=>'failed', 'msg'=>'Invalid Request');
				
			return array('result'=>'success', 'msg'=>'');
				
		}
		
		return array('result'=>'failed', 'msg'=>'Bad Request');
		
	}
}