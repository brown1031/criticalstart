<?php
class Roles_model extends CI_Model {
	
	function __construct() {
		
		parent::__construct();
		
	}
	
	public function getlist() {
		
		$this->db->select("*")->from('cs_role');
		
		$result = $this->db->get()->result();
		
		return $result;
		
	}
	
	public function get( $roleId ) {
		
		$this->db->select("*")->from('cs_role')->where('id', $roleId);
		
		$result = $this->db->get()->result();
		
		return isset( $result ) ? $result[0] : null;
		
	}
	
	public function checkDuplication( $strRoleName ) {
		
		$this->db->select("*")->from("cs_role")->where('roleName', $strRoleName);
		
		return $this->db->count_all_results() > 0;
	}
	
	public function add() {
		
		$inputs = $this->input->post();
		
		if ( isset( $inputs ) ) {
			if($this->checkDuplication($inputs['roleName']))
				return array('result'=>'failed', 'msg'=>'Duplicated role name');
			$params = array(
				'roleName' => $inputs['roleName'],
				'roleDescription' => $inputs['roleDescription']
			);
			
			$this->db->insert('cs_role', $params);
			
			$roleId = $this->db->insert_id();
			
			$userRole = $this->get($roleId);
			
			return array('result'=>'success', 'msg'=>'', 'userRole'=>$userRole);
		}
		
		return array('result'=>'failed', 'msg'=>'Bad Request');
	}
	
	public function delete($roleId) {
		
		$this->db->where('id', $roleId);
		
		$this->db->delete('cs_role');
		
		return array('result'=>'success', 'msg'=>'');
		
	}
	
	public function update() {
		
		$inputs = $this->input->post();
		
		if ( isset($inputs) ) {
			
			$params = array(
				'roleName' => $inputs['roleName'],
				'roleDescription' => $inputs['roleDescription']
			);
			
			$roleId = $inputs['roleId'];
			
			$this->db->where('id', $roleId);
			
			$this->db->update('cs_role', $params);
			
			return array('result'=>'success', 'msg'=>'');
		}
		
		return array('result'=>'failed', 'msg'=>'Bad Request');
	}
}