<?php
class Commons_model extends CI_Model {
	
	public function __construct() {
	
		parent::__construct();
		
	}

	// Check Access Control Start
	public function isLoggedIn() {

		if ( $this->session->userdata('userId') && $this->session->userdata('isloggedin') ) {
				
			return true;
				
		}

        return false;
	
	}
	
	public function getUserGroup() {
		
		if ( $this->isLoggedIn() ) {
			
			return $this->session->userdata('userRole');
			
		}
		
		return USERGROUP_NOT_LOGGEDIN;
		
	}
	
	public function isSuperAdmin() {
		
		if ( $this->isLoggedIn() ) {
			
			return $this->session->userdata('userRole') == USER_SUPERADMIN ? true:false;
			
		}
		
		return false;
		
	}
	
	public function isReadonlyAdmin() {
		
		if ( $this->isLoggedIn() ) {
			
			return $this->session->userdata('userRole') == USER_ROADMIN ? true : false;
			
		}
		
		return false;
		
	}
	
	public function isReadonlyUser() {
		
		if ( $this->isLoggedIn() ) {
			
			return $this->session->userdata('userRole') == USER_ROUSER ? true : false;
			
		}
		
		return false;
	}
	
	public function isDefaultUser() {
		
		if ( $this->isLoggedIn() ) {
			
			return $this->session->userdata('userRole') == USER_DEFAULTUSER ? true : false;
			
		}
		
		return false;
	}
	// Check Access Control End
	
	// EMail function start
	public function getSupportEmailAddress() {
		
		$this->db->select('supportEmail')->from('cs_siteconfig');
		
		$result = $this->db->get()->result();
		
		return $result ? $result[0]->supportEmail : 'support@advancedthreatanalytics.com';
		
	}
	
	public function sendEmail( $data ) {
		
		$this->load->library('phpmailer/phpmailer');
		$mail = new PHPMailer();
			
		$mail->IsSMTP(); 				// telling the class to use SMTP
		$mail->SMTPDebug = 0;           // enables SMTP debug information (for testing)
		// 1 = errors and messages
		// 2 = messages only
		$mail->SMTPAuth   = true;                    // enable SMTP authentication
		$mail->SMTPSecure = "ssl";                   // sets the prefix to the servier
		$mail->Host       = EMAIL_HOST;        // sets GMAIL as the SMTP server
		$mail->Port       = EMAIL_HOST_PORT;                     // set the SMTP port for the GMAIL server
		$mail->Username   = EMAIL_DEV_ACCOUNT;  		     // GMAIL username
		$mail->Password   = EMAIL_DEV_PASS;           // GMAIL password
			
		$mail->SetFrom($data['title'], SITE_TITLE);
		$mail->Subject    = $data['subject'];
		$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!";     // optional, comment out and test
		$mail->MsgHTML( $data['body'] );
		$mail->From = $data['from'];
	
		$mail->AddAddress($data['to'], $data['toname']);
			
		//$mail->AddAttachment("images/phpmailer.gif");      // attachment
		//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
			
		if(!$mail->Send()) {
			return ("Mailer Error: " . $mail->ErrorInfo);
		}
		
		return 'Success to send email';
	}
	// EMail function end

    public function printAudit( $params ) {

        $this->load->model('audit_model');

        $this->audit_model->add( $params );

    }
}