<?php
class Alerttypes_model extends CI_Model {
	
	function __construct() {
		
		parent::__construct();
		
	}
	
	public function getList() {
		
		$this->db->select("ca.*, cu.odUserFirstName, cu.odUserLastName")->from('cs_alerttype ca');
		
		$this->db->join('cs_users cu','cu.id=ca.modifiedId','left');
		
		$result = $this->db->get()->result();
		
		return $result;
		
	}
	
	public function get( $alertTypeId ) {
		
		$this->db->where( 'ca.id', $alertTypeId );
		
		$this->db->select("ca.*, cu.odUserFirstName, cu.odUserLastName")->from('cs_alerttype ca');
		
		$this->db->join('cs_users cu','cu.id=ca.modifiedId','left');
		
		$result = $this->db->get()->result();
		
		return isset($result) ? $result[0] : null;
		
	}
	
	public function checkDuplication( $strType ) {
		
		$this->db->select("*")->from('cs_alerttype')->where('type', $strType);
		
		return $this->db->count_all_results() > 0;
	}
	
	public function add() {
		
		$inputs = $this->input->post();
		
		if ( isset( $inputs ) ) {
			
			if ( $this->checkDuplication($inputs['alertType']))
				return array('result'=>'failed', 'msg' => 'Duplicated alert type');
			
			$params = array(
				'type' => $inputs['alertType'],
				'description' => $inputs['description'],
				'createdAt'=>date("Y-m-d H:i:s"),
				'modifiedAt'=>date("Y-m-d H:i:s"),
				//'modifiedId' =>'39'
			);
			
			$this->db->insert('cs_alerttype', $params);
			
			$alertTypeId = $this->db->insert_id();
			
			$alertType = $this->get( $alertTypeId );
			
			return array('result'=>'success', 'msg'=>'', 'alertType' => $alertType);
		}
		
		return array('result'=>'failed', 'msg' => 'Bad Request');
		
	}
	
	public function delete( $alertTypeId ) {
		
		$this->db->where('id', $alertTypeId );
		
		$this->db->delete('cs_alerttype');
		
		return array('result'=>'success', 'msg'=>'');
		
	}
	
	public function update() {
		
		$inputs = $this->input->post();
		
		if ( isset( $inputs ) ) {
			
			$params = array(
				'type'=> $inputs['alertType'],
				'description'=>$inputs['description'],
				'modifiedAt'=>date("Y-m-d H:i:s"),
				//'modifiedId'=>'39'
			);
			
			$this->db->where('id', $inputs['alertTypeId']);
			
			$this->db->update('cs_alerttype', $params);
			
			return array('result'=>'success', 'msg'=>'');
			
		} 
		
		return array('result'=>'failed', 'msg'=>'Bad Request');
		
	}
}