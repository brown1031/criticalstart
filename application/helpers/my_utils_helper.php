<?php if (!(defined('BASEPATH'))) exit('No direct script access allowed');

// Log Function 
if ( !function_exists('printLog')) {
	function printLog($sLogType, $strLog) {
		log_message($sLogType, $strLog);
	}
}

if ( !function_exists('generateSalt')) {
	function generateSalt ( $length = 32 ) {
		$strChars = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$strRnd = '';
		for ( $i = 0; $i < $length; $i ++ ) {
			$strRnd .= $strChars[ rand(0, strlen($strChars) - 1) ];
		}
	
		return $strRnd;
	}
}

if ( !function_exists('sendHTTPPost')) {
	function sendHTTPPost($strUrl, $params) {
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $strUrl,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $params,
			CURLOPT_SSL_VERIFYPEER => false
		);
		curl_setopt_array($ch, $options);
		$resp = curl_exec($ch);
		$errorno = curl_errno($ch);
		$result = null;
		if ( $errorno != 0 ) {
			printLog('error', 'curl error: ' . curl_error($ch));
		} else {
			$result = json_decode( $resp );
		}
		
		curl_close($ch);
		
		return $result;
	}
}

if ( !function_exists('sendHTTPGet')) {
	function sendHTTPGet($strUrl) {
		$ch = curl_init();
		$options = array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $strUrl,
				CURLOPT_FOLLOWLOCATION => 1,
				CURLOPT_HTTPHEADER => array(
					"Content-Type" => 'application/json'
				),
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_POST => 0
		);
		curl_setopt_array($ch, $options);
		$resp = curl_exec($ch);
		$errorno = curl_errno($ch);
		$result = null;
		if ( $errorno != 0 ) {
			printLog('error', 'curl error: ' . curl_error($ch));
		} else {
			$result = json_decode( $resp );
		}
		
		curl_close($ch);
		
		return $result;
	}
}

if ( !function_exists('getOpenDNSToken')) {
	function getOpenDNSToken( $params ) {
		$str_url = 'https://api.opendns.com/v3/users/me/tokens?outputFormat=jsonHttpStatusOverride&api-key=' . $params['apiKey'];
		$param_data = '{"email":"' . $params['name'] . '","passphrase":"' . $params['password'] . '"}';
		$result = sendHTTPPost($str_url, $param_data);
		return $result;
	}
}

if ( !function_exists('apiGetCompanyInfo')) {
	function apiGetCompanyInfo( $str_orgId, $str_token, $str_apiKey) {
		$str_url = 'https://api.opendns.com/v3/organizations/' . $str_orgId . '&&outputFormat=jsonHttpStatusOverride&token=' . $str_token . '&api-key=' . $str_apiKey;
		$result = sendHTTPGet($str_url);
		return $result;
	}
}

if ( !function_exists('retrieveAllUsersFromOD') ) {
	function retrieveAllUsersFromOD( $str_orgId, $str_token, $str_apiKey ) {
		// 'https://api.opendns.com/v3/organizations/1142299/users?offset=0&limit=50&sort={%22displayname%22:%22asc%22}&&outputFormat=jsonHttpStatusOverride&token=112D8BFA1F4482049849391CAE1C6169&api-key=4190F5204439FBCC08E06DBF2B4480F7'
		
		$str_url = 'https://api.opendns.com/v3/organizations/' . $str_orgId . '/users?offset=0&limit=50&sort={%22displayname%22:%22asc%22}&&outputFormat=jsonHttpStatusOverride&token=' . $str_token . '&api-key=' . $str_apiKey;
		$result = sendHTTPGet($str_url);
		return $result;
	}
}

if ( !function_exists('retrieveAllDomainsFromOD') ) {
	function retrieveAllDomainsFromOD($str_orgId, $str_token, $str_apiKey, $offset, $count) {
		// curl -H "Content-Type: application/json" -X GET 'https://api.opendns.com/v3/organizations/1142299/domainlists?offset=0&limit=50&&&outputFormat=jsonHttpStatusOverride&token=112D8BFA1F4482049849391CAE1C6169&api-key=4190F5204439FBCC08E06DBF2B4480F7'
		$str_url = 'https://api.opendns.com/v3/organizations/' . $str_orgId . '/domainlists?offset=' . $offset . '&limit=' . $count . '&outputFormat=jsonHttpStatusOverride&token='.$str_token.'&api-key='.$str_apiKey;
		$result = sendHTTPGet($str_url);
		return $result;
	}
}

if ( !function_exists('retrieveAllSubdomainsFromOD') ) {
	function retrieveAllSubdomainsFromOD( $str_orgId, $str_token, $str_apiKey, $offset, $limit, $domainListId) {
		// curl -H "Content-Type: application/json" -X GET 'https://api.opendns.com/v3/organizations/1142299/domainlists/332157/domains?offset=0&limit=50&&&outputFormat=jsonHttpStatusOverride&token=112D8BFA1F4482049849391CAE1C6169&api-key=4190F5204439FBCC08E06DBF2B4480F7'
		$str_url = 'https://api.opendns.com/v3/organizations/' . $str_orgId . '/domainlists/' . $domainListId . '/domains?offset=' . $offset . '&limit=' . $limit . '&outputFormat=jsonHttpStatusOverride&token='.$str_token.'&api-key='.$str_apiKey;
		$result = sendHTTPGet( $str_url );
		return $result;
	}
}

if ( !function_exists( 'apiAddDomainList') ) {
	function apiAddDomainList( $str_orgId, $str_token, $str_apiKey, $params) {
		// curl -H "Content-Type: application/json" -X POST 'https://api.opendns.com/v3/organizations/1142299/domainlists?outputFormat=jsonHttpStatusOverride&token=112D8BFA1F4482049849391CAE1C6169&api-key=4190F5204439FBCC08E06DBF2B4480F7' -d '{"isGlobal":false,"name":"Critical Start Threat Intel","access":"block","domains":[]}'
		
		$str_url = 'https://api.opendns.com/v3/organizations/' . $str_orgId . '/domainlists?outputFormat=jsonHttpStatusOverride&token='.$str_token.'&api-key='.$str_apiKey;
		
		$param = '{"isGlobal":false,"name":"'.$params['name'].'","access":"'.$params['access'].'","domains":[]}';
		
		// printLog('error', $param);
		
		$result = sendHTTPPost($str_url, $param);
		
		return $result;
	
	}
}

if ( !function_exists( 'apiDeleteDomainList') ) {
	
	function apiDeleteDomainList( $str_orgId, $str_token, $str_apiKey, $params ) {
		
		$str_url = "https://api.opendns.com/v3/organizations/" . $str_orgId . "/domainlists/" . $params->odId . "?outputFormat=jsonHttpStatusOverride&token=" . $str_token . "&api-key=" . $str_apiKey . "&method_override=DELETE";

		// printLog("error", $str_url);
		
		$result = sendHTTPPost( $str_url, null);
		
		return $result;		
	}
	
}

// Update domainlist
if ( !function_exists( 'apiUpdateDomainList' ) ) {
	
	function apiUpdateDomainList( $str_orgId, $str_token, $str_apiKey, $params ) {
		
		$str_url = "https://api.opendns.com/v3/organizations/" . $str_orgId . "/domainlists/" . $params['odId'] . "?outputFormat=jsonHttpStatusOverride&token=" . $str_token . "&api-key=" . $str_apiKey . "&method_override=PUT";
		
		$str_param = "{\"isGlobal\":" . ($params['odisGlobal'] == 'Y'?"true" : "false") . ',"id":' . $params['odId'] . ',"organizationId":' . $str_orgId . ',"access":"' . $params['odAccess'] . '","addDomains":[],"isMspDefault":' . ($params['odIsMspDefault']=='Y'?'true':'false,') . '"name":"' . $params['odName'] . '","removeDomains":[],' .'"thirdpartyCategoryId":null,"domainCount":1}';

		//printLog('error', 'strparam'. $str_param . $str_url);
		
		$result = sendHTTPPost($str_url, $str_param);
		
		return $result;
		
	}
	
}

if ( !function_exists('apiAddNewDomain') ) {
	
	function apiAddNewDomain( $str_orgId, $str_token, $str_apiKey, $params, $domainName ) {
		
		$str_url = "https://api.opendns.com/v3/organizations/" . $str_orgId . "/domainlists/" . $params->odId . "?outputFormat=jsonHttpStatusOverride&token=" . $str_token . "&api-key=" . $str_apiKey . "&method_override=PUT";
		
		$str_param = "{\"isGlobal\":" . ($params->odisGlobal == 'Y'?"true" : "false") . ',"id":' . $params->odId . ',"organizationId":' . $str_orgId . ',"access":"' . $params->odAccess . '","addDomains":["' . $domainName . '"],"isMspDefault":' . ($params->odIsMspDefault=='Y'?'true':'false,') . '"name":"' . $params->odName . '","removeDomains":[],' .'"thirdpartyCategoryId":null,"domainCount":1}';
		
		//printLog('error', 'strparam'. $str_param . $str_url);
		
		$result = sendHTTPPost($str_url, $str_param);
		
		return $result;
		
	}
}

// Remove domain
if ( !function_exists('apiRemoveDomain') ) {
	
	function apiRemoveDomain( $str_orgId, $str_token, $str_apiKey, $params, $domainName ) {
		
		$str_url = "https://api.opendns.com/v3/organizations/" . $str_orgId . "/domainlists/" . $params->odId . "?outputFormat=jsonHttpStatusOverride&token=" . $str_token . "&api-key=" . $str_apiKey . "&method_override=PUT";
		
		$str_param = "{\"isGlobal\":" . ($params->odisGlobal == 'Y'?"true" : "false") . ',"id":' . $params->odId . ',"organizationId":' . $str_orgId . ',"access":"' . $params->odAccess . '","addDomains":[],"isMspDefault":' . ($params->odIsMspDefault=='Y'?'true':'false,') . '"name":"' . $params->odName . '","removeDomains":["' . $domainName . '"],' .'"thirdpartyCategoryId":null,"domainCount":1}';
		
		//printLog('error', 'strparam'. $str_param . $str_url);
		
		$result = sendHTTPPost($str_url, $str_param);
		
		return $result;
		
	}
	
}

// Send Email function
/**
 * Params : titie, subject, body, from, to, toname, fromname
 */
if ( !function_exists( 'sendEmail' ) ) {

    function sendEmail( $data ) {

        $mail = new PHPMailer();

        $mail->IsSMTP(); 				// telling the class to use SMTP
        $mail->SMTPDebug = 4;           // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth   = true;                    // enable SMTP authentication
        $mail->SMTPSecure = "ssl";                   // sets the prefix to the servier
        $mail->Host       = EMAIL_HOST;        // sets GMAIL as the SMTP server
        $mail->Port       = EMAIL_HOST_PORT;                     // set the SMTP port for the GMAIL server
        $mail->Username   = EMAIL_DEV_ACCOUNT;  		     // GMAIL username
        $mail->Password   = EMAIL_DEV_PASS;           // GMAIL password

        $mail->SetFrom($data['title'], SITE_TITLE);
        $mail->Subject    = $data['subject'];
        $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!";     // optional, comment out and test
        $mail->MsgHTML( $data['body'] );
        $mail->From = $data['from'];
        $mail->FromName = $data['fromname'];

        $mail->AddAddress($data['to'], $data['toname']);

        //$mail->AddAttachment("images/phpmailer.gif");      // attachment
        //$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

        if(!$mail->Send()) {
            return false;
        }

        return true;
    }
}

// Check string is ip address
if ( !function_exists( 'is_ipaddress' ) ) {
	
	function is_ipaddress( $str_ipaddress ) {
		
		if( !$str_ipaddress or strlen(trim($str_ipaddress)) == 0){
			return false;
		}
		
		$ip=trim($str_ipaddress);
		
		if( preg_match( "/^[0-9]{1,3}(.[0-9]{1,3}){3}$/", $str_ipaddress ) ) {
			
			foreach(explode(".", $str_ipaddress) as $block) {
				
				if($block < 0 || $block > 255 )
					return false;
				
				return true;
				
			}
		}
		
		return false;		
	}
}