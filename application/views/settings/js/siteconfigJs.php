<script type="text/javascript">

var strBaseUrl = '<?php echo base_url();?>';

$(document).ready(function() {

	initEditableTable();
	
});

function initEditableTable () {
	
	$.fn.editable.defaults.success = function ( data ) {

		console.log( data );
		
	};

	$.fn.editable.defaults.url = "<?php echo base_url() . "settings/update";?>";
	
	// Init support email
	$("#supportEmail").editable({
		type: "email",
		name: 'supportEmail',
		title: 'Enter Support Email'
	});	
	
	$("#apiKey").editable({
		type: "text",
		name: 'apiKey',
		title: 'Enter API Key'
	});	

	$("#phone").editable({
		type: "text",
		name: "phone",
		title: "Enter Phone Number"
	});

	$("#httpPort").editable({
		type: "text",
		name: "httpPort",
		title: "Enter HTTP Port"
	});

	$("#maxDomainList").editable({
		type: 'text',
		name: 'maxDomainList',
		title: 'MAX Domain List'
	});

	$("#minPassword").editable({
		type: 'text',
		name: 'minPassword',
		title: 'Min Password Length'
	});
	
	$("#maxPassword").editable({
		type: 'text',
		name: 'maxPassword',
		title: 'Max Password Length'
	});

	$("#failedAttempts").editable({
		type: 'text',
		name: 'failedAttempts',
		title: 'Failed Attempts'
	});

	$("#lockout").editable({
		type: 'text',
		name: 'lockout',
		title: 'Lockout'
	});

	$("#rememberMe").editable({
		type: 'text',
		name: 'rememberMe',
		title: 'Remember Me'
	});

	$("#smsUrl").editable({
		type: 'text',
		name: 'smsUrl',
		title: 'SMS URL'
	});

	$("#smsUser").editable({
		type: 'text',
		name: 'smsUser',
		title: 'SMS Username'
	});

	$("#smsPassword").editable({
		type: 'text',
		name: 'smsPassword',
		title: 'SMS Password'
	});

	$("#smsFrom").editable({
		type: 'text',
		name: 'smsFrom',
		title: 'SMS From'
	});

	$("#smsApiId").editable({
		type: 'text',
		name: 'smsApiId',
		title: 'SMS API ID'
	});

	$("#smsCertificatie").editable({
		type: 'selected2',
		name: 'smsCertificatie',
		source: [{id: 'Y', text: 'YES'}, {id: 'N', text: 'NO'}],
		title: 'SMS Certificate'
	});

	$("#retentionPeriod").editable({
		type: 'text',
		name: 'retentionPeriod',
		title: 'Retention Period'
	});

	
}

</script>