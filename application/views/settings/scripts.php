<!-- BEGIN CONTENT -->
<div ng-app="CSMainApp">
	<div ng-controller="ScriptsController" ng-init="init('<?php echo base_url(); ?>');">
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">Settings</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<span>Settings</span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span>Scripts</span>
						</li>					
					</ul>
				</div>
				<!-- END PAGE HEADER-->
				<div class="clearfix">
				</div>
				<div class="row">
				    <div class="col-sm-12">
	                    <div class="portlet box blue">
	    					<div class="portlet-title">
	                            <div class="caption">
	    							<i class="fa fa-edit"></i>Scripts
	    						</div>
	                            <div class="actions" data-toggle="modal" data-target="#idDialog">
	    							<a href="#" class="btn btn-default btn-sm">
	    							<i class="fa fa-plus"></i> Add New </a>
	    						</div>
	    					</div>
	    					<div class="portlet-body">
	    						<div class="table-scrollable">
	    							<table class="table table-bordered table-hover">
	    								<thead>
	    									<tr>
		    									<th>#</th>
		    									<th>File Name</th>
		    									<th>File Path</th>
		    									<th>Description</th>
		    									<th>Action</th>
		    								</tr>
	    								</thead>
	    								<tbody ng-repeat="item in scripts" on-finish-render="ngRepeatFinished">
	    									<tr >
	    										<td class="col-md-1">{{$index + 1}}</td>
	    										<td class="col-md-2"><a href="javascript:;" ng-click="item.bedit=!item.bedit"><span>{{item.fileName}}</span></a></td>
	    										<td class="col-md-2">{{item.filePath}}</td>
	    										<td class="col-md-6">{{item.description}}</td>
	    										<td class="text-center col-md-1">
	    											<span class="btn red btn-sm" ng-click="onDeleteScript($index);"><i class="fa fa-trash-o"></i> Delete</span>
	    										</td>
	    									</tr>
	    									<tr ng-show="item.bedit">
	    										<td colspan="8">
													<form class="form-horizontal" action="<?php echo base_url() . 'settings/updateScript'; ?>" method="POST" id="editScriptsForm_{{$index}}">
														<input type="hidden" ng-value="item.id" name="scriptId">
	                                                    <div class="row no-margin">
                                                    		<div class="form-group col-md-6">
                                                    			<label class="control-label col-md-3">File Name</label>
                                                    			<div class="col-md-9">
                                                    				<input type="text" class="form-control" ng-model="item.fileName" placeholder="Script File Name" name="fileName"/>
                                                    			</div>                                                    				
                                                    		</div>
                                                    		<div class="form-group col-md-6">
                                                    			<label class="control-label col-md-3">File Path</label>
                                                    			<div class="col-md-9">
                                                    				<input type="text" class="form-control" ng-model="item.filePath" placeholder="Script File Path" name="filePath"/>
                                                    			</div>                                                    				
                                                    		</div>
	                                                    </div>
	                                                    <div class="row no-margin">
                                                    		<div class="form-group col-md-6">
                                                    			<label class="control-label col-md-3">Description</label>
                                                    			<div class="col-md-9">
                                                    				<textarea class="form-control" placeholder="Your description here..." rows="3" ng-model="item.description" name="description"></textarea>
                                                    			</div>                                                    				
                                                    		</div>
                                                    		<div class="form-group col-md-6">
                                                    			<label class="control-label col-md-3">Run Times</label>
                                                    			<div class="col-md-9">
                                                    				<select name="runTimes" class="form-control select2" ng-model="item.runTimes">
                                                    					<option value="0">Run Manually</option>
                                                    					<option value="1">Set Interval ( number of hours )</option>
                                                    				</select>
                                                    			</div>                                                    				
                                                    		</div>
	                                                    </div>
	                                                    <div class="row no-margin">
                                                    		<div class="form-group col-md-6">
                                                    			<label class="control-label col-md-3">Arguments</label>
                                                    			<div class="col-md-9">
                                                    				<input type="text" class="form-control" ng-model="item.scriptArgs" placeholder="Script Arguments" name="scriptArgs"/>
                                                    			</div>                                                    				
                                                    		</div>
                                                    		<div class="form-group col-md-6">
                                                    			<label class="control-label col-md-3">Script Max Runtime</label>
                                                    			<div class="col-md-9">
                                                    				<input type="text" class="form-control" ng-model="item.scriptMaxRuntime" placeholder="Script MAX Runtime" name="scriptMaxRuntime"/>
                                                    			</div>                                                    				
                                                    		</div>
	                                                    </div>
	                                                    <div class="row no-margin">
                                                    		<div class="form-group col-md-6">
                                                    			<label class="control-label col-md-3">Script Organizations</label>
                                                    			<div class="col-md-9">
                                                    				<select name="scriptOrgs" class="form-control select2" ng-model="item.scriptOrgs">
						                                            	<option value="1">All</option>
						                                                <option value="2">Comma Seperated</option>
						                                                <option value="3">No Applicable</option>
						                                            </select>
                                                    			</div>                                                    				
                                                    		</div>
                                                    		<div class="form-group col-md-6">
                                                    			<label class="control-label col-md-3">Script Return Values</label>
                                                    			<div class="col-md-9">
                                                    				<input type="text" class="form-control" ng-model="item.scriptReturnVals" placeholder="Script Return Values" name="scriptReturnVals"/>
                                                    			</div>                                                    				
                                                    		</div>
	                                                    </div>
	                                                    <div class="row no-margin">
                                                    		<div class="form-group col-md-6">
                                                    			<label class="control-label col-md-3">Script Audit</label>
                                                    			<div class="col-md-9">
                                                    				<input type="checkbox" class="make-switch" ng-model="item.scriptAudit" placeholder="Script Audit" name="scriptAudit"/>
                                                    			</div>                                                    				
                                                    		</div>
                                                    		<div class="form-group col-md-6">
                                                    		</div>
	                                                    </div>
	                                                    <div class="row no-margin">
	                                                        <hr/>
	                                                    </div>
	                                                    <div class="row no-margin">
	                                                        <span class="btn btn-danger" ng-click="item.bedit=false">Cancel</span>
	                                                        <span class="btn btn-primary pull-right" ng-click="onUpdateScript( $index )">Update</span>
	                                                    </div>       
	                                                    <div class="row no-margin">
	                                                        <hr/>
	                                                    </div> 
	                                                </form>
	                                            </td>
	    									</tr>
	    								</tbody>
	    							</table>
	    						</div>
	    				    </div>
	    				</div>				    
				    </div>
	                				
				</div>
			</div>
		</div>
		<!-- Dialog - Add New Domain -->
		<div id = "idDialog" class="modal fade">
			<form action="<?php echo base_url() . 'settings/addNewScript';?>" class="horizontal-form" id='formNewScript' method="post">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3>New Script</h3>
						</div>
						<div class="modal-body">
							<div class="form-horizontal">
								<div class="row no-margin">
                                	<div class="form-group">
                                    	<label class="control-label col-md-3">File Name</label>
                                    	<div class="col-md-9">
                                        	<input type="text" class="form-control" placeholder="Script File Name" name="fileName"/>
                                        </div>                                                    				
                                    </div>
                                 </div>
                                <div class="row no-margin">
                                    <div class="form-group">
                                    	<label class="control-label col-md-3">File Path</label>
                                        <div class="col-md-9">
                                        	<input type="text" class="form-control" placeholder="Script File Path" name="filePath"/>
                                        </div>                                                    				
                                    </div>
	                            </div>
	                            <div class="row no-margin">
                                	<div class="form-group">
                                    	<label class="control-label col-md-3">Description</label>
                                        <div class="col-md-9">
                                        	<textarea class="form-control" placeholder="Your description here..." rows="2" name="description"></textarea>
                                        </div>                                                    				
                                    </div>
                                </div>
                                <div class="row no-margin">
                                    <div class="form-group">
                                    	<label class="control-label col-md-3">Run Times</label>
                                        <div class="col-md-9">
                                        	<select name="runTimes" class="form-control select2">
                                            	<option value="0">Run Manually</option>
                                                <option value="1">Set Interval ( number of hours )</option>
                                            </select>
                                        </div>                                                    				
                                    </div>
	                            </div>
	                            <div class="row no-margin">
                                	<div class="form-group ">
                                    	<label class="control-label col-md-3">Arguments</label>
                                    	<div class="col-md-9">
                                        	<input type="text" class="form-control" placeholder="Script Arguments" name="scriptArgs"/>
                                    	</div>                                                    				
                           			</div>
                           		</div>
                           		<div class="row no-margin">
                                    <div class="form-group ">
                                    	<label class="control-label col-md-3">Script Max Runtime</label>
                                    	<div class="col-md-9">
                                        	<input type="text" class="form-control" placeholder="Script MAX Runtime" name="scriptMaxRuntime"/>
                                        </div>                                                    				
                                    </div>
	                             </div>
	                             <div class="row no-margin">
                                 	<div class="form-group ">
                                    	<label class="control-label col-md-3">Script Organizations</label>
                                        <div class="col-md-9">
                                        	<select name="scriptOrgs" class="form-control select2">
                                            	<option value="1">All</option>
                                                <option value="2">Comma Seperated</option>
                                                <option value="3">No Applicable</option>
                                            </select>
                                        </div>                     	                               				
                                    </div>
                                </div>
                                <div class="row no-margin">
                                 	<div class="form-group ">
                                    	<label class="control-label col-md-3">Script Return Values</label>
                                        <div class="col-md-9">
                                        	<input type="text" class="form-control" placeholder="Script Return Values" name="scriptReturnVals"/>
										</div>                                                    				
                                    </div>
	                            </div>
	                            <div class="row no-margin">
                                	<div class="form-group ">
                                    	<label class="control-label col-md-3">Script Audit</label>
                                        <div class="col-md-9">
                                        	<input type="checkbox" class="make-switch" placeholder="Script Audit" name="scriptAudit"/>
                                        </div>                                                    				
                                    </div>
	                            </div>
							</div>
						</div>
						<div class="modal-footer">
							<span ng-click="addNewScript()" class="btn btn-primary" id="submitButton" >Save</span>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>