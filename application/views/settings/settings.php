<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">Settings</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span>Settings</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<span>Site Config</span>
					</li>					
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<div class="row">
			    <div class="col-sm-12">
                    <div class="portlet box blue">
    					<div class="portlet-title">
                            <div class="caption">
    							<i class="fa fa-edit"></i>Site Config
    						</div>
    					</div>
    					<div class="portlet-body">
    						<div class="table-scrollable">
    						<?php if (isset($siteconfig)) {?>
                            	<table class="table table-striped">
                            		<tbody>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 Support Email
											</td>
											<td style="width:50%">
												<a href="#" id="supportEmail" data-type="text" data-original-title="Enter Support Email" data-pk="1" >
												<?php echo $siteconfig->supportEmail; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">Support Email Address</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 API Key
											</td>
											<td style="width:50%">
												<a href="#" id="apiKey" data-type="text" data-original-title="Enter API Key" data-pk="1">
												<?php echo $siteconfig->apiKey; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">OpenDNS API Key</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 Phone
											</td>
											<td style="width:50%">
												<a href="#" id="phone" data-type="text" data-original-title="Enter Phone number" data-pk="1">
												<?php echo $siteconfig->phone; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">Phone number</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 HTTP Port
											</td>
											<td style="width:50%">
												<a href="#" id="httpPort" data-type="text" data-original-title="Enter HTTP Port" data-pk="1">
												<?php echo $siteconfig->httpPort; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">HTTP Port Number</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 MAX Domain List
											</td>
											<td style="width:50%">
												<a href="#" id="maxDomainList" data-type="text" data-original-title="Enter MAX Domain List" data-pk="1">
												<?php echo $siteconfig->maxDomainList; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">Max Domain List</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 MIN Password Length
											</td>
											<td style="width:50%">
												<a href="#" id="minPassword" data-type="text" data-original-title="Enter Min Password Length" data-pk="1">
												<?php echo $siteconfig->minPassword; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">Min Password Length</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 Max Password Length
											</td>
											<td style="width:50%">
												<a href="#" id="maxPassword" data-type="text" data-original-title="Enter Max Password Length" data-pk="1">
												<?php echo $siteconfig->maxPassword; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">Max Password Length</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 Failed Attempts
											</td>
											<td style="width:50%">
												<a href="#" id="failedAttempts" data-type="text" data-original-title="Failed Attempts" data-pk="1">
												<?php echo $siteconfig->failedAttempts; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">Failed Attempts</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 Lockout
											</td>
											<td style="width:50%">
												<a href="#" id="lockout" data-type="text" data-original-title="Lockout" data-pk="1">
												<?php echo $siteconfig->lockout; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">Locout</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 Remember Me
											</td>
											<td style="width:50%">
												<a href="#" id="rememberMe" data-type="text" data-original-title="Remember Me" data-pk="1">
												<?php echo $siteconfig->rememberMe; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">Remember Me</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 SMS URL
											</td>
											<td style="width:50%">
												<a href="#" id="smsUrl" data-type="text" data-original-title="SMS URL" data-pk="1">
												<?php echo $siteconfig->smsUrl; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">SMS URL</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 SMS Username
											</td>
											<td style="width:50%">
												<a href="#" id="smsUser" data-type="text" data-original-title="SMS User" data-pk="1">
												<?php echo $siteconfig->smsUser; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">SMS User</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 SMS Password
											</td>
											<td style="width:50%">
												<a href="#" id="smsPassword" data-type="text" data-original-title="SMS Password" data-pk="1">
												<?php echo $siteconfig->smsPassword; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">SMS Password</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 SMS From
											</td>
											<td style="width:50%">
												<a href="#" id="smsFrom" data-type="text" data-original-title="SMS From" data-pk="1">
												<?php echo $siteconfig->smsFrom; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">SMS From</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 SMS API ID
											</td>
											<td style="width:50%">
												<a href="#" id="smsApiId" data-type="text" data-original-title="SMS API ID" data-pk="1">
												<?php echo $siteconfig->smsApiId; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">SMS API ID</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 SMS Certificate
											</td>
											<td style="width:50%">
												<a href="#" id="smsCertificatie" data-type="select2" data-value="<?php echo $siteconfig->smsCertificatie; ?>" data-original-title="SMS Certificate" data-pk="1">
												</a>
											</td>
											<td style="width:35%">
												<span class="text-muted">SMS Certificate</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 Retention Period
											</td>
											<td style="width:50%">
												<a href="#" id="retentionPeriod" data-type="text" data-original-title="Retention Period" data-pk="1">
												<?php echo $siteconfig->retentionPeriod; ?> </a>
											</td>
											<td style="width:35%">
												<span class="text-muted">Retention Period</span>
											</td>
                            			</tr>
                            			<tr>
                            				<td style="width:15%;text-align: right;">
												 SMS Certificate File
											</td>
											<td style="width:50%">
												<form action="">
													<div class="row no-margin">
														<div class="col-md-3">
															<input type="file" name="smsCertFile">
														</div>
														<div class="col-md-9">
															<input type="submit" class="btn-xs blue" value="Upload">
														</div>
														
													</div>												
												</form>
											</td>
											<td style="width:35%">
												<span class="text-muted">SMS Certifiation File</span>
											</td>
                            			</tr>
                            		</tbody>
                            	</table>	
                            <?php } ?>	
    						</div>
    				    </div>
    				</div>				    
			    </div>
                				
			</div>
		</div>
	</div>
