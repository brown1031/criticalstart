<!-- BEGIN CONTENT -->
<div ng-app="CSMainApp">
	<div ng-controller="SourceTypeController" ng-init="init('<?php echo base_url(); ?>');">
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">Settings</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<span>Settings</span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span>Source Types</span>
						</li>					
					</ul>
				</div>
				<!-- END PAGE HEADER-->
				<div class="clearfix">
				</div>
				<div class="row">
				    <div class="col-sm-12">
	                    <div class="portlet box blue">
	    					<div class="portlet-title">
	                            <div class="caption">
	    							<i class="fa fa-edit"></i>Source Types
	    						</div>
	                            <div class="actions" data-toggle="modal" data-target="#idDialog">
	    							<a href="#" class="btn btn-default btn-sm">
	    							<i class="fa fa-plus"></i> Add New </a>
	<!--     							<a href="#" class="btn btn-default btn-sm"> -->
	<!--     							<i class="fa fa-print"></i> Invite An Existing Account </a> -->
	    						</div>
	    					</div>
	    					<div class="portlet-body">
	    						<div class="table-scrollable">
	    							<table class="table table-bordered table-hover">
	    								<thead>
	    									<tr>
		    									<th>#</th>
		    									<th>ID</th>
		    									<th>Name</th>
		    									<th>Description</th>
		    									<th>Created At</th>
		    									<th>Modified At</th>
		    									<th>Modified By</th>
		    									<th>Action</th>
		    								</tr>
	    								</thead>
	    								<tbody ng-repeat="item in sourceTypes">
	    									<tr >
	    										<td class="col-md-1">{{$index + 1}}</td>
	    										<td class="col-md-1">{{item.id}}</td>
	    										<td class="col-md-2"><a href="javascript:;" ng-click="item.bedit=!item.bedit"><span>{{item.typeName}}</span></a></td>
	    										<td class="col-md-6">{{item.typeDescription}}</td>
	    										<td class="col-md-1">{{item.createdAt}}</td>
	    										<td class="col-md-1">{{item.modifiedAt}}</td>
	    										<td class="col-md-1">{{item.modifiedBy}}</td>
	    										<td class="text-center col-md-1">
	    											<span class="btn red btn-sm" ng-click="onDeleteSourceType($index);"><i class="fa fa-trash-o"></i> Delete</span>
	    										</td>
	    									</tr>
	    									<tr ng-show="item.bedit">
	    										<td colspan="8">
													<form class="form-horizontal" action="<?php echo base_url() . 'settings/updateSourceType'; ?>" method="POST" id="editSourceTypeForm_{{$index}}">
														<input type="hidden" ng-value="item.id" name="sourceTypeId">
	                                                    <div class="row no-margin">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">Type Name</label>
                                                    			<div class="col-md-9">
                                                    				<input type="text" class="form-control" ng-model="item.typeName" placeholder="Type Name" name="typeName"/>
                                                    			</div>                                                    				
                                                    		</div>
	                                                    </div>
	                                                    <div class="row no-margin">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">Description</label>
                                                    			<div class="col-md-9">
                                                    				<textarea class="form-control" placeholder="Your description here..." rows="7" ng-model="item.typeDescription" name="typeDescription"></textarea>
                                                    			</div>                                                    				
                                                    		</div>
	                                                    </div>
	                                                    <div class="row no-margin">
	                                                        <hr/>
	                                                    </div>
	                                                    <div class="row no-margin">
	                                                        <span class="btn btn-danger" ng-click="item.bedit=false">Cancel</span>
	                                                        <span class="btn btn-primary pull-right" ng-click="onUpdateSourceType( $index )">Update</span>
	                                                    </div>       
	                                                    <div class="row no-margin">
	                                                        <hr/>
	                                                    </div> 
	                                                </form>
	                                            </td>
	    									</tr>
	    								</tbody>
	    							</table>
	    						</div>
	    				    </div>
	    				</div>				    
				    </div>
	                				
				</div>
			</div>
		</div>
		<!-- Dialog - Add New Source Type -->
		<div id = "idDialog" class="modal fade">
			<form action="<?php echo base_url() . 'settings/addNewSourceType';?>" class="horizontal-form" id='formNewSourceType' method="post">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3>New Source Type</h3>
						</div>
						<div class="modal-body">
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-hide">
								<button class="close" data-close="alert"></button>
								Added Successfully!
							</div>
							<div class="form-body">
								<div class="row">
									<div class="form-horizontal">
										<div class="form-group">
											<label class="col-md-3 control-label">Type Name
												<span class="required" aria-required="true"> * </span>
											</label>
											<div class="col-md-8">
												<input type="text" class="form-control" placeholder="Type Name" name="typeName" id="typeName" required>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Description</label>
											<div class="col-md-8" >
												<textarea class="form-control" placeholder="Your descripton here..." rows="7" style="margin: 0px; height: 155px;" name="typeDescription"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<span ng-click="addNewSourceType()" class="btn btn-primary" id="submitButton" >Save</span>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>