<div ng-app="CSMainApp" ng-controller="AlertsController" ng-init="init('<?php echo base_url();?>')">
<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">Alerts</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span> Alerts</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<div class="row">
			    <div class="col-sm-12">
                    <div class="portlet box blue">
    					<div class="portlet-title">
                            <div class="caption">
    							<i class="fa fa-edit"></i> Alerts
    						</div>
                            <div class="actions">
								<a href="#" class="btn btn-default btn-sm"  data-toggle="modal" data-target="#idAddDialog">
    							<i class="fa fa-plus"></i> Add New</a>
    							<a href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#idUploadDialog">
    							<i class="fa fa-upload"></i> Upload</a>
    						</div>
    					</div>
    					<div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Info</th>
                                        <th class="text-center">Data</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">iType</th>
                                        <th class="text-center">Source Type</th>
                                        <th class="text-center">Alert Data Type</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody ng-repeat="item in alerts" on-finish-render="ngRepeatFinished">
                                    <tr>
                                        <td>{{$index + 1}}</td>
                                        <td><a href="javascript:;" ng-click="item.bedit=!item.bedit" ><span>{{item.info}}</span></a></td>
                                        <td>{{item.sourceName}}</td>
                                        <td>{{item.comEmail}}</td>
                                        <td>{{item.itypeType.itypeType}}</td>
                                        <td>{{item.sourceType.typeName}}</td>
                                        <td>{{item.alertType.type}}</td>
                                        <td class="text-center">
                                            <?php
                                            if ( $this->session->userdata('userRole') == USER_SUPERADMIN || $this->session->userdata('userRole') == USER_DEFAULTUSER) {
                                                ?>
                                                <button class="btn btn-danger" ng-click="onRemoveAlert($index)">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr ng-show="item.bedit">
                                        <td colspan="11">
                                            <form class="form-horizontal" action="<?php echo base_url() . 'alerts/update'; ?>" method="POST" id="editAlertsForm_{{$index}}">
                                                <input type="hidden" ng-value="item.id" name="id">
                                                <div class="row no-margin form-body">
                                                    <div class="alert alert-danger display-hide">
                                                        <button class="close" data-close="alert"></button>
                                                        You have some form errors. Please check below.
                                                    </div>
                                                    <div class="alert alert-success display-hide">
                                                        <button class="close" data-close="alert"></button>
                                                        Your form validation is successful!
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Info</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" ng-model="item.info" placeholder="Alert Info" name="info" <?php if ( $this->session->userdata('userRole') == USER_ROADMIN ||  $this->session->userdata('userRole') == USER_ROUSER) { echo 'disabled';} ?>/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Alert Data Type</label>
                                                            <div class="col-md-9">
                                                                <select class="form-control" ng-model="item.alertType" ng-options="option.type for option in alertDataTypes"></select>
                                                            </div>
                                                            <input type="hidden" name="cs_alerttype_id" ng-model="item.alertType" ng-value="item.alertType.id" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row no-margin">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">SourceType</label>
                                                            <div class="col-md-9">
                                                                <select class="form-control" ng-model="item.sourceType" ng-options="option.typeName for option in sourceTypes"></select>
                                                            </div>
                                                            <input type="hidden" name="cs_sourcetype_id" ng-model="item.sourceType" ng-value="item.sourceType.id">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">iType</label>
                                                            <div class="col-md-9">
                                                                <select class="form-control" ng-model="item.itypeType" ng-options="option.itypeType for option in iTypes"></select>
                                                            </div>
                                                            <input type="hidden" name="cs_itype_id" ng-model="item.itypeType" ng-value="item.itypeType.id">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row no-margin">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Source Name</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" ng-model="item.sourceName" name="sourceName">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Active</label>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" ng-model="item.active" bootstrap-switch>
                                                            </div>
                                                            <input type="hidden" ng-model="item.active" ng-value="isActive(item.active)" name="active">
                                                            <label class="control-label col-md-3">Shared</label>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" bootstrap-switch ng-model="item.shared" >
                                                            </div>
                                                            <input type="hidden" name="shared" ng-model="item.shared" ng-value="isShared(item.shared)">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row no-margin">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Notes</label>
                                                            <div class="col-md-9">
                                                                <textarea class="form-control" ng-model="item.notes" name="notes">
                                                                    {{item.notes}}
                                                                </textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row no-margin">
                                                    <span class="btn btn-danger" ng-click="item.bedit=false">Cancel</span>
                                                    <span class="btn btn-primary pull-right" ng-click="onUpdateAlert( $index )">Update</span>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
    				    </div>
    				</div>				    
			    </div>
			</div>
		</div>
	</div>
	<div id="idUploadDialog" class="modal fade">
		<form action="<?php echo base_url(); ?>alerts/upload" class="horizontal-form" id="formUpload" method="POST" enctype="multipart/form-data">
			<div class="modal-dialog">				
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3> Upload New Alert</h3>
					</div>
					<div class="modal-body">
						<div class="form-body">
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-hide">
								<button class="close" data-close="alert"></button>
								Added successfully!
							</div>
							<div class="row">
								<div class="form-horizontal">
									<div class="form-group">
										<label class="col-md-3 control-label">Select File</label>
										<div class="col-md-8">
											<input type="file" name="fileCSV">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<span class="btn btn-primary" id="submitButton" ng-click='onUploadAlert()'> Upload</span>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div id = "idAddDialog" class="modal fade">
		<form action="<?php echo base_url() . 'alerts/add';?>" class="horizontal-form" id='formAddAlert' method="post">
			<div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3>New Alert</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-body">
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-hide">
								<button class="close" data-close="alert"></button>
								Your form validation is successful!
							</div>
                            <div class="row">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Alert Info</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="Alert Info" name="info">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Alert Data Type</label>
                                        <div class="col-md-8">
											<select class="form-control" ng-model="alertInfo.alerttype" ng-options="option.type for option in alertDataTypes"></select>
                                        </div>
										<input type="hidden" name="cs_alerttype_id" ng-model="alertInfo.alerttype" ng-value="alertInfo.alerttype.id" >									
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Source Type</label>
										<div class="col-md-8">
											<select class="form-control" ng-model="alertInfo.sourcetype" ng-options="option.typeName for option in sourceTypes"></select>
										</div>
										<input type="hidden" name="cs_sourcetype_id" ng-model="alertInfo.sourcetype" ng-value="alertInfo.sourcetype.id">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">iType</label>
										<div class="col-md-8">
											<select class="form-control" ng-model="alertInfo.itype" ng-options="option.itypeType for option in iTypes"></select>
										</div>
										<input type="hidden" name="cs_itype_id" ng-model="alertInfo.itype" ng-value="alertInfo.itype.id">
                                    </div>
									<div class="form-group">
										<label class="control-label col-md-3">Source Name</label>
										<div class="col-md-8">
											<input type="text" ng-model="alertInfo.sourceName" name="sourceName" class="form-control">
										</div>
									</div>
                                    <div class="form-group">
                                    	<label class="col-md-3 control-label">Notes</label>
                                    	<div class="col-md-8">
                                    		<textarea class="form-control" name="notes"></textarea>
                                    	</div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Shared</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" class="make-switch" bootstrap-switch ng-model='alertInfo.shared'>
                                        </div>
										<input type="hidden" name="shared" ng-model="alertInfo.shared" ng-value="isShared(alertInfo.shared)">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Active</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" class="make-switch" ng-model='alertInfo.active' bootstrap-switch>
                                        </div>
										<input type="hidden" name="active" ng-model="alertInfo.active" ng-value="isActive(alertInfo.active)">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span class="btn btn-primary" id="submitButton" ng-click='onAddNewAlert()'> Save</span>
                    </div>
                </div>
            </div>
		</form>
	</div>
</div>