<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">Alert Exceptions</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span>Alert Exceptions</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<div class="row">
			    <div class="col-sm-12">
                    <div class="portlet box blue">
    					<div class="portlet-title">
                            <div class="caption">
    							<i class="fa fa-edit"></i>Alert Exceptions
    						</div>
                            <div class="actions" onclick='onAddNewCompany()'>
    							<a href="#" class="btn btn-default btn-sm">
    							<i class="fa fa-save"></i> Update </a>
<!--     							<a href="#" class="btn btn-default btn-sm"> -->
<!--     							<i class="fa fa-print"></i> Invite An Existing Account </a> -->
    						</div>
    					</div>
    					<div class="portlet-body">
    						<div class="table-scrollable">
                                					
    						</div>
    				    </div>
    				</div>				    
			    </div>
                				
			</div>
		</div>
	</div>
