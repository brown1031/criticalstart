<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">Audits</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span>Audits</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<div class="row">
			    <div class="col-sm-12">
                    <div class="portlet box blue">
    					<div class="portlet-title">
                            <div class="caption">
    							<i class="fa fa-edit"></i>Audits
    						</div>
    					</div>
    					<div class="portlet-body">
    						<div class="">
                            	<table id="tblAudits" class="table table-striped table-bordered table-hover">
    								<thead>
    									<tr>
	    									<th>#</th>
	                                        <th class="text-center">Audit Details</th>
	                                        <th class="text-center">Audit Action</th>
	                                        <th class="text-center">Audit Date</th>
	                                        <th class="text-center">Org. ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
                                    	if (isset( $audits )) { 
										$i = 0;
                                    	foreach( $audits as $k => $v ) {$i ++;?>
                                    	<tr>
                                    		<td><?php echo $i; ?></td>
                                    		<td><?php echo $v->auditDetails; ?></td>
                                    		<td><?php echo $v->auditAction; ?></td>
                                    		<td><?php echo $v->auditDate; ?></td>
                                    		<td><?php echo $v->orgComId; ?></td>
                                    	</tr>
                                    	<?php } } else {
                                    	?>
                                    	<tr>
                                    		<td colspan="5">
                                    			<span>No Active Records</span>
                                    		</td>
                                    	</tr>
                                    	<?php }?>
                                    </tbody>
                                </table>			
    						</div>
    				    </div>
    				</div>				    
			    </div>
                				
			</div>
		</div>
	</div>
