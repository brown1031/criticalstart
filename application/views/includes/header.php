<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo SITE_TITLE;?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->

    <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

    <!-- BEGIN PLUGINS USED BY X-EDITABLE -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/bootstrap-editable/inputs-ext/address/address.css"/>
    <!-- END PLUGINS USED BY X-EDITABLE -->

    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo base_url();?>assets/global/css/components.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/admin/layout2/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/admin/layout2/css/themes/grey.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo base_url();?>assets/admin/layout2/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/favicon.png"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<?php if ($page != 'login') {?>
<body class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">
<div id="idSupportTicket" class="modal fade">
	<form action="<?php echo base_url() . 'settings/ticket';?>" class="horizontal-form" id='formSupportTicket' method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3>Contact Support for Critical Start MSP</h3>
				</div>
				<div class="modal-body">
					<div class="form-body">
						<div class="row">
							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-md-3 control-label">Subject
									</label>
									<div class="col-md-8">
										<input type="text" class="form-control" placeholder="Briefly describe your question or support issue" name="ticketSubject" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Details</label>
									<div class="col-md-8" >
										<textarea class="form-control" placeholder="Fill in details here. Please try to be as specific as possible" rows="7" style="margin: 0px; height: 155px;" name="ticketDetails"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<span class="btn btn-danger" data-dismiss="modal" >Cancel</span>
					<span onclick="sendTicket()" class="btn green" id="submitTicket" >Save</span>
				</div>
			</div>
		</div>
	</form>
</div>
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="#">
			<img src="<?php echo base_url();?>assets/global/img/white-transparent.png" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->

		<!-- BEGIN PAGE TOP -->
		<div class="page-top">
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<li  class="dropdown dropdown-user">
                        <a href="#" class="dropdown-toggle" data-toggle="modal" data-target="#idSupportTicket"><span>Support </span></a>
					</li>
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
					<li class="dropdown dropdown-user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						    <span class="username username-hide-on-mobile">
                                <?php
                                    echo $this->session->userdata('userFirstName'). ' ' . $this->session->userdata('userLastName');
                                    if ( $this->session->userdata('userRole') == USER_SUPERADMIN ) {
                                        echo ' (SuperAdmin)';
                                    } else if ( $this->session->userdata('userRole') == USER_ROADMIN) {
                                        echo ' (Read-Only Admin)';
                                    } else if ( $this->session->userdata('userRole') == USER_ROUSER) {
                                        echo ' (Read-Only User)';
                                    } else {
                                        echo ' (User)';
                                    }
                                ?>
                            </span>
						    <i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="<?php echo base_url().'users/myprofile'; ?>">
								<i class="icon-user"></i> My Profile </a>
							</li>
							<li>
								<a href="<?php echo base_url().'users/logout';?>">
								<i class="icon-key"></i> Log Out </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		<!-- END PAGE TOP -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<div class="page-sidebar navbar-collapse collapse">
				<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                    <?php
                    if ( $this->session->userdata('userRole') == USER_SUPERADMIN || $this->session->userdata('userRole') == USER_ROADMIN) {
                        ?>
                        <li class="<?php if ($page == 'company') { ?>active open<?php } ?> ">
                            <a href="<?php echo base_url() . 'companies'; ?>">
                                <i class="fa fa-building"></i>
                                <span class="title"> Company</span>
                                <?php if ($page == 'company') { ?><span class="selected"></span><?php } ?>
                            </a>
                        </li>
                    <?php
                    } else {
                    ?>
                    <li class="<?php if ($page == 'domains') { ?>active open<?php }?> ">
                        <a href="<?php echo base_url() . 'domains';?>">
                            <i class="fa fa-building"></i>
                            <span class="title"> Domain Lists</span>
                            <?php if ($page == 'domains') { ?><span class="selected"></span><?php }?>
                        </a>
                    </li>
                    <?php
                    }
                    ?>
					<li class="<?php if ($page == 'alerts') { ?>active open<?php }?> ">
						<a href="<?php echo base_url() . 'alerts';?>">
						<i class="fa fa-bullhorn"></i>
						<span class="title"> Alerts</span>
						<?php if ($page == 'alerts') { ?><span class="selected"></span><?php }?>
						</a>
					</li>
					<li class="<?php if ($page == 'audits') { ?>active open<?php }?> ">
						<a href="<?php echo base_url() . 'audit';?>">
						<i class="fa fa-history"></i>
						<span class="title"> Audits</span>
						<?php if ($page == 'audits') { ?><span class="selected"></span><?php }?>
						</a>
					</li>
					<li class="<?php if (preg_match('/users/', $page)) { ?>active open<?php }?> ">
						<a href="<?php echo base_url() . 'users';?>">
						<i class="fa fa-users"></i>
						<span class="title"> Users</span>
						<?php if (preg_match('/users/', $page)) { ?><span class="selected"></span><?php }?>
						</a>
					</li>
                    <?php
                    if ( (int)$this->session->userdata('userRole') == USER_SUPERADMIN) {
                        ?>
                        <li class="<?php if (preg_match('/settings/', $page)) { ?>active open<?php } ?> ">
                            <a href="javascript:;">
                                <i class="icon-settings"></i>
                                <span class="title">Settings</span>
                                <?php if (preg_match('/settings/', $page)) { ?><span class="selected"></span><?php } ?>
                                <span class="arrow <?php if (preg_match('/settings/', $page)) {
                                    echo 'open';
                                } ?>"></span>
                            </a>
                            <ul class="sub-menu">
                                <li <?php if (preg_match('/userroles/', $page)) {
                                    echo 'class="active"';
                                } ?>><a href="<?php echo base_url() . 'settings/userroles'; ?>"><i
                                            class="fa fa-cogs"></i> Admin Role</a></li>
                                <li <?php if (preg_match('/domainwhitelist/', $page)) {
                                    echo 'class="active"';
                                } ?>><a href="<?php echo base_url() . 'settings/domainwhitelist'; ?>"><i
                                            class="fa fa-cogs"></i> Domain Whitelist</a></li>
                                <li <?php if (preg_match('/sourcetypes/', $page)) {
                                    echo 'class="active"';
                                } ?>><a href="<?php echo base_url() . 'settings/sourcetypes'; ?>"><i
                                            class="fa fa-cogs"></i> Source Types</a></li>
                                <li <?php if (preg_match('/alertdatatypes/', $page)) {
                                    echo 'class="active"';
                                } ?>><a href="<?php echo base_url() . 'settings/alertdatatypes'; ?>"><i
                                            class="fa fa-cogs"></i> Alert Data Types</a></li>
                                <li <?php if (preg_match('/itypes/', $page)) {
                                    echo 'class="active"';
                                } ?>><a href="<?php echo base_url() . 'settings/itypes'; ?>"><i class="fa fa-cogs"></i>
                                        iType</a></li>
                                <li <?php if (preg_match('/siteconfig/', $page)) {
                                    echo 'class="active"';
                                } ?>><a href="<?php echo base_url() . 'settings/siteconfig'; ?>"><i
                                            class="fa fa-cogs"></i> Site Config</a></li>
                                <li <?php if (preg_match('/scripts/', $page)) {
                                    echo 'class="active"';
                                } ?>><a href="<?php echo base_url() . 'settings/scripts'; ?>"><i class="fa fa-cogs"></i>
                                        Scripts</a></li>
                            </ul>
                        </li>
                    <?php
                    }
                    ?>
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
		</div>
		<!-- END SIDEBAR -->
<?php } else { ?>
<body class="login">
<?php } ?>