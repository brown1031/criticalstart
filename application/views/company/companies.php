<style>
.row-no-margin {
    margin-left: 0px !important;
    margin-right: 0px !important;
    margin-top: 4px;
}
#js-tr-name {
    cursor: pointer;
}
.color-blue {
    color: #009CFF;
}
</style>
    <!-- BEGIN CONTENT -->
	<div class="page-content-wrapper" ng-app="CSMainApp">
		<div class="page-content" ng-controller="CompanyController" ng-init="init('<?php echo base_url();?>', '<?php echo $this->session->userdata('userRole'); ?>')">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">Companies</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#">
						<i class="fa fa-home"></i>
						<span>Companies</span>
						</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<div class="row">
			    <div class="col-sm-12">
                    <div class="portlet box blue">
    					<div class="portlet-title">
                            <div class="caption">
    							<i class="fa fa-edit"></i>Company List
    						</div>

                            <div class="actions">
                                <a href="javascript:;" ng-click="refreshCompanies()" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                                <?php if ( $this->session->userdata('userRole') == USER_SUPERADMIN) { ?>
    							<a href="#" class="btn btn-default btn-sm" ng-click='onAddNewCompany()'>
    							<i class="fa fa-plus"></i> Add A New Company </a>
                                <?php } ?>
    						</div>

    					</div>
    					<div class="portlet-body">
    						<div class="table-scrollable">
    							<table class="table table-striped table-bordered table-hover">
    								<thead>
    									<tr>
    									<th>#</th>
                                            <th class="text-center">Organization ID</th>
                                            <th class="text-center">Organization Name</th>
                                            <th class="text-center">Network Count</th>
                                            <th class="text-center">Created at</th>
                                            <th class="text-center">Modified at</th>
                                            <th class="text-center"></th>
                                        </tr>
                                    </thead>
                                    <tbody ng-repeat="item in companies" ng-model="companies" on-finish-render="ngRepeatFinished">
                                    	<tr>
                                            <td>{{$index + 1}}</td>
                                            <td ng-click="item.bedit=!item.bedit" class="color-blue" style="cursor: pointer" >{{item.orgId}}</td>
                                            <td class="text-center">{{item.orgName}}</td>                                                                                        
                                            <td class="text-center">{{item.networkCount}}</td>                                            
                                            <td class="text-center">{{item.odCreatedAt}}</td>                                            
                                            <td class="text-center">{{item.odModifiedAt}}</td>                                            
                                            <td class="text-center"><button class="btn btn-danger" ng-show="userRole=='1'" ng-click="onDeleteCompany($index)"><i class="fa fa-trash-o"></i></button></td>
                                        </tr>
                                        <tr ng-show="item.bedit">
                                            <td colspan="7">
                                                <form class="form-horizontal" action="<?php echo base_url() . 'companies/update'; ?>" method="POST" id="editForm_{{$index}}">
                                                	<input type="hidden" ng-value="item.id" name="companyId">
                                                    <div class="row row-no-margin">
                                                    	<div class="col-md-6">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">Orgin ID</label>
                                                    			<div class="col-md-9">
                                                    				<input type="text" class="form-control" ng-model="item.originId" placeholder="Organization ID" disabled />
                                                    			</div>                                                    				
                                                    		</div>
                                                    	</div>                                                      	                                                	
                                                        <div class="col-md-6">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">Creator User ID</label>
                                                    			<div class="col-md-9">
                                                    				<input type="text" class="form-control" ng-model="item.creatorId" placeholder="Creator User ID" disabled />
                                                    			</div>                                                    				
                                                    		</div>
                                                    	</div>
                                                    </div>
                                                    <div class="row row-no-margin">
                                                    	<div class="col-md-6">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">MSP Org ID</label>
                                                    			<div class="col-md-9">
                                                    				<input type="text" class="form-control" ng-model="item.mspOrgId" disabled/>
                                                    			</div>                                                    				
                                                    		</div>
                                                    	</div>
                                                    	<div class="col-md-6">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">FireEye</label>
                                                    			<div class="col-md-9">
                                                    				<input type="checkbox" class="make-switch" name="chkFireEye" ng-model="item.enabledFireEye" ng-disabled="userRole!='1'"/>
                                                    			</div>
                                                    		</div>
                                                    	</div>
                                                    </div>
                                                    <div class="row row-no-margin">
                                                    	<div class="col-md-6" >
                                                    		<div class="form-group" ng-show="item.customApiKey">
                                                    			<label class="control-label col-md-3">API Key</label>
                                                    			<div class="col-md-9">
                                                    				<div class="input-group">
                                                    					<input type="text" class="form-control" ng-model="item.apiKey" placeholder="API Key"  ng-show="item.bshowapikey" name="txtApiKey" ng-disabled="userRole!='1'">
                                                    					<input type="text" class="form-control" value="********" ng-show="!item.bshowapikey" ng-disabled="userRole!='1'">
                                                    					<div class="input-group-btn">
                                                    						<button id="showApiKey" class="btn btn-default" type="button" title="Show API Key" ng-click="item.bshowapikey=!item.bshowapikey"><i class="fa fa-eye"></i></button>
                                                    					</div>                                                    					
                                                    				</div>
                                                    			</div>                                                    				
                                                    		</div>
                                                    	</div>
                                                    	<div class="col-md-6">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">Token</label>
                                                    			<div class="col-md-9">
                                                    				<div class="input-group">
                                                    					<input type="text" class="form-control" name="txtOdToken" ng-model="item.odToken" ng-show="item.bshowtoken" ng-disabled="userRole!='1'">
                                                    					<input type="text" class="form-control" value="********" ng-show="!item.bshowtoken" ng-disabled="userRole!='1'">
                                                    					<div class="input-group-btn">
                                                    						<button id="showToken" class="btn btn-default" type="button" title="Show OD Token" ng-click="item.bshowtoken=!item.bshowtoken"><i class="fa fa-eye"></i></button>
                                                    					</div>
                                                    				</div>
                                                    				
                                                    			</div>
                                                    		</div>
                                                    	</div>
                                                    </div>
                                                    <div class="row row-no-margin">
                                                    	<div class="col-md-6">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">Whitelist Enabled</label>
                                                    			<div class="col-md-9">
                                                    				<input type="checkbox" class="make-switch" name="chkEnabledWhiteList" ng-model="item.enabledWhiteList"  ng-disabled="userRole!='1'"/>
                                                    			</div>
                                                    		</div>
                                                    	</div>
                                                    </div>
                                                    <div class="row row-no-margin">
                                                    	<div class="col-md-6">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">Whitelist Values</label>
                                                    			<div class="col-md-9">
                                                    				<input type="text" class="form-control" ng-model="item.whiteListVal" name="txtWhiteListVal" ng-disabled="userRole!='1'">
                                                    			</div>                                                    				
                                                    		</div>
                                                    	</div>
                                                    	<div class="col-md-6">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">Share Treat Intel</label>
                                                    			<div class="col-md-9">
                                                    				<input type="checkbox" class="make-switch" name="chkShareTreatIntel" ng-model="item.sharedThreatIntel"  ng-disabled="userRole!='1'"/>
                                                    			</div>
                                                    		</div>
                                                    	</div>
                                                    </div>
                                                    <div class="row row-no-margin">
                                                    	<div class="col-md-6">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">Days Alert Active</label>
                                                    			<div class="col-md-9">
                                                    				<input type="text" class="form-control" ng-model="item.daysAlertActive" name="txtDaysAlertActive"  ng-disabled="userRole!='1'" />
                                                    			</div>                                                    				
                                                    		</div>
                                                    	</div>
                                                    	<div class="col-md-6">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">Require 2FA</label>
                                                    			<div class="col-md-9">
                                                    				<input type="checkbox" class="make-switch" name="chkRequire2FA" ng-model="item.require2FA" ng-disabled="userRole!='1'" >
                                                    			</div>
                                                    		</div>
                                                    	</div>
                                                    </div>
                                                    <div class="row row-no-margin">
                                                    	<div class="col-md-6">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">Custom API Key</label>
                                                    			<div class="col-md-9">
                                                    				<input type="checkbox" class="make-switch" ng-model="item.customApiKey" name="chkCustomApiKey" ng-change="item.customApiKey=!item.customApiKey" ng-disabled="userRole!='1'"/>
                                                    			</div>                                                    				
                                                    		</div>
                                                    	</div>
                                                    	<div class="col-md-6">
                                                    		<div class="form-group">
                                                    			<label class="control-label col-md-3">Connect Wise ID</label>
                                                    			<div class="col-md-9">
                                                    				<input type="text" class="form-control" ng-model="item.connectWiseId" name="txtConnectWiseId" ng-disabled="userRole!='1'"/>
                                                    			</div>
                                                    		</div>
                                                    	</div>
                                                    </div>
                                                    <div class="row row-no-margin">
                                                        <hr/>
                                                    </div>
                                                    <div class="row row-no-margin">
                                                        <span class="btn btn-danger" ng-click="item.bedit=false">Cancel</span>
                                                        <span class="btn btn-primary pull-right" ng-click="onUpdateCompany( $index )" ng-show="userRole=='1'">Update</span>
                                                        <a href="<?php echo base_url().'domains/getList/';?>{{item.id}}"><span class="btn btn-success pull-right">View DomainLists</span></a>
                                                        <a href="<?php echo base_url().'users/getCompanyUserList/';?>{{item.id}}"><span class="btn btn-success pull-right">View Users</span></a>
                                                    </div>       
                                                    <div class="row row-no-margin">
                                                        <hr/>
                                                    </div> 
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
    							</table>
    						</div>
    				    </div>
    				</div>				    
			    </div>
                				
			</div>
		</div>
	</div>
</div>
<!-- END CONTAINER -->
<!-- Dialog - Add New Company -->
<div id = "idNewCompany" class="modal fade">
	<form action="<?php echo base_url() . 'companies/addNew';?>" class="horizontal-form" id='formNewCompany' method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3>Company Info</h3>
				</div>
				<div class="modal-body">
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						You have some form errors. Please check below.
					</div>
					<div class="alert alert-success display-hide">
						<button class="close" data-close="alert"></button>
						Added Successfully!
					</div>
					<div class="form-body">
						<div class="row">
							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-md-3 control-label">Organization ID</label>
									<div class="col-md-8">
										<input type="text" class="form-control" placeholder="Organization ID" name="orgId" id="orgId">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">User Name</label>
									<div class="col-md-8">
										<input type="text" class="form-control" placeholder="User Name" name="userName" id="userName">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Password</label>
									<div class="col-md-8">
										<input type="text" class="form-control" placeholder="Password" name="password" id="password">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Save</button>
				</div>
			</div>
		</div>
	</form>
</div>

<div id="idWaiting" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Please wait for a moment ...</h2>
            </div>
        </div>
    </div>
</div>
