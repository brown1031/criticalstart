<div ng-app="CSMainApp">
	<div ng-controller="DomainsController" ng-init="init('<?php echo base_url(); ?>', '<?php echo $companyInfo->id;?>', '<?php echo $this->session->userdata("userRole");?>')">
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">Domain Lists</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url() . 'companies';?>"><span>Companies</span></a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#"><span><?php echo $companyInfo->orgName;?></span></a>
						</li>			
					</ul>
				</div>
				<!-- END PAGE HEADER-->
				<div class="clearfix">
				</div>
				<div class="row">
					<div class="col-md-12">
		                    <div class="portlet box blue">
		    					<div class="portlet-title">
		                            <div class="caption">
		    							<i class="fa fa-edit"></i>Domains List
		    						</div>
		    						<div class="actions" >
                                        <a href="javascript:;" class="btn btn-default btn-sm" ng-click="onRefreshDomainList()"><i class="fa fa-refresh"></i></a>
		    							<a href="#" class="btn btn-default btn-sm" ng-click='onAddNewDomainList()' ng-show="userRole=='1'">
		    							<i class="fa fa-plus"></i> Add A New DomainList </a>
		    						</div>
		    					</div>
		    					<div class="portlet-body">
		    						<div class="table-scrollable">
		                                <table class="table table-striped table-bordered table-hover">
		                                    <thead>
		                                        <tr>
		                                            <th class="text-center">#</th>
		                                            <th class="text-center">ID</th>
													<th class="text-center">List Name</th>
													<th class="text-center">Access</th>
													<th class="text-center">Action</th>
		                                        </tr>
		                                    </thead>
		                                    <tbody ng-repeat="item in domainsList" on-finish-render="ngRepeatFinished">
												<tr>
													<td>{{$index + 1}}</td>
													<td>{{item.odId}}</td>
													<td><a href="javascript:;" ng-click="item.bedit=!item.bedit" ><span>{{item.odName}}</span></a></td>
													<td>{{item.odAccess.val}}</td>
													<td class="text-center"><button ng-show="userRole=='1'" class="btn btn-danger" ng-disabled="item.odisGlobal=='Y'" ng-click="onRemoveDomainList($index)"><i class="fa fa-trash-o"></i></button></td>
												</tr> 
												<tr ng-show="item.bedit">
													<td colspan="5">
														<form class="form-horizontal" action="<?php echo base_url() . 'domains/update'; ?>" method="POST" id="editDomainForm_{{$index}}">
															<input type="hidden" ng-value="item.id" name="domainListId">
		                                                    <div class="row no-margin">
		                                                    	<div class="col-md-8">
		                                                    		<div class="form-group">
		                                                    			<label class="control-label col-md-3">DomainList Name</label>
		                                                    			<div class="col-md-9">
		                                                    				<input type="text" class="form-control" ng-model="item.odName" placeholder="DomainList Name" name="domainListName" ng-disabled="userRole!='1'"/>
		                                                    			</div>                                                    				
		                                                    		</div>
		                                                    	</div>                                                      	                                                	
		                                                        <div class="col-md-3">
		                                                    		<div class="form-group">
		                                                    			<label class="control-label col-md-3"></label>
		                                                    			<div class="col-md-9">
		                                                    				<select class="form-control" ng-model="item.odAccess" ng-options="option.label for option in accessOptions"  ng-disabled="userRole!='1'"></select>
		                                                    			</div>     
		                                                    			<input type="hidden" name="access" ng-model="item.odAccess" ng-value="item.odAccess.val" ng-disabled="userRole!='1'">
		                                                    		</div>
		                                                    	</div>
		                                                    </div>
		                                                    <div class="row no-margin">
		                                                    	<div class="col-md-4">
		                                                    		<div class="form-group">
		                                                    			<label class="control-label col-md-3">SourceType ID</label>
		                                                    			<div class="col-md-9">
		                                                    				<select class="form-control" name="sourceType" ng-model="item.sourceType" ng-options="option.typeName for option in sourceTypes" ng-disabled="userRole!='1'">
                                                                                <option value=""></option>
		                                                    				</select>
		                                                    			</div>   
		                                                    			<input type="hidden" name="sourceTypeId" ng-model="item.sourceType" ng-value="item.sourceType.id" ng-disabled="userRole!='1'">
		                                                    		</div>
		                                                    	</div>                                                      	                                                	
		                                                        <div class="col-md-4">
		                                                    		<div class="form-group">
		                                                    			<label class="control-label col-md-3">Domain Limit</label>
		                                                    			<div class="col-md-9">
		                                                    				<input type="text" class="form-control" ng-model="item.domainLimit" placeholder="Domain Limit" name="domainLimit" ng-disabled="userRole!='1'"/>
		                                                    			</div>                                                    				
		                                                    		</div>
		                                                    	</div>
		                                                    	<div class="col-md-4">
		                                                    		<div class="form-group">
		                                                    			<label class="control-label col-md-3">Domain List Sync.</label>
		                                                    			<div class="col-md-9">
		                                                    				<input type="checkbox" class="make-switch" name="domainListSync" ng-model="item.domainListSync" ng-disabled="userRole!='1'">
		                                                    			</div>                                                    				
		                                                    		</div>
		                                                    	</div>
		                                                    </div>
		                                                    <div class="row no-margin">
		                                                        <hr/>
		                                                    </div>
		                                                    <div class="row no-margin">
		                                                        <span class="btn btn-danger" ng-click="item.bedit=false">Cancel</span>
		                                                        <span class="btn btn-primary pull-right" ng-click="onUpdateDomainList( $index )" ng-show="userRole=='1'">Update</span>
		                                                        <a href="<?php echo base_url().'domains/subdomains/' . $companyInfo->id . "/";?>{{item.id}}"><span class="btn btn-success pull-right">View Domains</span></a>
		                                                    </div>       
		                                                    <div class="row no-margin">
		                                                        <hr/>
		                                                    </div> 
														</form>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Dialog - Add New Domain -->
		<div id = "idNewDomainlist" class="modal fade">
			<form action="<?php echo base_url() . 'domains/addNewDomainList/' . $companyInfo->id;?>" class="horizontal-form" id='formNewDomainlist' method="post">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3>DomainList Info</h3>
						</div>
						<div class="modal-body">
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-hide">
								<button class="close" data-close="alert"></button>
								Added Successfully!
							</div>
							<div class="form-body">
								<div class="row">
									<div class="form-horizontal">
										<div class="form-group">
											<label class="col-md-3 control-label">Domain List Name</label>
											<div class="col-md-8">
												<input type="text" class="form-control" placeholder="List Name" name="listName" id="listName">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Domains on this list will be</label>
											<div class="col-md-8" >
												<select class="form-control" name="access" id="access">
													<option value="allow">Allowed</option>
													<option value="block">Blocked</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<span ng-click="AddNewDomainList()" class="btn btn-primary" id="submitButton" >Save</span>
						</div>
					</div>
				</div>
			</form>
		</div>
        <div id="idWaiting" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2>Please wait for a moment ...</h2>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>