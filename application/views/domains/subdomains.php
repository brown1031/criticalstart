<div ng-app="CSMainApp">
<div ng-controller="DomainsController" ng-init="subdomain_init('<?php echo base_url(); ?>', '<?php echo $companyInfo->id;?>', '<?php echo $domainInfo->id;?>','<?php echo $this->session->userdata("userRole"); ?>')">
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">Domains</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="<?php echo base_url().'companies';?>"><span>Companies</span></a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="<?php echo base_url() . 'domains/getList/'.$companyInfo->id; ?>"><span><?php echo $companyInfo->orgName;?></span></a>
					<i class="fa fa-angle-right"></i>
				</li>			
				<li>
					<a href="#"><span><?php echo $domainInfo->odName;?></span></a>
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<div class="clearfix">
		</div>
		<div class="row">
			<div class="col-md-12">
                <div class="portlet box blue">
    				<div class="portlet-title">
                            <div class="caption">
    							<i class="fa fa-edit"></i>Domains
    						</div>
    						
    						<div class="actions" ng-click='onAddNewSubDomain()' ng-show="userRole=='1'">
    							<a href="#" class="btn btn-default btn-sm">
    							<i class="fa fa-plus"></i> Add A New Domain </a>
    						</div>
    					</div>
    					<div class="portlet-body">
    						<div class="table-scrollable">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">ID</th>
											<th class="text-center">Domain Name</th>
											<th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<tr ng-repeat="item in subDomains">
											<td>{{$index + 1}}</td>
											<td>{{item.odId}}</td>
											<td><a href="#"><span class="">{{item.name}}</span></a></td>
											<td class="text-center"><button ng-show="userRole=='1'" class="btn btn-danger" ng-click="onRemoveSubDomain($index)"><i class="fa fa-trash-o"></i></button></td>
										</tr> 
								</tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Dialog - Add New Company -->
<div id = "idNewSubDomain" class="modal fade">
	<form action="<?php echo base_url() . 'domains/addNewDomain/' . $companyInfo->id . '/' . $domainInfo->id;?>" class="horizontal-form" id='formNewSubDomain' method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3>Domain Info</h3>
				</div>
				<div class="modal-body">
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						You have some form errors. Please check below.
					</div>
					<div class="alert alert-success display-hide">
						<button class="close" data-close="alert"></button>
						Added Successfully!
					</div>
					<div class="form-body">
						<div class="row">
							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-md-3 control-label">Domain Name</label>
									<div class="col-md-8">
										<input type="text" class="form-control" placeholder="Domain Name" name="domainName" id="domainName">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<span ng-click="AddNewSubDomain()" class="btn btn-primary" id="submitButton" >Save</span>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>