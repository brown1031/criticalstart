<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">My Profile</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?php echo base_url() . 'users';?>"><span>Users </span></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#"><span>My Profile</span></a>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <div class="clearfix">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-edit"></i>Profile Account
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light">
                                        <div class="portlet-title tabbable-line">
                                            <div class="caption caption-md">
                                                <i class="icon-globe theme-font"></i>
                                                <span class="caption-subject font-blue-madison bold uppercase">opendns user<small class="uppercase"> active</small></span>
                                                <span class="caption-subject font-red-soft bold uppercase">not opendns user</span>
                                            </div>
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                                </li>
                                                <li>
                                                    <a href="#tab_1_2" data-toggle="tab">Change Password</a>
                                                </li>
                                                <li>
                                                    <a href="#tab_1_3" data-toggle="tab">Privacy Settings</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab_1_1">
                                                    <form action="#" role="form">
                                                        <div class="form-group">
                                                            <label class="control-label">First Name</label>
                                                            <input type="text" placeholder="First Name" class="form-control" name="firstName" value="<?php echo $this->session->userdata('userFirstName'); ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Last Name</label>
                                                            <input type="text" placeholder="Last Name" class="form-control" name="lastName" value="<?php echo $this->session->userdata('userLastName'); ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Email</label>
                                                            <input type="text" placeholder="Email" class="form-control" name="userEmail" value="<?php echo $this->session->userdata('userEmail'); ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">User Role</label>
                                                            <select class="form-control">
                                                                <option>Read-only Admin</option>
                                                                <option>User</option>
                                                                <option>Read-only User</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Primary Contact</label>
                                                            <input type="checkbox" class="form-control make-switch" name="primaryContact">
                                                        </div>
                                                        <div class="margin-top-10">
                                                            <a href="#" class="btn green-haze">Save Changes</a>
                                                            <a href="#" class="btn default">Cancel</a>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="tab-pane " id="tab_1_2">
                                                    <form action="#" role="form">
                                                        <div class="form-group">
                                                            <label class="control-label">New Password</label>
                                                            <input type="text" placeholder="New Password" class="form-control" name="password" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Confirm Password</label>
                                                            <input type="text" placeholder="Confirm Password" class="form-control" name="confpass" required>
                                                        </div>
                                                        <div class="margin-top-10">
                                                            <a href="#" class="btn green-haze">Change Password</a>
                                                            <a href="#" class="btn default">Cancel</a>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="tab-pane " id="tab_1_3">
                                                    <form action="#" role="form">
                                                        <div class="form-group">
                                                            <label class="control-label">Hash Key</label>
                                                            <input type="text" placeholder="Hash Key" class="form-control" name="hashKey">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">2 Factor Phone</label>
                                                            <input type="checkbox" class="form-control make-switch" name="twoFactorPhone" checked>
                                                        </div>
                                                        <div class="margin-top-10">
                                                            <a href="#" class="btn green-haze">Save Changes</a>
                                                            <a href="#" class="btn default">Cancel</a>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>