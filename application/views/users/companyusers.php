<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<h3 class="page-title">Company Users</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="<?php echo base_url() . 'companies';?>"><span>Companies</span></a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#"><span><?php echo $companyInfo->orgName;?></span></a>
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
		<div class="clearfix">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="col-sm-12">
                    <div class="portlet box blue">
    					<div class="portlet-title">
                            <div class="caption">
    							<i class="fa fa-edit"></i>Users
    						</div>
    					</div>
    					<div class="portlet-body">
    						<div class="table-scrollable">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
											<th>First Name</th>
											<th>Last Name</th>
											<th>Email</th>
											<th>Role</th>
											<th>Status</th>
											<th>OpenDNS User</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php $i = 0; foreach ( $users as $k => $v ) { $i ++;?>
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $v->odUserId;?></td>
											<td><?php echo $v->odUserFirstName;?></td>
											<td><?php echo $v->odUserLastName;?></td>
											<td><?php echo $v->odUserEmail;?></td>
											<td><?php echo $v->odUserRole;?></td>
											<td><?php echo $v->odUserStatus;?></td>
											<td><?php echo $v->isOdUser == 'Y' ? 'YES' : 'NO';?></td>
										</tr> 
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
			</div>
		</div>
	</div>
</div>