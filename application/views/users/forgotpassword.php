<div class="menu-toggler sidebar-toggler"></div>
<div class="logo">
	<a href="#">
	<!-- // <img src="<?php //echo base_url(); ?>assets/admin/layout2/img/logo-big.png" alt=""/> -->
	</a>
</div>

<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form class="login-form" action="<?php echo base_url() . 'users/forgotpwd';?>" method="post">
		<h3 class="form-title">Forgot Password</h3>
        <?php if( isset( $errors ) ) { echo $errors; } ?>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>Enter your email address. </span>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Email Address</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email Address" name="userEmail" value="<?php if(isset($userEmail)) {echo $userEmail;} ?>"/>
		</div>		
		<div class="form-actions">
			<button type="submit" class="btn btn-success uppercase">Submit</button>
		    <a href="<?php echo base_url()."users/login"?>" id="forget-password" class="forget-password">Login Page</a>			
		</div>
	</form>
</div>