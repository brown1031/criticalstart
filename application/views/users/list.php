<div ng-app="CSMainApp">
<div  ng-controller="UsersController" ng-init="init('<?php echo base_url();?>', '<?php echo $this->session->userdata("userRole");?>');">
<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">Users</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span>Users</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<div class="row">
			    <div class="col-sm-12">
                    <div class="portlet box blue">
    					<div class="portlet-title">
                            <div class="caption">
    							<i class="fa fa-edit"></i> Users
    						</div>
                            <?php
                                if ( $this->session->userdata('userRole') == USER_SUPERADMIN) {
                            ?>
                            <div class="actions" data-toggle="modal" data-target="#idDialog">
    							<a href="#" class="btn btn-default btn-sm">
    							<i class="fa fa-plus"></i> Add New </a>
    						</div>
                            <?php } ?>
    					</div>
    					<div class="portlet-body">
    						<div class="table-scrollable">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">First Name</th>
                                        <th class="text-center">Last Name</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Company</th>
                                        <th class="text-center">User Role</th>
                                        <th class="text-center">Date Created</th>
                                        <th class="text-center">Date Modified</th>
                                        <th class="text-center">Last Logged In</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody ng-repeat="item in userslist" on-finish-render="ngRepeatFinished">
                                        <tr>
                                            <td>{{ $index + 1 }}</td>
                                            <td>{{item.odUserFirstName}}</td>
                                            <td>{{item.odUserLastName}}</td>
                                            <td><a href="javascript:;" ng-click="item.bedit=!item.bedit">{{item.odUserEmail}}</a></td>
                                            <td>{{item.companyName}}</td>
                                            <td>{{item.roleName}}</td>
                                            <td>{{item.createdAt}}</td>
                                            <td>{{item.modifiedAt}}</td>
                                            <td>{{item.lastLogin}}</td>
                                            <td class="text-center">
                                                <button class="btn btn-danger" ng-click="onRemoveUser( $index )"  ng-show="userRole=='1' && item.isOdUser=='N'"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                        <tr ng-show="item.bedit && (userRole=='1' || userRole=='3')">
                                            <td colspan="10">
                                                <form class="form-horizontal" action="<?php echo base_url() . 'users/update'; ?>" method="POST" id="editUsersForm_{{$index}}">
                                                    <input type="hidden" ng-value="item.id" name="userId">
                                                    <div class="row no-margin" ng-show="item.isOdUser=='N'">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">First Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" ng-model="item.odUserFirstName" placeholder="First Name" name="firstName"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Last Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" ng-model="item.odUserLastName" placeholder="Last Name" name="lastName"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row no-margin"  ng-show="item.isOdUser=='N'">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">User Email</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" ng-model="item.odUserEmail" placeholder="Email" name="userEmail"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Company</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control" ng-model="item.companyInfo" ng-options="option.name for option in companies" ></select>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="companyId" ng-value="item.companyInfo.id">
                                                        </div>
                                                    </div>
                                                    <div class="row no-margin">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Password</label>
                                                                <div class="col-md-9">
                                                                    <input type="password" class="form-control" placeholder="Password" name="password"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Primary Contact</label>
                                                                <div class="col-md-9">
                                                                    <input type="checkbox" class="make-switch" name="isPrimaryContact" ng-model="item.primaryContact">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row no-margin">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Role</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control" ng-model="item.roleInfo" ng-options="option.roleName for option in userRoles" ></select>
                                                                </div>
                                                            </div>
                                                            <input type=hidden name="userRole" ng-value="item.roleInfo.id">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" ng-show="item.odUser2FA==false">
                                                                <label class="control-label col-md-3">2 Factor Auth</label>
                                                                <div class="col-md-9">
                                                                    <input type="checkbox" class="make-switch" name="user2FA" ng-model="item.user2FA">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row no-margin">
                                                        <hr/>
                                                    </div>
                                                    <div class="row no-margin">
                                                        <span class="btn btn-danger" ng-click="item.bedit=false">Cancel</span>
                                                        <span class="btn btn-primary pull-right" ng-click="onUpdateUser( $index )">Update</span>
                                                    </div>
                                                    <div class="row no-margin">
                                                        <hr/>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
    						</div>
    				    </div>
    				</div>				    
			    </div>
                				
			</div>
		</div>
	</div>
    <div id = "idDialog" class="modal fade">
        <form action="<?php echo base_url() . 'users/add';?>" class="horizontal-form" id='formNewUser' method="post">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3>User Info</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">First Name</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="First Name" name="firstName">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Last Name</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="Last Name" name="lastName">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Email</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="Email Address" name=userEmail>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                    	<label class="col-md-3 control-label">Company</label>
                                    	<div class="col-md-8">
                                    		<select ng-model="companyInfo" class="form-control" ng-options="option.name for option in companies" ></select>
                                    	</div>
                                    	<input type=hidden name=companyId ng-value="companyInfo.id" ng-model=companyInfo>
                                    </div>
                             
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Role</label>
                                        <div class="col-md-8">
                                            <select class="form-control" name="roleId">
                                                <option value="2">Read-only Admin</option>
                                                <option value="3">User Role</option>
                                                <option value="4">Read-only User Role</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Primary Contact</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" class="form-control make-switch" name="isPrimaryContact">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span class="btn btn-primary" id="submitButton" ng-click='onAddNewUser()'>Save</span>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>