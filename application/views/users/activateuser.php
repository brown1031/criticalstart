<div class="menu-toggler sidebar-toggler"></div>
<div class="logo">
    <a href="#">
        <img src="<?php echo base_url(); ?>assets/admin/layout2/img/logo-big.png" alt=""/>
    </a>
</div>

<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form action="<?php echo base_url(). 'users/activate';?>" class="login-form" method="post">
        <h3 class="form-title">Please Activate Your Account</h3>

        <?php if (isset( $errors ) ){ echo $errors;}else { echo '';}?>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">New Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Confirm Password" name="passconf"/>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-success uppercase">Activate</button>
            <a href="<?php echo base_url()."users/login";?>" id="login" class="forget-password">Log In</a>
        </div>
        <input type="hidden" name="userEmail" value="<?php echo $this->input->get('userEmail'); ?>">
        <input type="hidden" name="activeCode" value="<?php echo $this->input->get('activeCode'); ?>">
    </form>
</div>