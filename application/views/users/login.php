<div class="menu-toggler sidebar-toggler"></div>
<div class="logo">
	<a href="#">
	<img src="<?php echo base_url(); ?>assets/admin/layout2/img/logo-big.png" alt=""/>
	</a>
</div>

<div class="content">
	<!-- BEGIN LOGIN FORM -->
    <form action="<?php echo base_url(). 'users/checkAuth';?>" class="login-form" method="post">
		<h3 class="form-title">Sign In</h3>

        <?php if (isset( $errors ) ){ echo $errors;}else { echo '';}?>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Username</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="User Email" name="userEmail"/>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="userPass"/>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn btn-success uppercase">Login</button>
			<label class="rememberme check">
			<input type="checkbox" name="remember" value="0" onchange="onRememberMeClick();"/>Remember</label>
			<a href="<?php echo base_url()."users/forgotPassword"?>" id="forget-password" class="forget-password">Forgot Password?</a>
		</div>
	</form>
</div>