<style>
.row-no-margin {
    margin-left: 0px !important;
    margin-right: 0px !important;
    margin-top: 4px;
}
#js-tr-name {
    cursor: pointer;
}
.color-blue {
    color: #009CFF;
}
</style>
    <!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">Domain Lists</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span>Configuration</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<span>Policy Settings</span>
						<i class="fa fa-angle-right"></i>
					</li>					
					<li>
						<span>Domain Lists</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<div class="clearfix">
			</div>
			<div class="row">
			    <div class="col-sm-12">
                    <div class="portlet box blue">
    					<div class="portlet-title">
                            <div class="caption">
    							<i class="fa fa-edit"></i>Editable Table
    						</div>
                            <div class="actions">
    							<a href="#" class="btn btn-default btn-sm">
    							<i class="fa fa-user"></i> Add A New Account </a>
    							<a href="#" class="btn btn-default btn-sm">
    							<i class="fa fa-print"></i> Invite An Existing Account </a>
    						</div>
    					</div>
    					<div class="portlet-body">
    						<div class="table-scrollable">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">Role</th>
                                            <th class="text-center">Timezone</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i = 0; $i < 10; $i++) { ?>
                                        <tr>
                                            <td><?php echo $i + 1; ?></td>
                                            <td id="js-tr-name" class="color-blue">Test User Alpha</td>
                                            <td class="text-center">Full Admin</td>
                                            <td class="text-center">America/Chicago</td>
                                            <td class="text-center">Status</td>                                            
                                            <td class="text-center"><i class="fa fa-user"></i></td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td colspan="6">
                                                <div class="form-horizontal">
                                                    <div class="row row-no-margin">
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control" placeholder="My Love"/>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control" placeholder="Jeni"/>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <select class="form-control">
                                                                <option>User</option>
                                                                <option>Admin</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <select class="form-control">
                                                                <option>Time UTC+00</option>
                                                                <option>Time UTC+01</option>
                                                            </select>
                                                        </div>                                                                                                                                                            
                                                    </div>
                                                    
                                                    <div class="row row-no-margin">
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" placeholder="appleofeye@gmail.com"/>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control" placeholder="Password"/>
                                                        </div>                                                    
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control" placeholder="Confirm Password"/>
                                                        </div>                                                        
                                                    </div>
                                                    <div class="row row-no-margin">
                                                        <hr/>
                                                    </div>
                                                    <div class="row row-no-margin">
                                                        <span class="col-sm-3 form-control-static"><b>Two-step verification</b></span>
                                                        <span class="col-sm-2 form-control-static"><b>Disabled</b></span>
                                                        <div class="col-sm-2">
                                                            <button class="btn btn-default">Enable</button>
                                                        </div>
                                                    </div>
                                                    <div class="row row-no-margin">
                                                        <hr/>
                                                    </div>
                                                    <div class="row row-no-margin">
                                                        <button class="btn btn-danger">Cancel</button>
                                                        <button class="btn btn-primary pull-right">Save</button>
                                                    </div>                                                                                                        
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>							
    						</div>
    				    </div>
    				</div>				    
			    </div>
                				
			</div>
		</div>
	</div>
</div>
<!-- END CONTAINER -->
