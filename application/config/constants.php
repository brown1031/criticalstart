<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('SITE_TITLE',							'Critical Start');

define('USERGROUP_NOT_LOGGEDIN', 				-1);

define('USER_SUPERADMIN',						1);
define('USER_ROADMIN',							2);
define('USER_DEFAULTUSER',						3);
define('USER_ROUSER',							4);

define('EMAIL_HOST', 							'smtp.gmail.com');
define('EMAIL_HOST_PORT', 						465);
define('EMAIL_DEV_ACCOUNT',						'novacenterit@gmail.com');
define('EMAIL_DEV_PASS', 						'novacenter@!@#');
define('DEFAULT_SALT',							'271gWOlB$GO2u@gEl1pRvd#LaiLa0LFT');

/* End of file constants.php */
/* Location: ./application/config/constants.php */