/*
SQLyog Community v11.4 (64 bit)
MySQL - 5.0.27-community-nt : Database - criticalsys_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`criticalsys_db` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `criticalsys_db`;

/*Table structure for table `cs_alerts` */

DROP TABLE IF EXISTS `cs_alerts`;

CREATE TABLE `cs_alerts` (
  `id` bigint(20) NOT NULL auto_increment,
  `info` varchar(512) default NULL,
  `createdAt` varchar(45) default NULL,
  `modifiedAt` varchar(45) default NULL,
  `notes` varchar(256) default NULL,
  `sourceName` varchar(256) default NULL,
  `active` varchar(1) default NULL,
  `shared` varchar(1) default 'N',
  `cs_users_id` bigint(20) default NULL,
  `cs_companies_id` bigint(20) default NULL,
  `cs_sourcetype_id` bigint(20) default NULL,
  `cs_alerttype_id` bigint(20) default NULL,
  `cs_itype_id` bigint(20) default NULL,
  `deletedYN` varchar(1) default 'N',
  PRIMARY KEY  (`id`),
  KEY `fk_cs_alerts_cs_users1` (`cs_users_id`),
  KEY `fk_cs_alerts_cs_companies1` (`cs_companies_id`),
  KEY `fk_cs_alerts_cs_sourcetype1` (`cs_sourcetype_id`),
  KEY `fk_cs_alerts_cs_alerttype1` (`cs_alerttype_id`),
  KEY `fk_cs_alerts_cs_itype1` (`cs_itype_id`),
  CONSTRAINT `fk_cs_alerts_cs_alerttype1` FOREIGN KEY (`cs_alerttype_id`) REFERENCES `cs_alerttype` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_cs_alerts_cs_companies1` FOREIGN KEY (`cs_companies_id`) REFERENCES `cs_companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cs_alerts_cs_itype1` FOREIGN KEY (`cs_itype_id`) REFERENCES `cs_itype` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_cs_alerts_cs_sourcetype1` FOREIGN KEY (`cs_sourcetype_id`) REFERENCES `cs_sourcetype` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_cs_alerts_cs_users1` FOREIGN KEY (`cs_users_id`) REFERENCES `cs_users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_alerts` */

/*Table structure for table `cs_alerttype` */

DROP TABLE IF EXISTS `cs_alerttype`;

CREATE TABLE `cs_alerttype` (
  `id` bigint(20) NOT NULL auto_increment,
  `type` varchar(45) NOT NULL default '',
  `description` varchar(512) default NULL,
  `createdAt` varchar(45) default NULL,
  `modifiedAt` varchar(45) default NULL,
  `modifiedId` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `type` (`type`),
  KEY `fk_cs_alerttype_cs_users1` (`modifiedId`),
  CONSTRAINT `fk_cs_alerttype_cs_users1` FOREIGN KEY (`modifiedId`) REFERENCES `cs_users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_alerttype` */

insert  into `cs_alerttype`(`id`,`type`,`description`,`createdAt`,`modifiedAt`,`modifiedId`) values (1,'IP4 Address','adsdsaaa',NULL,'2015-03-13 10:00:42',NULL),(2,'IP6 Address','asdfasdf',NULL,'2015-03-13 10:00:52',NULL),(3,'Domain','',NULL,'2015-03-13 10:03:41',NULL),(4,'URL',NULL,NULL,NULL,NULL),(5,'File Hash MD5',NULL,NULL,NULL,NULL),(6,'File Hash SHA1',NULL,NULL,NULL,NULL),(7,'File Hash SHA256',NULL,NULL,NULL,NULL),(8,'Email address',NULL,NULL,NULL,NULL),(18,'tes','','2015-03-15 22:47:55','2015-03-15 22:47:55',NULL);

/*Table structure for table `cs_audits` */

DROP TABLE IF EXISTS `cs_audits`;

CREATE TABLE `cs_audits` (
  `id` bigint(20) NOT NULL auto_increment,
  `auditDetails` varchar(512) default NULL,
  `auditAction` varchar(45) default NULL,
  `auditDate` varchar(45) default NULL,
  `auditUserId` varchar(45) default NULL,
  `odComOrgId` varchar(45) default NULL,
  `cs_companies_id` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_cs_audits_cs_companies1` (`cs_companies_id`),
  CONSTRAINT `fk_cs_audits_cs_companies1` FOREIGN KEY (`cs_companies_id`) REFERENCES `cs_companies` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_audits` */

insert  into `cs_audits`(`id`,`auditDetails`,`auditAction`,`auditDate`,`auditUserId`,`odComOrgId`,`cs_companies_id`) values (1,'Super Admin logged in successfully!','Super Admin Login','2015-03-24 04:47:05','Super Admin',NULL,NULL),(2,'Super Admin logged in successfully!','Super Admin Login','2015-03-24 05:22:30','Super Admin',NULL,NULL),(3,'Super Admin logged in successfully!','Super Admin Login','2015-03-24 18:30:57','Super Admin',NULL,NULL),(4,'Super Admin logged in successfully!','Super Admin Login','2015-03-25 08:58:56','Super Admin',NULL,NULL),(5,'Super Admin logged in successfully!','Super Admin Login','2015-03-26 19:17:26','Super Admin',NULL,NULL),(6,'Super admin logged out','Super admin Logout','2015-03-26 20:03:58',NULL,NULL,NULL),(7,'Super Admin logged in successfully!','Super Admin Login','2015-03-26 20:04:13','Super Admin',NULL,NULL),(8,'Super Admin logged in successfully!','Super Admin Login','2015-03-27 00:24:37','Super Admin',NULL,NULL),(9,'Super Admin logged in successfully!','Super Admin Login','2015-03-29 03:32:35','Super Admin',NULL,NULL);

/*Table structure for table `cs_companies` */

DROP TABLE IF EXISTS `cs_companies`;

CREATE TABLE `cs_companies` (
  `id` bigint(20) NOT NULL auto_increment,
  `odOrgId` varchar(100) NOT NULL,
  `odOriginId` varchar(100) default NULL,
  `odOrganizationName` varchar(100) default NULL,
  `odCreatorUserId` varchar(100) default NULL,
  `odMspOrgId` varchar(128) default NULL,
  `odOrgTypeId` varchar(100) default NULL,
  `odCreatedAt` varchar(45) default NULL,
  `odModifiedAt` varchar(45) default NULL,
  `odHasDelegatedAdmin` varchar(30) default NULL,
  `enabledFireEye` varchar(1) default NULL,
  `odNetworkCount` varchar(45) default NULL,
  `odToken` varchar(100) default NULL,
  `apiKey` varchar(200) default NULL,
  `userId` varchar(45) default NULL,
  `password` varchar(45) default NULL,
  `enabledWhiteList` varchar(1) default NULL,
  `whiteListVal` text,
  `sharedThreatIntel` varchar(1) default NULL,
  `daysAlertActive` varchar(10) default '30',
  `require2FA` varchar(1) default NULL,
  `connectWiseId` varchar(50) default NULL,
  `customApiKey` varchar(1) default 'N',
  `deletedYN` varchar(1) default 'N',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `odOrgId_UNIQUE` (`odOrgId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_companies` */

insert  into `cs_companies`(`id`,`odOrgId`,`odOriginId`,`odOrganizationName`,`odCreatorUserId`,`odMspOrgId`,`odOrgTypeId`,`odCreatedAt`,`odModifiedAt`,`odHasDelegatedAdmin`,`enabledFireEye`,`odNetworkCount`,`odToken`,`apiKey`,`userId`,`password`,`enabledWhiteList`,`whiteListVal`,`sharedThreatIntel`,`daysAlertActive`,`require2FA`,`connectWiseId`,`customApiKey`,`deletedYN`) values (65,'1181921','29388337','XYZ Test - For Testing Only','6471955','717057','1','2015-02-26 19:36:01','2015-02-26 19:36:01','1','N','0','1398162D0CB6F1A61E9542B6E95240DA','4190F5204439FBCC08E06DBF2B4480F7','xyztestuser@criticalstart.com','xyztestuser','N','adsfasdfaasdfasdf','N','1111','N','asdfasdfasdfasdf','Y','N');

/*Table structure for table `cs_cookies` */

DROP TABLE IF EXISTS `cs_cookies`;

CREATE TABLE `cs_cookies` (
  `id` bigint(11) unsigned NOT NULL auto_increment,
  `cookie_id` varchar(255) default NULL,
  `userid` varchar(255) default NULL,
  `password` varchar(255) default NULL,
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_cookies` */

/*Table structure for table `cs_domains` */

DROP TABLE IF EXISTS `cs_domains`;

CREATE TABLE `cs_domains` (
  `id` bigint(20) NOT NULL auto_increment,
  `odId` varchar(45) default NULL,
  `odAccess` varchar(45) default NULL,
  `odisGlobal` varchar(45) default NULL,
  `odName` varchar(45) default NULL,
  `odCreatedAt` varchar(45) default NULL,
  `odModifiedAt` varchar(45) default NULL,
  `odIsMspDefault` varchar(45) default NULL,
  `sourceTypeId` bigint(45) default NULL,
  `domainLimit` int(11) default '500',
  `cs_companies_id` bigint(20) NOT NULL,
  `domainListSync` varchar(1) default 'Y',
  PRIMARY KEY  (`id`),
  KEY `fk_cs_domains_cs_companies1` (`cs_companies_id`),
  KEY `fk_sourcetype_id` (`sourceTypeId`),
  CONSTRAINT `fk_cs_domains_cs_companies1` FOREIGN KEY (`cs_companies_id`) REFERENCES `cs_companies` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_sourcetype_id` FOREIGN KEY (`sourceTypeId`) REFERENCES `cs_sourcetype` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_domains` */

insert  into `cs_domains`(`id`,`odId`,`odAccess`,`odisGlobal`,`odName`,`odCreatedAt`,`odModifiedAt`,`odIsMspDefault`,`sourceTypeId`,`domainLimit`,`cs_companies_id`,`domainListSync`) values (301,'316930','allow','N','MSP Default Allow Domainlist','1421886118','1424989569','N',1,50,65,'Y'),(302,'316932','block','N','MSP Default Block Domainlist','1421886119','1424989590','N',1,50,65,'Y'),(303,'329103','block','N','Critical Start Threat Intel Block List','1423677631','1423677631','N',1,50,65,'Y'),(304,'337053','allow','Y','Global Allow List','1424979361','1424979361','N',1,50,65,'Y'),(305,'337055','block','Y','Global Block List','1424979361','1424979361','N',1,50,65,'Y'),(306,'345085','allow','N','testDomain','1426100511','1426100511','N',1,50,65,'Y'),(307,'345599','allow','N','testDomainList','1426168536','1426168536','N',1,50,65,'Y'),(308,'345601','block','N','ttestes','1426168573','1426168573','N',1,50,65,'Y'),(309,'345603','allow','N','ttert','1426168842','1427165932','N',1,50,65,'Y');

/*Table structure for table `cs_itype` */

DROP TABLE IF EXISTS `cs_itype`;

CREATE TABLE `cs_itype` (
  `id` bigint(20) NOT NULL auto_increment,
  `itypeType` varchar(30) NOT NULL default '',
  `description` varchar(512) default NULL,
  `createdAt` varchar(45) default NULL,
  `modifiedAt` varchar(45) default NULL,
  `modifiedId` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `itypeType` (`itypeType`),
  KEY `fk_cs_itype_cs_users1` (`modifiedId`),
  CONSTRAINT `fk_cs_itype_cs_users1` FOREIGN KEY (`modifiedId`) REFERENCES `cs_users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_itype` */

insert  into `cs_itype`(`id`,`itypeType`,`description`,`createdAt`,`modifiedAt`,`modifiedId`) values (1,'Malicious Actor','asfd',NULL,'2015-03-13 11:46:47',NULL),(2,'Adware',NULL,NULL,NULL,NULL),(3,'Botnet',NULL,NULL,NULL,NULL),(4,'IP Check',NULL,NULL,NULL,NULL),(5,'Exploit Kit',NULL,NULL,NULL,NULL),(6,'IP Geolocation',NULL,NULL,NULL,NULL),(7,'Malware Download',NULL,NULL,NULL,NULL),(8,'Phishing',NULL,NULL,NULL,NULL),(9,'SPAM',NULL,NULL,NULL,NULL),(10,'Scanning',NULL,NULL,NULL,NULL),(11,'TOR',NULL,NULL,NULL,NULL),(12,'Sinkhole',NULL,NULL,NULL,NULL),(13,'APT',NULL,NULL,NULL,NULL),(14,'FireEye Domain',NULL,NULL,NULL,NULL),(15,'FireEye IP',NULL,NULL,NULL,NULL),(16,'Unknown',NULL,NULL,NULL,NULL),(25,'test','testasdf','2015-03-15 22:38:59','2015-03-15 22:39:27',NULL);

/*Table structure for table `cs_role` */

DROP TABLE IF EXISTS `cs_role`;

CREATE TABLE `cs_role` (
  `id` int(11) NOT NULL auto_increment,
  `roleName` varchar(50) NOT NULL default '',
  `roleDescription` varchar(1024) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `roleName` (`roleName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_role` */

insert  into `cs_role`(`id`,`roleName`,`roleDescription`) values (1,'Super Admin','Super Admin - updated description.'),(2,'Read Only Admin','Read only Admin has access to view all records in the application but can not edit anything except his own user record'),(3,'User Role','User role has access only to the records for their company.'),(4,'Read Only User Role','Read only user role has access to all records but can not edit anything except his own user record.');

/*Table structure for table `cs_scripts` */

DROP TABLE IF EXISTS `cs_scripts`;

CREATE TABLE `cs_scripts` (
  `id` bigint(20) NOT NULL auto_increment,
  `scriptFilePath` varchar(100) NOT NULL default '',
  `scriptFileName` varchar(45) default NULL,
  `scriptDescription` varchar(512) default NULL,
  `scriptRunTimes` varchar(45) default NULL,
  `scriptArgs` varchar(100) default NULL,
  `scriptMaxRuntime` varchar(45) default NULL,
  `scriptOrgs` varchar(512) default NULL,
  `scriptReturnVals` varchar(1024) default NULL,
  `scriptAudit` varchar(1) default 'Y',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `scriptFilePath` (`scriptFilePath`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_scripts` */

insert  into `cs_scripts`(`id`,`scriptFilePath`,`scriptFileName`,`scriptDescription`,`scriptRunTimes`,`scriptArgs`,`scriptMaxRuntime`,`scriptOrgs`,`scriptReturnVals`,`scriptAudit`) values (2,'/var/pyapps/test.py','test script','This is test app','1','test','2','2','test=1','Y'),(3,'te','te','te','0','te','te','1','te','N'),(4,'11','te','te','0','te','te','1','te','N');

/*Table structure for table `cs_siteconfig` */

DROP TABLE IF EXISTS `cs_siteconfig`;

CREATE TABLE `cs_siteconfig` (
  `id` bigint(20) NOT NULL auto_increment,
  `supportEmail` varchar(45) default 'support@criticalstart.com',
  `apiKey` varchar(200) default NULL,
  `phone` varchar(45) default '(855)980-7440',
  `httpPort` int(11) default NULL,
  `maxDomainList` int(11) default NULL,
  `minPassword` int(11) default '4',
  `maxPassword` int(11) default '32',
  `failedAttempts` int(11) default '10',
  `lockout` int(11) default '15',
  `rememberMe` int(11) default '30',
  `smsUrl` varchar(100) default NULL,
  `smsUser` varchar(45) default NULL,
  `smsPassword` varchar(45) default NULL,
  `smsFrom` varchar(45) default NULL,
  `smsApiId` varchar(100) default NULL,
  `smsCertificatie` text,
  `retentionPeriod` varchar(45) default NULL,
  `smsCertFile` varchar(45) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_siteconfig` */

insert  into `cs_siteconfig`(`id`,`supportEmail`,`apiKey`,`phone`,`httpPort`,`maxDomainList`,`minPassword`,`maxPassword`,`failedAttempts`,`lockout`,`rememberMe`,`smsUrl`,`smsUser`,`smsPassword`,`smsFrom`,`smsApiId`,`smsCertificatie`,`retentionPeriod`,`smsCertFile`) values (1,'support@advancedthreatanalytics.com','4190F5204439FBCC08E06DBF2B4480F7','(855)980-7440',8090,501,8,32,10,15,30,'42','123','123','111','222','N','12',NULL);

/*Table structure for table `cs_sourcetype` */

DROP TABLE IF EXISTS `cs_sourcetype`;

CREATE TABLE `cs_sourcetype` (
  `id` bigint(20) NOT NULL auto_increment,
  `typeName` varchar(45) NOT NULL default '',
  `typeDescription` varchar(45) default NULL,
  `createdAt` varchar(45) default NULL,
  `modifiedAt` varchar(45) default NULL,
  `modifiedId` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `typeName` (`typeName`),
  KEY `fk_cs_sourcetype_cs_users2` (`modifiedId`),
  CONSTRAINT `fk_cs_sourcetype_cs_users2` FOREIGN KEY (`modifiedId`) REFERENCES `cs_users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_sourcetype` */

insert  into `cs_sourcetype`(`id`,`typeName`,`typeDescription`,`createdAt`,`modifiedAt`,`modifiedId`) values (1,'FireEye','FireEye HTTP Posts',NULL,NULL,NULL),(2,'CSV Upload','CSV File Upload',NULL,NULL,NULL),(3,'Manual - User','Manually added by User',NULL,NULL,NULL),(4,'Manual - Admin','Manually added by Admin',NULL,NULL,NULL),(5,'Other','Other.',NULL,'2015-03-13 11:22:45',NULL),(15,'test','test3','2015-03-16 16:08:30','2015-03-16 16:08:30',NULL);

/*Table structure for table `cs_subdomains` */

DROP TABLE IF EXISTS `cs_subdomains`;

CREATE TABLE `cs_subdomains` (
  `id` bigint(11) unsigned NOT NULL auto_increment,
  `name` varchar(45) default NULL,
  `domainListId` bigint(11) default NULL,
  `odId` varchar(45) default NULL,
  `createdAt` varchar(45) default NULL,
  PRIMARY KEY  (`id`),
  KEY `cs_domainlist_id` (`domainListId`),
  CONSTRAINT `cs_domainlist_id` FOREIGN KEY (`domainListId`) REFERENCES `cs_domains` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_subdomains` */

insert  into `cs_subdomains`(`id`,`name`,`domainListId`,`odId`,`createdAt`) values (294,'example.com',303,'2201','1295584907'),(295,'owasp.org',304,'4731145',NULL),(296,'testdomain1.com',306,'3664065','1314552725'),(297,'rickyrocks.com',306,'15594983',NULL),(298,'robtest2.com',308,'15595633',NULL),(299,'robtest23.com',308,'15595635',NULL);

/*Table structure for table `cs_tickets` */

DROP TABLE IF EXISTS `cs_tickets`;

CREATE TABLE `cs_tickets` (
  `id` bigint(11) unsigned NOT NULL auto_increment,
  `ticketTitle` varchar(256) default NULL,
  `ticketContent` text,
  `createDate` varchar(30) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_tickets` */

/*Table structure for table `cs_users` */

DROP TABLE IF EXISTS `cs_users`;

CREATE TABLE `cs_users` (
  `id` bigint(20) NOT NULL auto_increment,
  `cs_companies_id` bigint(20) NOT NULL,
  `odUserId` varchar(100) default NULL,
  `odUserFirstName` varchar(45) default NULL,
  `odUserLastName` varchar(45) default NULL,
  `odUserEmail` varchar(45) default NULL,
  `odTwoFactorPhone` varchar(45) default 'N',
  `lastLogin` varchar(45) default NULL,
  `createdAt` varchar(45) default NULL,
  `modifiedAt` varchar(45) default NULL,
  `hashKey` varchar(255) default NULL,
  `password` varchar(512) default NULL,
  `odUserRole` varchar(45) default NULL,
  `odUserRoleId` varchar(45) default NULL,
  `odUserStatus` varchar(45) default NULL,
  `odUser2FA` varchar(1) default 'N',
  `user2FA` varchar(1) default 'N',
  `odUser2FactorTypeName` varchar(45) default NULL,
  `odUser2FTypeId` varchar(45) default NULL,
  `primaryContact` varchar(1) default 'N',
  `cs_role_id` int(11) default '3',
  `isOdUser` varchar(1) default 'N',
  PRIMARY KEY  (`id`),
  KEY `fk_cs_users_cs_companies` (`cs_companies_id`),
  KEY `fk_cs_users_cs_role1` (`cs_role_id`),
  CONSTRAINT `fk_cs_users_cs_companies` FOREIGN KEY (`cs_companies_id`) REFERENCES `cs_companies` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_cs_users_cs_role1` FOREIGN KEY (`cs_role_id`) REFERENCES `cs_role` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_users` */

insert  into `cs_users`(`id`,`cs_companies_id`,`odUserId`,`odUserFirstName`,`odUserLastName`,`odUserEmail`,`odTwoFactorPhone`,`lastLogin`,`createdAt`,`modifiedAt`,`hashKey`,`password`,`odUserRole`,`odUserRoleId`,`odUserStatus`,`odUser2FA`,`user2FA`,`odUser2FactorTypeName`,`odUser2FTypeId`,`primaryContact`,`cs_role_id`,`isOdUser`) values (41,65,'6722985','XYZ Test','User','xyztestuser@criticalstart.com','',NULL,'2015-03-24 04:47:11','2015-03-24 04:47:11','271qW01B$G02u@gE11pRvd','56e107d9516bcdb0a92485e77b841d60c4a5a30ac8cd8eef246244b24d131d0b','Full Admin','1','Active','N','N','','','N',3,'Y'),(42,65,NULL,'adsf','adsf','karl.sv1031@gmail.com','N',NULL,'2015-03-24 06:07:13',NULL,NULL,NULL,NULL,NULL,NULL,'N','N',NULL,NULL,'N',2,'N'),(43,65,NULL,'Jim','Brian','jim.briam@gmail.com','N',NULL,'2015-03-24 06:07:56',NULL,NULL,NULL,NULL,NULL,NULL,'N','N',NULL,NULL,'N',2,'N'),(44,65,NULL,'Jim','Brianasdf','jim.briam@gmail.com','N',NULL,'2015-03-24 06:08:06',NULL,NULL,NULL,NULL,NULL,NULL,'N','N',NULL,NULL,'N',3,'N'),(45,65,NULL,'Jim','Brianasdf','jim.briam@gmail.com','N',NULL,'2015-03-24 06:08:37',NULL,NULL,NULL,NULL,NULL,NULL,'N','N',NULL,NULL,'N',3,'N'),(46,65,NULL,'Jim','Brianasdf','jim.briam@gmail.com','N',NULL,'2015-03-24 06:14:35',NULL,NULL,NULL,NULL,NULL,NULL,'N','N',NULL,NULL,'N',3,'N'),(47,65,NULL,'a','fasdf','asdf@mail.com','N',NULL,'2015-03-24 06:14:54',NULL,NULL,NULL,NULL,NULL,NULL,'N','N',NULL,NULL,'N',2,'N');

/*Table structure for table `cs_whitelist` */

DROP TABLE IF EXISTS `cs_whitelist`;

CREATE TABLE `cs_whitelist` (
  `id` bigint(20) NOT NULL auto_increment,
  `data` varchar(512) default NULL,
  `notes` varchar(256) default NULL,
  `createdAt` varchar(45) default NULL,
  `modifiedAt` varchar(45) default NULL,
  `extra` varchar(512) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cs_whitelist` */

insert  into `cs_whitelist`(`id`,`data`,`notes`,`createdAt`,`modifiedAt`,`extra`) values (2,'test','test','2015-03-13 15:41:45','2015-03-13 15:41:45',NULL),(5,'www.google.com','Should never block Google','2015-03-15 18:27:41','2015-03-15 18:27:41',NULL),(14,'test.com','test 2','2015-03-16 16:07:43','2015-03-16 16:07:43',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
